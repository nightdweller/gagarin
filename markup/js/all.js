var Mouse = {x: 0, y: 0};

$(document).ready(function () {
    $('body').addClass('main');

    $('.js-logo img').mouseFlipTop();
    $('.js-moon-dynamic').mouseFlipRight(40, 50).mouseFlipTop(40, 200).clickscale('img');
    $('.js-grid').backgroundFlipRight(100, 2000).backgroundFlipTop(100, 2000);
});

$(document).on('mousemove', function (e) {
    Mouse.x = e.pageX;
    Mouse.y = e.pageY;
});