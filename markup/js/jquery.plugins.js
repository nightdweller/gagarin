$(function ($) {
    $.fn.mouseFlipTop = function (k, radius) {
        k = k || 20;
        radius = radius || 25;
        var $this = this;

        var defPos = $this.position().top;
        var height = window.innerHeight;
        var real = 0;

        setInterval(function () {
            var mul = 0.5 - Mouse.y / height;
            var pos = $this.position().top;
            real = defPos + mul * radius;
            $this.css('top', parseFloat(pos) + (real - parseFloat(pos)) / k);
        }, 30);
        return this;
    };

    $.fn.mouseFlipRight = function (k, radius) {
        k = k || 20;
        radius = radius || 25;
        var $this = this;

        var defPos = $this.position().left;
        var height = window.innerHeight;
        var real = 0;

        setInterval(function () {
            var mul = 0.5 - Mouse.x / height;
            var pos = $this.position().left;
            real = defPos + mul * radius;
            $this.css('left', parseFloat(pos) + (real - parseFloat(pos)) / k);
        }, 30);
        return this;
    };

    $.fn.backgroundFlipRight = function (k, radius) {
        k = k || 20;
        radius = radius || 25;
        var $this = this;

        var defPos = 0;
        var height = window.innerHeight;
        var real = 0;

        setInterval(function () {
            var mul = 0.5 - Mouse.x / height;
            var pos = parseFloat($this.css('background-position-x'));
            real = defPos + mul * radius;
            $this.css('background-position-x', (parseFloat(pos) + (real - parseFloat(pos)) / k) + 'px');
        }, 30);
        return this;
    };

    $.fn.backgroundFlipTop = function (k, radius) {
        k = k || 20;
        radius = radius || 25;
        var $this = this;

        var defPos = 0;
        var height = window.innerHeight;
        var real = 0;

        setInterval(function () {
            var mul = 0.5 - Mouse.y / height;
            var pos = parseFloat($this.css('background-position-y'));
            real = defPos + mul * radius;
            $this.css('background-position-y', (parseFloat(pos) + (real - parseFloat(pos)) / k) + 'px');
        }, 30);
        return this;
    };

    $.fn.clickscale = function (selector) {
        var s = 1;
        var $this = this;
        this.on('mousedown', function (e) {
            s += (0.8 - s) / 2;
            $this.find(selector).css('transform', 'scale(' + s + ')');
        });

        setInterval(function () {
            s += (1 - s) / 3;
            $this.find(selector).css('transform', 'scale(' + s + ')');
        }, 100);

        return this;
    };
});