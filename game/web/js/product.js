var Product = {
    lastUpdate: moment()
};

Product.product = function (id, name, description, price, count, remainTime, marketPrice) {
    var that = this;
    this.id = id;
    this.name = name;
    this.description = description;
    this.price = price;
    this.count = count || 0;
    this.remainTime = remainTime;
    this.marketPrice = marketPrice;

    var end = moment().add(1000 * remainTime);
    this.isActive = function () {
        return that.remainTime > 0;
    };

    this.setCount = function (count) {
        this.count = count;
    }
};

var ProductRegistry = {
    products: []
};

ProductRegistry.pushProduct = function (id, name, description, price, count, time, marketPrice) {
    ProductRegistry.products.push(new Product.product(id, name, description, price, count, time, marketPrice));
};

ProductRegistry.getProduct = function (id) {
    for (var i = 0; i < ProductRegistry.products.length; i++) {
        if (ProductRegistry.products[i].id == id) {
            return ProductRegistry.products[i];
        }
    }

    return null; 
};

ProductRegistry.init = function () {
    var fabricPrice = ProductRegistry.getProduct('fabric_price');
    fabricPrice.onRelease = function () {
        for (var i = 0; i < User.fabrics.length; i++) {
            var fabric = User.fabrics[i];
            $('.js-fabrics .js-item[data-index=' + fabric.id + ']')
                .find('.js-price')
                .attr('data-number', Math.ceil(fabric.getPrice()))
            ;
        }
    };

    fabricPrice.onDestroy = function () {
        for (var i = 0; i < User.fabrics.length; i++) {
            var fabric = User.fabrics[i];
            $('.js-fabrics .js-item[data-index=' + fabric.id + ']')
                .find('.js-price')
                .attr('data-number', Math.ceil(fabric.getPrice()))
            ;
        }
    }
};

$(document).ready(function () {
    var $productsContainer = $('.js-products-container');
    var $products = $('.js-products');

    for (var i = 0; i < ProductRegistry.products.length; i++) {
        $products.append(Templates.createProduct(ProductRegistry.products[i]));
    }
    var $tooltip = $('.js-product-tooltip');
    var $buyBtn = $productsContainer.find('.js-buy-btn');

    $products.on('mouseover', '.js-product', function () {
        var $this = $(this);
        $tooltip.find('.js-header').html($this.attr('data-name'));
        $tooltip.find('.js-description').html($this.attr('data-description'));
        $buyBtn.attr('data-id', $this.attr('data-id'));
        $productsContainer.addClass('hover');
        $products.find('.js-product').removeClass('hover');
        $this.addClass('hover');
    }).on('click', '.js-product', function () {
        var $this = $(this);
        App
            .api('product.release', {id: $this.attr('data-id')})
            .done(function (data) {
                var product = ProductRegistry.getProduct($this.attr('data-id'));

                product.count = data.count;
                if (data.remain_time) {
                    product.remainTime = data.remain_time;
                    product.endTime = moment().add(product.remainTime, 'seconds');
                }

                if (product.onRelease) {
                    product.onRelease();
                }

                if (data.product) {
                    product = ProductRegistry.getProduct(data.product.id);
                    product.count = data.product.count;
                    product.remainTime = data.product.remain_time;
                    product.endTime = moment().add(product.remainTime, 'seconds');

                    if (product.onRelease) {
                        product.onRelease();
                    }
                }

                Product.render();
            })
        ;
    }).find('.js-timer').timer(function (diff) {
        var secs = diff.asSeconds();
        var h = intval(secs / 3600).toString();
        var m = intval((secs % 3600) / 60).toString();
        var s = intval(secs % 60).toString();

        if (h.length < 2) h = '0' + h;
        if (m.length < 2) m = '0' + m;
        if (s.length < 2) s = '0' + s;

        if (h != '00') {
            return h + ':' + m + ':' + s;
        }

        if (m != '00') {
            return m + ':' + s;
        }

        return diff.seconds();
    }, null, function (diff) {
        var id = this.closest('.js-product').attr('data-id');
        var product = ProductRegistry.getProduct(id);
        product.remainTime = diff.asSeconds();
    }, function () {
        var id = this.closest('.js-product').attr('data-id');
        var product = ProductRegistry.getProduct(id);
        if (product.onDestroy) {
            product.onDestroy();
        }
    });

    $productsContainer.mouseleave(function (e) {
        $productsContainer.removeClass('hover');
    });

    $products.find('.js-count').each(function () {
        var $this = $(this);

        if ($this.attr('data-count') == '0') {
            $this.hide();
        } else {
            $this.show();
        }
    });

    VK.addCallback('onOrderSuccess', function (id) {
        App.api('shop.update', {order_id: id}).done(Shop.handleResponse);
    });

    $buyBtn.on('click', function () {
        var product = ProductRegistry.getProduct($(this).attr('data-id'));

        if (User.balance - product.marketPrice >= 0) {
            App.api('shop.buy', {
                item: product.id + '|1'
            }).done(Shop.handleResponse);

            Effects.scoreSprite('+1', false);

            return;
        }

        VK.callMethod('showOrderBox', {
            type: 'item',
            item: $buyBtn.attr('data-id') + '|1'
        });
    });

    Product.render();
});

Product.render = function () {
    var $products = $('.js-products .js-product');
    $products.each(function () {
        var $this = $(this);
        var product = ProductRegistry.getProduct($this.attr('data-id'));

        if (product.count) {
            $this.find('.js-count').show().html(product.count);
        } else {
            $this.find('.js-count').hide();
        }

        $this.find('.js-timer').attr('data-left', product.remainTime);
        $this.find('.js-timer').updateTimer();
    });
};

$(document).on('click', '.js-fabric-votes-buy', function (e) {
    var $this = $(this);

    VK.callMethod('showOrderBox', {
        type: 'item',
        item: 'fabric:' + $this.attr('data-index') + '|1'
    });
}).on('click', '.js-tender-buy', function (e) {
    var $this = $(this);

    VK.callMethod('showOrderBox', {
        type: 'item',
        item: 'money:' + $this.attr('data-count') + '|1'
    });
}).on('click', '.js-energy-votes-buy', function (e) {
    var $this = $(this);

    VK.callMethod('showOrderBox', {
        type: 'item',
        item: 'energy:' + $this.attr('data-count') + '|1'
    });
}).on('click', '.js-energy-buy', function (e) {
    var $this = $(this);
    var price = intval($this.attr('data-price'));
    var count = intval($this.attr('data-count'));
    if (User.balance - price >= 0) {
        App.api('shop.buy', {
            item: 'energy:' + count + '|1'
        }).done(Shop.handleResponse);

        Message.show('Куплена энергия', 'Покупка');
    } else {
        Message.show('Нехватает септимов');
    }
});