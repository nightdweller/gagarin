$(document).on('click', 'a[ajax]', function (e) {
    e.preventDefault();
    $.ajax({
        url: $(this).attr('href')
    }).done(function (data) {
        $('.js-container').html(data);
    });
});