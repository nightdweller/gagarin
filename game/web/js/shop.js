var Shop = {};

Shop.handleResponse = function (response) {
    if (response.fabrics) {
        var fabrics = response.fabrics;

        for (var i = 0; i < fabrics.length; i++) {
            var fabricData = fabrics[i];
            var fabric = User.getFabric(fabricData.id);
            fabric.count = fabricData.count;

            fabric.price = PriceGenerator.getPrice(fabric.id, fabric.count);
            var $fabric = $('.js-item[data-index=' + fabric.id + ']');
            $fabric.find('.item-count').html(fabric.count);
            $fabric.find('.item-price').attr('data-number', fabric.getPrice());
            $fabric.find('.js-profit-all').html(Math.ceil(fabric.profit * fabric.count * 100) / 100);
            User.incValue += fabric.profit;
            $('.js-money-diff').attr('data-number', User.incValue);

            fabric.$entity.show('slow');
        }

        updateFabrics();
    }

    if (response.balance) {
        User.balance = response.balance;
        $('.js-balance').html(response.balance);

        App.page('main');
    }

    if (response.energy) {
        User.energy = response.energy;
        setTimeout(function () {
            User.render();
        }, 300);
    }

    if (response.products) {
        var products = response.products;

        for (var i = 0; i < products.length; i++) {
            var productData = products[i];
            var product = ProductRegistry.getProduct(productData.id);
            product.count = parseInt(productData.count);
        }

        setTimeout(Product.render, 300);
    }

    if (response.sponsor) {
        var product = response.sponsor;
        var types = {'sponsor_big': 'big', 'sponsor_medium': 'medium', 'sponsor_small': 'small'};
        var type = types[product.type];
        var $sponsor = $('.js-buy-sponsor[data-type=' + type + '][data-index=' + product.index + ']');
        $sponsor.find('img').attr('src', Vk.currentUser.getPhoto());
        $sponsor.find('.sponsor-name').html(Vk.currentUser.first_name + ' ' + Vk.currentUser.last_name);
        $sponsor.removeClass('free').removeClass('js-buy-sponsor');
    }
};

Shop.open = function () {
    var $shop = $('.js-bank-modal');
    $shop.show();
    $shop.removeClass('hidden');
};

Shop.close = function () {
    var $shop = $('.js-bank-modal');
    $shop.addClass('hidden');
    setTimeout(function () {
        $shop.hide();
    }, 500);
};