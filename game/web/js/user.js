var User = {
    renderData: {
        money: 0,
        friendMoney: 0,
        init: false,
        update: function () {
            User.renderData.money += (User.money - User.renderData.money) / 2;
            User.renderData.friendMoney += (User.friendMoney - User.renderData.friendMoney) / 2;
        }
    },
    fabrics: [],
    bonus: {friends: false, menu: false, group: false},
    clicksCount: 0,
    inClicksCount: 0
};

User.pushFabric = function (id, name, description, price, profit, count, fullDescription, coords) {
    User.fabrics.push(new Fabric.fabric(id, name, description, price, profit, count, fullDescription, coords));
};

User.updateBonus = function(key, value) {
    User.bonus[key] = value;

    if (User.bonus.friends && User.bonus.menu && User.bonus.group) {
        $('.js-todo').hide('slow', function () {
            var $freeGiftTimer = $('.js-free-gift-timer');
            if ($freeGiftTimer.attr('data-left') < 0) {
                $('.js-get-free-gift').show();
                $('.js-free-gift-message').hide('slow');
                $freeGiftTimer.hide();
            } else {
                $freeGiftTimer.timer('%h:%m:%s', null, null, function () {
                    $('.js-free-gift-timer').hide();
                    $('.js-get-free-gift').show();
                });

                $freeGiftTimer.show('slow');
                $('.js-free-gift-message').show('slow');
            }
        });
    } else {
        $('.js-free-gift-timer').hide();
        $('.js-free-gift-message').hide();
    }

    return value;
};

User.getFabric = function (id) {
    for (var i = 0; i < User.fabrics.length; i++) {
        if (User.fabrics[i].id == id) {
            return User.fabrics[i];
        }
    }

    return null;
};

$(document).ready(function () {
    User.renderData.init = true;
    User.renderData.$money = $('.js-user-money');
    User.renderData.$pageMoney = $('.js-user-page-money');
    User.renderData.$moneySpan = User.renderData.$money.find('span');
    User.renderData.$user = $($('.js-user').get(1));
    User.renderData.$word = User.renderData.$money.find('span.word');
    User.renderData.$energyBar = $('.js-energy-bar');
    User.renderData.$energy = User.renderData.$energyBar.find('.status');
    User.renderData.$energyStatus = User.renderData.$energyBar.find('.js-energy-status');
});

User.render = function () {
    if (!User.renderData.init) return;
    User.renderData.update();
    var money = Math.ceil(User.renderData.money || (User.renderData.money = 0)).toString();
    if (!User.home) {
        money = Math.ceil(User.renderData.friendMoney || (User.renderData.friendMoney = 0)).toString();
    }

    var energyPercent = intval(100 * (User.energy / User.maxEnergy));
    User.renderData.$energy.css('width', energyPercent + '%');
    User.renderData.$energyStatus.html(energyPercent + '%');

    var pretty = Assets.prettyNumber(money);
    var $money = User.home ? User.renderData.$money : User.renderData.$pageMoney;
    var width = 0;
    $money.find('span').each(function (index) {
        width += this.offsetHeight ? parseInt(this.offsetWidth) : 0;

        if (pretty.charAt(index)) {
            this.innerHTML = pretty.charAt(index);

            if ('0' <= pretty.charAt(index) && pretty.charAt(index) <= '9') {
                this.className = '';
            } else {
                this.className = 'symbol';
            }
        } else {
            this.innerHTML = '';
        }
    });

    $money.css('margin-left', (465 - width) / 2 + 'px');

    //VK.callMethod('setTitle', Assets.money(money, 4));

    $($('.js-user').get(1)).find('.js-money').attr('data-number', money);
    $money.find('span.word').html(Assets.numberWord(money) + ' $');
};

//User.update = function () {
//    App.api('user.get', {id: User.id}).done(function (data) {
//        User.money = data.money;
//        User.lifes = data.lifes;
//        User.render();
//    });
//};

User.getIncValue = function () {
    var inc = User.incValue * (Math.random() / 2 + 0.25).toFixed(2);
    if (ProductRegistry.getProduct('click_power').isActive() || ProductRegistry.getProduct('moon_party').isActive()) {
        inc *= 2;
    }

    return inc;
};

User.gameValue = 0;

User.moonMoney = function () {
    if (!User.home) {
        //User.friendMoney += Math.ceil(User.currentIncValue * 100) / 100;
        User.clicksCount++;

        return Math.ceil(User.currentIncValue * 100) / 100;
    }

    User.money += User.getIncValue();
    User.clicks++;
    User.gameValue++;
    if (User.gameValue > 1000) {
        User.gameValue = 0;
        Game1.start();
    }

    return Math.ceil(User.getIncValue() * 100) / 100;
};

setInterval(function () {
    if (!User.home) {
        var clicksCount = 0;
        Vo.api('getVariable', {
            key: 1505,
            user_id: User.currentVkId
        }, function (data) {
            if (data.response) {
                clicksCount = data.response;
            }
        });

        Vo.api('putVariable', {
            key: 1505,
            user_id: User.currentVkId,
            value: Number(User.clicksCount) + Number(clicksCount)
        }, function (data) {});

        User.clicksCount = 0;

        return;
    }

    Vo.api('getVariable', {
        key: 1505,
        user_id: User.vkId
    }, function (data) {
        if (data.response) {
            User.inClicksCount += Number(data.response);

            if (User.inClicksCount) {
                Vo.api('putVariable', {
                    key: 1505,
                    user_id: User.vkId,
                    value: '0'
                }, function (data) {
                    if (data.response) {
                        User.inClicksCount += Number(data.response);
                    }
                });
            }
        }
    });
}, 5000);

setInterval(function () {
    if (User.home && User.inClicksCount) {
        Effects.scoreSprite(Number(User.incValue), true, 650 + Math.random() * 200, 250 + Math.random() * 200, {
            color: '#ffbc47'
        });
        //$('.js-moon').trigger('mousedown');
        User.moonMoney();
        User.inClicksCount--;
    }
}, 100);

User.updated = intval((new Date()).getTime());

User.update = function () {
    if (User.home) {
        var now = intval((new Date()).getTime());
        User.money += Number(User.incValue - 1) * (now - User.updated) / 1000;//Fabric.calculateProfit(User.fabrics);
        User.updated = now;
    } else {
        if (User.currentIncValue) {
            //User.friendMoney += Number(User.currentIncValue);
        }
    }
};

User.updateInterval = setInterval(User.update, 100);
User.renderInterval = setInterval(User.render, 50);
User.saveInterval = setInterval(function () {
    if (!User.home) return;
    App.api('user.money', {
        money: Math.ceil(User.money * 100) / 100,
        clicks: User.clicks || 0
    }).done(function (response) {
        if (response.energy) {
            User.energy = response.energy;
        }

        //$('.js-place').html(response.place);
        //var data = {};
        //for (var i = 0; i < response.users.length; i++) {
        //    data[response.users[i].vk_id] = response.users[i].money;
        //}

        //Vk.Users.get({user_ids: Object.keys(data), fields: 'first_name,photo_100'}, function (users) {
        //    users = users.all();
        //    var $users = $('.js-user');
        //    var $left = $($users.get(0));
        //    var $center = $($users.get(1));
        //    var $right = $($users.get(2));
        //    if (users[0]) {
        //        $right.find('.js-name').html(users[0].first_name);
        //        $right.find('.js-photo').attr('src', users[0].photo_100);
        //        $right.find('.js-money').attr('data-number', data[users[0].id]);
        //        $right.show();
        //    } else {
        //        $right.hide('slow');
        //    }
        //
        //    if (users[1]) {
        //        $left.find('.js-name').html(users[1].first_name);
        //        $left.find('.js-photo').attr('src', users[1].photo_100);
        //        $left.find('.js-money').attr('data-number', data[users[1].id]);
        //        $left.show();
        //    } else {
        //        $left.hide('slow');
        //    }
        //});
    });
}, 5000);
