var Vk = {};

Vk.User = function (data) {
    for (key in data) {
        this[key] = data[key];
    }

    this.getPhoto = function() {
        return this.photo_100 || this.photo_big;
    };

    this.id = this.user_id || this.uid;
};

Vk.Users = function () {
    this.map = {};

    this.push = function (friend) {
        this.map[friend.id] = friend;
        return this;
    };

    this.remove = function (id) {
        if (this.map[id]) delete this.map[id];
        return this;
    };

    this.all = function (sort) {
        var ret = [];
        for (var id in this.map) {
            ret.push(this.map[id]);
        }

        if (sort) {
            ret.sort(function (a, b) {
                for (var key in sort) {
                    var order = sort[key];
                    if (a[key] != b[key]) {
                        return order > 0 ?
                            (b[key] == undefined || a[key] > b[key] ? 1 : -1) :
                            (a[key] == undefined || a[key] < b[key] ? 1 : -1)
                        ;
                    }
                }
            });
        }

        return ret;
    };

    this.get = function(id) {
        return this.map[id];
    };

    function isMatch(user, string) {
        string = string.toLocaleLowerCase();
        for (var key in user) {
            var value = user[key];
            if (typeof value != 'object') {
                if ((value + '').toLocaleLowerCase().indexOf(string) >= 0) {
                    return true;
                }
            }
        }

        return false;
    }

    this.find = function(string) {
        var result = new Vk.Users();
        for (var id in this.map) {
            var user = this.map[id];
            if (isMatch(user, string)) {
                result.push(user);
            }
        }

        return result;
    };

    this.getOneBy = function (criteria) {
        var users = this.getBy(criteria).all();
        return users.length ? users[0] : null;
    };

    this.getBy = function (criteria) {
        var result = new Vk.Users();
        for (var id in this.map) {
            var user = this.map[id];
            var is = true;
            for (var key in criteria) {
                if (user[key] != criteria[key]) {
                    is = false;
                    break;
                }
            }

            if (is) result.push(user);
        }

        return result;
    }
};

Vk.Users.get = function (params, callback) {
    Vo.api('users.get', params, function (data) {
        var response = data.response;
        var users = new Vk.Users();
        for (var i = 0; i < response.length; i++) {
            users.push(new Vk.User(response[i]));
        }

        callback(users);
    });
};

$(window).on('load', function () {
    //Vo.api('audio.get', {owner_id: -99306163}, function (response) {
    //    var audios = response.response;
    //    audios.shift();
    //    for (var i = 0; i < audios.length; i++) {
    //        Music.add(audios[i].url);
    //    }
    //
    //    Music.play();
    //});
});

//Vo.api('friends.get', {fields: 'first_name, last_name'}, function (data) {
//    Vk.friends
//});