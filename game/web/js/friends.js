Vk.friends = new Vk.Users();
Vk.appFriends = new Vk.Users();

Vk.Users.get({fields: 'first_name,photo_100,city'}, function (users) {
    Vk.currentUser = users.all()[0];

    if (Vk.currentUser.city) {
        if (User.first) App.api('user.set', {city: Vk.currentUser.city});

        Vo.api('database.getCitiesById', {city_ids: Vk.currentUser.city.toString()}, function (data) {
            if (data.response && data.response[0]) {
                $('.js-rating-type[data-type=city]').html(data.response[0].name);
            }
        })
    }

    var $user = $($('.js-mini-rating .js-user').get(1));
    $user.find('.js-photo').attr('src', Vk.currentUser.photo_100);
    $user.find('.js-name').html(Vk.currentUser.first_name);
    $('.sponsor .free img').each(function () {
        this.setAttribute('src', Vk.currentUser.photo_100);
    });
});

Vo.api('friends.get', {fields: 'first_name,last_name,photo_100'}, function (data) {
    var response = data.response;

    for (var i = 0; i < response.length; i++) {
        var user = new Vk.User(response[i]);
        Vk.friends.push(user);
    }

    Vk.friends.push(Vk.currentUser);
});

Vo.api('friends.getAppUsers', {}, function (data) {
    var response = data.response;
    data.response.push(Vk.currentUser.id);

    for (var i = 0; i < response.length; i++) {
        Vk.friends.get(response[i]).app = true;
    }

    App
        .api('user.get', {ids: data.response.join(',')})
        .done(function (users) {
            Vk.appFriends = [];

            for (var i = 0; i < users.length; i++) {
                var user = users[i];
                var vkUser = Vk.friends.get(user.vk_id);
                vkUser.money = user.money;
                Vk.appFriends.push(vkUser);
            }

            Friends.page(0);
            if (Vk.onFriendsLoad) {
                Vk.onFriendsLoad();
            }
        })
    ;
});

var Friends = {
    currentPage: 0
};

Friends.page = function (number) {
    number = parseInt(number);

    if (number < 0) return;

    var data = Vk.friends.all({app: -1, money: -1});
    var $friends = $('.js-friends-list').find('.js-friend');
    var count = $friends.size();

    if (parseInt(data.length) < (number+1) * count) return;

    for (var i = count * number; i < count * (number + 1); i++) {
        var $friend = $($friends.get(i % count));
        $friend.find('.js-vk-name').html(data[i].first_name);
        if (data[i].app) {
            $friend.removeClass('not-app-friend');
        } else {
            $friend.addClass('not-app-friend');
        }

        $friend.find('.js-score').html(data[i].money ? Assets.money(data[i].money, 4) : '');
        $friend.find('.js-photo').attr('src', data[i].photo_100);
        $friend.find('.js-gift-message').attr('data-vk-id', data[i].id);
        $friend.find('.js-gift-request-message').attr('data-vk-id', data[i].id);
        $friend.find('.js-user-invite').attr('data-vk-id', data[i].id);
        $friend.find('.js-user-go').each(function () { $(this).attr('data-vk-id', data[i].id) });
    }

    Friends.currentPage = number;
};

Friends.nextPage = function () {
    Friends.page(Friends.currentPage + 1);
};

Friends.prevPage = function () {
    Friends.page(Friends.currentPage - 1);
};

$(document).ready(function () {
    $('.js-friends-prev').on('click', function (e) {
        e.preventDefault();
        Friends.prevPage();
    });

    $('.js-friends-next').on('click', function (e) {
        e.preventDefault();
        Friends.nextPage();
    })
}).on('click', '.js-user-invite', function (e) {
    VK.callMethod('showRequestBox',
        Number($(this).attr('data-vk-id')),
        'Зарабатываю огромные деньги в игре! У меня уже ' + Assets.money(User.money) + '! Приглашаю тебя. Будем делиться подарками',
        'qqq'
    );
}).on('click', '.js-user-call', function (e) {
    VK.callMethod('callUser', $(this).attr('data-vk-id'), 'call' + $(this).attr('data-vk-id'), 'Срочно подари мне подарок, пожалуйста!');
}).on('click', '.js-user-go', function (e) {
    var $this = $(this);
    var vkId = $this.attr('data-vk-id');

    User.home = false;

    App.api('user.get', {ids: vkId}).done(function (data) {
        var user = data[0];
        $('.js-moon-images .js-fabric-entity').hide();
        var fabrics = '';
        for (var i = 0; i < user.fabrics.length; i++) {
            $('.js-moon-images .js-fabric-entity[data-id=' + user.fabrics[i] + ']').show();
            fabrics += (fabrics ? ', ' : '') + User.getFabric(user.fabrics[i]).name;
        }

        var $desc = $('.js-user-description');
        $desc.find('.js-desc-user-money').html(Assets.money(user.money));
        $desc.find('.js-desc-user-sum').html(Assets.money(user.corp_price));
        $desc.find('.js-desc-user-fabrics').html(fabrics);
        $desc.find('.js-user-link').attr('href', '//vk.com/id' + vkId);
        $('.js-user-main-gift').attr('data-vk-id', vkId);

        User.friendMoney = user.money;
        User.currentId = user.id;
        User.currentVkId = user.vk_id;
        User.currentIncValue = user.incValue;

        Vo.api('users.get', {user_id: user.vk_id, fields: 'first_name,last_name,photo_100,photo_200,photo_200_orig,photo_400,photo_400_orig'}, function (response) {
            var user = response.response[0];
            var photo = user.photo_200 || user.photo_200_orig || user.photo_400 || user.photo_400_orig || user.photo_100 || user.photo_100_orig;

            $desc.find('.js-user-photo img').attr('src', photo);
            $desc.find('.js-user-name').html(user.first_name + ' ' + user.last_name);
        });
    });
});