var Templates = {};

Templates.fabricCount = '\
<span>\
%count%\
<img src="//env-0035871.jelastic.regruhosting.ru/dev/gagarin/game/web/images/icons/gagarin_icon%index%.png"/>\
\
</span>\
';

Templates.createFabricCount = function (index, count) {
    return Assets.render(Templates.fabricCount, {
        index: Number(index) + 1,
        count: count
    });
};

Templates.leadProfile = '\
<div class="lead-profile js-user-go js-page-btn" data-page="user-page" data-vk-id="%vkId%">\
    <div class="lead-cell place">\
        %place%\
    </div>\
    <div class="profile-photo">\
        <img src="%photo%" />\
    </div>\
    <div class="lead-cell name">\
        %name%\
    </div>\
    <div class="lead-cell name">\
        %points%\
    </div>\
</div>\
';

Templates.createLeadProfile = function (data) {
    return Assets.render(Templates.leadProfile, {
        place: data.place,
        photo: data.photo,
        name: data.name,
        points: data.points,
        vkId: data.vk_id
    });
};

Templates.button = '<a class="btn-white" href="%href%" %attrs%>%name%</a>';

Templates.createButton = function (name, href, attrs) {
    return $(Assets.render(Templates.button, {
        name: name,
        attrs: attrs || '',
        href: href || '#'
    }));

};

Templates.giftRow = '\
<tr>\
<td><img src="%image%" /></td>\
<td class="box-text"><a class="link" href="//vk.com/id%vkId%" target="_blank">%name%</a> %text%</td>\
<td><a href="#" class="js-gift-message btn-white" data-vk-id="%dataVkId%">Подарить подарок</a></td>\
</tr>\
';

Templates.createGiftsBox = function (data) {
    var str = '<table class="box-table">';
    for (var i = 0; i < data.length; i++) {
        var row = data[i];
        str += Assets.render(Templates.giftRow, {
            image: row.user.photo_100,
            name: row.user.first_name,
            text: (row.user.sex == 1 ? 'подарила' : 'подарил') + ' подарок: "' + ProductRegistry.getProduct(row.index).name + '"',
            vkId: row.user.id,
            dataVkId: row.user.id
        });
    }
    str += '</table>';

    return $(str);
};

Templates.createProductTimer = function(product) {
    return $(Assets.render(Templates.productTimer, {
        id: product.id,
        name: product.name,
        time: product.remainTime
    }));
};

Templates.fabricEntity = '\
<img class="fabric-entity js-fabric-entity" src="%image%" style="left: %x%px; top: %y%px; transform: rotate(%angle%deg); -ms-transform: rotate(%angle2%deg); -webkit-transform: rotate(%angle3%deg);" data-id="%id%"/>\
';

Templates.createFabricEntity = function (fabric) {
    return $(Assets.render(Templates.fabricEntity, {
        image: '//env-0035871.jelastic.regruhosting.ru/dev/gagarin/game/web/images/base/' + fabric.id + '.png',
        x: fabric.coords.x,
        y: fabric.coords.y,
        angle: fabric.coords.angle,
        angle2: fabric.coords.angle,
        angle3: fabric.coords.angle,
        id: fabric.id
    }));
};

Templates.product =
'\
<a href="#" class="product js-product" data-index="%index%" data-name="%name%" data-price="%price%" data-market-price="%marketPrice%" data-description="%description%" data-id="%id%">\
    <div class="timer js-timer" data-left="%remainTime%"></div>\
    <span class="count js-count" data-count="%count%">%count%</span>\
    <img src="%image%"/>\
</a>'
;

Templates.createProduct = function (product) {
    return $(Assets.render(Templates.product, {
        id: product.id,
        name: product.name,
        index: product.index,
        description: product.description,
        count: product.count,
        remainTime: product.remainTime,
        price: product.price,
        marketPrice: product.marketPrice,
        image: '//env-0035871.jelastic.regruhosting.ru/dev/gagarin/game/web/images/product/' + product.id + '.png'
    }));
};

Templates.simpleProduct =
'\
<a href="#" class="product %classname%" data-name="%name%" data-id="%id%">\
    <img width="40" height="40" src="%image%"/>\
</a>\
'
;

Templates.createSimpleProduct = function (product, classname) {
    classname = classname || '';
    return $(Assets.render(Templates.simpleProduct, {
        id: product.id,
        name: product.name,
        index: product.index,
        image: '//env-0035871.jelastic.regruhosting.ru/dev/gagarin/game/web/images/product/' + product.id + '.png',
        classname: classname
    }));
};

Templates.Fabric =
'<div class="item js-item" data-index="%id%">\
    <div class="item-header">\
        <div class="item-header-content js-item-info-btn">\
            <div class="item-icon">\
                <img src="%image%" class="js-icon"/>\
            </div>\
            <div class="item-header-container">\
                <div class="item-title js-title">%name%</div>\
                <div class="item-price js-price js-dynamic-number" money="4" data-number="%price%">0</div>\
            </div>\
            <div class="item-count js-count">%count%</div>\
        </div>\
        <div class="item-info-btn js-item-buy">+</div>\
    </div>\
    <div class="item-content" style="display: none;">\
        <div class="item-description js-description">%description%</div>\
            <div class="item-footer">\
                <div class="item-footer-block">\
                    <strong>ОДНА:</strong>\
                    <span><span class="js-profit-one">%profit%</span>/СЕК</span>\
                </div>\
            <div class="item-footer-block">\
                <strong>ВСЕ:</strong>\
                <span><span class="js-profit-all">%profit_all%</span>/СЕК</span>\
            </div>\
        </div>\
    </div>\
    <div class="btn-buy-container">\
        <button class="btn-white btn-buy js-fabric-votes-buy" data-index="%data_index%">Купить</button>\
        <div class="fabric-votes-price">%votes_price%</div>\
    </div>\
</div>';

Templates.createFabric = function (fabric, votesPrice) {
    fabric.price = PriceGenerator.getPrice(fabric.id, fabric.count);
    return $(Assets.render(Templates.Fabric, {
        id: fabric.id,
        name: fabric.name,
        description: fabric.description,
        price: fabric.getPrice(),
        profit: Assets.money(fabric.profit, 4),
        count: fabric.count,
        image: '//env-0035871.jelastic.regruhosting.ru/dev/gagarin/game/web/images/icons/gagarin_icon' + (parseInt(fabric.id)+1) + '.png',
        votes_price: (votesPrice || 1) + ' ' + Assets.pluralize(votesPrice || 1, 'голос', 'голоса', 'голосов'),
        data_index: fabric.id,
        profit_all: Assets.money(Math.ceil(fabric.count * fabric.profit * 100) / 100, 4)
    }));
};

Templates.mission = '\
<div class="mission js-mission" data-id="%id%"><img src="%image%" width="100%" height="100%"/></div>\
';

Templates.createMission = function (mission) {
    return Assets.render(Templates.mission, {
        id: mission.id,
        name: mission.name,
        image: mission.image
    });
};

Templates.sprite = '<div class="sprite">%value%</div>';

Templates.createSprite = function (value) {
    return $(Assets.render(Templates.sprite, {value: value}));
};