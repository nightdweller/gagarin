var Effects = {};

Effects.scoreSprite = function (number, format, x, y, css) {
    if (format == undefined) {
        format = true;
    }

    var $sprite = Templates.createSprite('+ ' + (format ? Assets.money(number, 4)  : number));
    $(document.body).append($sprite);

    $sprite.css({
        left: (x || (Mouse.x - $sprite.outerWidth() / 2)) + 'px',
        top: (y || Mouse.y) + 'px'
    });

    if (css) {
        $sprite.css(css);
    }

    $sprite.animate({
        top: ((y || Mouse.y) - 200) + 'px',
        opacity: 0
    }, 'slow', function () {
        $sprite.remove();
    });
};