var Level = function (id, name, data) {
    this.id = id;
    this.name = name;
    this.data = data;
};

Level.prototype.fabricCheck = function () {
    var count = 0;
    var successCount = 0;
    for (var i = 0; i < User.fabrics.length; i++) {
        var fabric = User.fabrics[i];

        if (this.data[fabric.id]) {
            if (fabric.count) {
                successCount += Math.min(this.data[fabric.id], Number(fabric.count));
            }
        }
    }

    for (var key in this.data) {
        count += Number(this.data[key]);
    }

    return Math.floor(100 * successCount / (count || 1));
};

var LevelRegistry = {
    last: null
};

Level.current = null;

LevelRegistry.add = function (id, name, data) {
    var level = new Level(id, name, data);
    if (!Level.current) {
        Level.current = level;
    }

    if (LevelRegistry.last) {
        LevelRegistry.last.next = level;
    }

    LevelRegistry.last = level;
};

Level.render = function () {
    var next = Level.current.next;
    var status = next.fabricCheck();
    var $fabrics = $('.js-level-fabrics');
    var data = next.data;
    $fabrics.html('');

    for (var index in data) {
        var count = data[index];
        $fabrics.append(Templates.createFabricCount(index, count));
    }

    $('.js-level-name').html(next.name);
    $('.js-level-status').html(status + '%');
    $('.js-level-status-bar').css({width: status + '%'});
};

Level.update = function () {
    var status =  Level.current.next.fabricCheck();
    if (status >= 99) {
        if (Level.current.id == 2) {
            Game1.start(function () {
                Level.current = Level.current.next;

                App.api('user.level', {level: Level.current.id});
                Level.update();
            });
        } else {
            Level.current = Level.current.next;

            App.api('user.level', {level: Level.current.id});
            Level.update();
        }
    }

    setTimeout(Level.render, 300);
};

LevelRegistry.init = function () {
    //Level.render();
};

$(document).ready(function () {
    setTimeout(Level.update, 2000);
});
