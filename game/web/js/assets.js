function intval(value) {
    if (value === true) return 1;

    return parseInt(value) || 0;
}

var Assets = {};

Assets.pluralize = function (number, one, two, five) {
    if ('array' == typeof one) {
        two = one[1];
        five = one[2];
        one = one[0];
    }

    if (number == 1) return one;

    var p = number % 10;
    if (4 < p || !p || (9 < number && number < 20)) return five;

    return two;
};

Assets.money = function (number, digits) {
    if (number < 10) {
        return Math.ceil(number * 100) / 100 + ' $';
    }

    digits = digits || 7;
    var word = Assets.numberWord(Math.ceil(number));
    return (word ? Assets.prettyNumber(number).substr(0, digits) : Assets.prettyNumber(number)) + ' ' + word + ' $';
};

Assets.prettyNumber = function (number) {
    number = Math.ceil(number).toString();

    if (number.length > 6) {
        var delimeter = (number.length) % 3;
        delimeter = delimeter || -1;
        var space = 3;

        number = number.substr(0, 6);

        result = '';
        for (var i = 0; i < number.length; i++) {
            result += (i == delimeter ? ',' : '') + (space == i ? ' ' : '') + number.charAt(i);
        }

        return result;
    }

    if (number.length > 3) {
        number = number;
        var space = number.length % 3 ? number.length % 3 : 3;
        return number.substr(0, space) + ' ' + number.substr(space, number.length);
    }

    return number;
};

Assets.numberWord = function (number) {
    number = number.toString();

    if (number.length > 18) return 'КВТН';

    if (number.length > 15) return 'КВЛН';

    if (number.length > 12) return 'ТРЛН';

    if (number.length > 9) return 'МЛРД';

    if (number.length > 6) return 'МЛН';

    return ''
};

Assets.render = function (string, data) {
    data = data || {};
    for (var key in data) {
        //console.log(string);
        string = string.replace('%' + key + '%', data[key]).replace('%' + key + '%', data[key]);
    }

    return string;
};