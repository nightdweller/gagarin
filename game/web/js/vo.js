var Vo = {
    INTERVAL: 400,
    queue: [],
    lastResponse: 0,
    block: false
};

Vo.api = function (method, params, callback) {
    if (App.scheme != 'http') {
        params.https = 1;
    }

    Vo.queue.push({method: method, params: params, callback: callback});
    Vo.update();

    return Vo;
};

Vo.update = function (failure, ver) {
    var time = (new Date()).getTime();

    if (!VK._inited) {
        return;
    }

    if (!Vo.block && time - Vo.lastResponse > Vo.INTERVAL && Vo.queue.length) {
        var params = Vo.queue.shift();

        VK.api(params.method, params.params, function (data) {
            Vo.lastResponse = time;
            Vo.block = false;
            setTimeout(Vo.update, Vo.INTERVAL);

            params.callback(data);
        });

        Vo.block = true;
    }
};

VK.init(Vo.update);