var Gift = {};

Gift.send = function (vkId, product) {
    App.api('gift.send', {
        vkId: vkId,
        type: 'products',
        index: product.id
    }).done(function (data) {
        if (data <= 0) {
            Message.close();
            Message.show('%username%, нельзя отправить больше 5 подарков друзьям :(', 'Упс');
            return ;
        }

        VK.callMethod('showRequestBox',
            Number(vkId),
            'Дарю тебе подарок в космосе: ' + product.name + '!',
            'qqq'
        );

        Message.close();
        Message.show('Подарок отправлен! Сегодня можно отправить ещё ' + data);
    });
};

Gift.request = function (vkId, product) {
    VK.callMethod('showRequestBox',
        Number(vkId),
        product.name + ' - подари мне подарок, пожалуйста. Спасибо!',
        'qqq'
    );
};

Gift.showMessage = function (vkId, className) {
    className = className || 'js-gift-send';
    var productIds = ['click_power', 'moon_party', 'fabric_price'];
    var $message = $('<div style="text-align: center;"></div>');
    for (var i = 0; i < productIds.length; i++) {
        var $gift = Templates.createSimpleProduct(ProductRegistry.getProduct(productIds[i]), className);
        $gift.attr('data-vk-id', vkId);
        $message.append($gift);
    }

    $message.find('.js-gift-send');

    Message.show($message.html(), 'Выбери подарок')
};

$(document).on('click', '.js-gift-send', function () {
    var $this = $(this);
    var vkId = $this.attr('data-vk-id');

    Gift.send(vkId, ProductRegistry.getProduct($this.attr('data-id')));
}).on('click', '.js-gift-request', function () {
    var $this = $(this);
    var vkId = $this.attr('data-vk-id');

    Gift.request(vkId, ProductRegistry.getProduct($this.attr('data-id')));
}).on('click', '.js-gift-message', function () {
    var $this = $(this);
    var vkId = $this.attr('data-vk-id');

    Gift.showMessage(vkId);
}).on('click', '.js-gift-request-message', function () {
    var $this = $(this);
    var vkId = $this.attr('data-vk-id');

    Gift.showMessage(vkId, 'js-gift-request');
});
