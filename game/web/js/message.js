var Message = {
    queue: [],
    isOpen: false,
    isReady: false
};

Message.show = function (content, title, buttons, className) {
    if (Vk.currentUser) {
        content = Assets.render(content, {username: Vk.currentUser.first_name});
    }

    buttons = buttons || [{name: 'Хорошо'}];
    className = className || '';

    Message.queue.push({
        content: content,
        buttons: buttons,
        title: title || null,
        className: className
    });

    Message.update();

    return Message;
};

Message.close = function (e) {
    if (e) {
        e.preventDefault();
    }

    Message.$buttons.html('');
    Message.$container.addClass('hidden');

    setTimeout(function () {
        Message.$container.hide();
        Sound.play('close');

        Message.isOpen = false;
        Message.update();
    }, 300);

    return Message;
};

Message.update = function () {
    if (Message.isOpen || !Message.isReady) {
        return Message;
    }

    var data = Message.queue.shift();
    if (!data) {
        return;
    }

    Message.$buttons.html('');

    for (var i = 0; i < data.buttons.length; i++) {
        var $button = Templates.createButton(data.buttons[i].name, data.buttons[i].href || '#', data.buttons[i].attrs || '');
        $button.on('click', data.buttons[i].onclick || Message.close);

        Message.$buttons.append($button);
    }

    var $title = Message.$container.find('.js-message-title');
    if (data.title) {
        $title.html(data.title);
        $title.show();
    } else {
        $title.hide();
    }

    Message.$container.removeClass('lg');
    if (data.className) {
        Message.$container.addClass(data.className);
    }

    Message.$container.show();
    Message.$container.find('.js-message-content').html(data.content);

    setTimeout(function () {
        Message.$container.removeClass('hidden');
        Sound.play('message');
    }, 200);
    Message.isOpen = true;

    return Message;
};

$(document).ready(function () {
    Message.$container = $('.js-message');
    Message.$buttons = Message.$container.find('.js-message-buttons');
    Message.isReady = true;
    setTimeout(function () {
        Message.update();
    }, 300);
});

//Message.show('content').show('Сообщение под номером 2', null, [{name: 'Хорошо'}, {name: 'Отмена'}]);