var Sound = {
    sounds: {},
    enabled: true
};

Sound.check = function () {
    for (key in Sound.sounds) {
        if (!Sound.sounds[key]) {
            return false;
        }
    }

    return true;
};

Sound.add = function (id, path) {
    Sound.sounds[id] = false;
    var result = createjs.Sound.registerSound(path, id);
    return Sound;
};

Sound.play = function (id) {
    if (!Sound.enabled) {
        return Sound;
    }

    createjs.Sound.play(id).volume = 0.2;
    return Sound;
};

Sound.onload = function () {};

createjs.Sound.addEventListener('fileload', function (e) {
    if (Sound.sounds[e.id] == undefined) {
        return;
    }

    Sound.sounds[e.id] = true;
    if (Sound.check()) {
        Sound.onload();
    }
});

createjs.Sound.addEventListener('fileerror', function (e) {
    if (Sound.sounds[e.id] == undefined) {
        return;
    }

    delete Sound.sounds[e.id];

    if (Sound.check()) {
        Sound.onload();
    }
});

var Music = {
    list: [],
    current: null
};

Music.add = function (url) {
    var audio = new Audio(url);
    Music.list.push(audio);
    audio.onended = function () {
        Music.play();
    };

    return Music;
};

Music.play = function(index) {
    if (index == undefined) {
        index = parseInt((Math.random() - 0.01) * Music.list.length);
    }

    Music.list[index].play();
    Music.list[index].volume = 0.8;
    Music.current = Music.list[index];
};

$(document).ready(function () {
    var status = true;

    $('.js-music-btn').on('click', function () {
        var $this = $(this);
        status = !status;

        if (status) {
            Music.play();
        } else {
            Music.current.pause();
            Music.current.currentTime = 0.0;
        }

        $this.removeClass('active');
        if (status) {
            $this.addClass('active');
        }
    });

    $('.js-sound-btn').on('click', function () {
        var $this = $(this);
        Sound.enabled = !Sound.enabled;
        $this.removeClass('active');
        if (Sound.enabled) {
            $this.addClass('active');
        }
    })
});