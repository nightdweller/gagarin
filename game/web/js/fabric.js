var Fabric = {
    lastUpdate: moment()
};

Fabric.fabric = function (id, name, description, price, profit, count, fullDescription, coords) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.fullDescription = fullDescription;
    this.price = price;
    this.profit = profit;
    this.count = count || 0;
    this.coords = coords || {x: 0, y: 0, angle: 0};

    this.getPrice = function () {
        if (ProductRegistry.getProduct('fabric_price').isActive()) {
            return this.price / 2;
        }

        return this.price;
    }
};

Fabric.calculateProfit = function (fabrics) {
    var sum = 0;
    var diff = moment().diff(Fabric.lastUpdate) / 1000;
    for (var i = 0; i < fabrics.length; i++) {
        var fabric = fabrics[i];
        sum += fabric.count * fabric.profit * diff;
    }

    Fabric.lastUpdate = moment();

    if (ProductRegistry.getProduct('moon_party').isActive()) {
        sum *= 2;
    }

    return sum;
};

// 160 160
$(document).ready(function () {
    var $images = $('.js-moon-images');
    for (var i = 0; i < User.fabrics.length; i++) {
        var fabric = User.fabrics[i];
        //if (fabric.id == 0) {
        //    for (var j = 0; j < fabric.count; j++) {
        //        var $image = Templates.createFabricEntity(fabric);
        //        $image.css('transform', 'rotate(' + (j * (28 + i) + fabric.id * 5) + 'deg)');
        //        $image.css('padding-top', (160 - 30 * fabric.id) + 'px');
        //        $images.append($image);
        //    }
        //} else {
            var $image = Templates.createFabricEntity(fabric);
            $images.append($image);
        //}

        if (!fabric.count) {
            $image.hide();
        }

        fabric.$entity = $image;

        var $fabric = Templates.createFabric(fabric);
        $fabric.find('.btn-buy-container').remove();
        if (i < 6) {
            $('.js-all-fabrics.page-1').append($fabric);
        } else {
            $('.js-all-fabrics.page-2').append($fabric);
        }
    }

    var $rocket = $('.js-fabric-entity[data-id=1]');

    function goRocket() {
        $rocket.css({
            'transform-origin': '-700px',
            '-ms-transform-origin': '-700px',
            '-webkit-transform-origin': '-700px'
        });

        setTimeout(function () {
            $rocket.css({
                'transform-origin': '0',
                '-ms-transform-origin': '0',
                '-webkit-transform-origin': '0'
            });

            setTimeout(goRocket, 10000);
        }, 5000);
    }

    goRocket();
});

$(document).on('click', '.js-fabric-entity', function () {
    //var id = $(this).attr('data-id');
    //var fabric = User.getFabric(id);
    //var description = fabric.fullDescription;
    //
    //Message.show(description, fabric.name);
});