var Mouse = {x: 0, y: 0};

$(document).ready(function () {
    //$('body').addClass('main');

    if (Vk.hash) {
        App.page(Vk.hash);
    } else {
        App.page('main');
    }

    //$('.js-logo img').mouseFlipTop();
    //$('.js-moon-dynamic').mouseFlipRight(40, 50).mouseFlipTop(40, 50);
    //$('.js-moon-dynamic').clickscale('.moon-moon, .moon-circle');
    //$('.js-grid').backgroundFlipRight(100, 2000).backgroundFlipTop(100, 2000);

    $('.js-moon-dynamic').on('mousedown', function (e) {
        //if ($(e.target).hasClass('fabric-entity')) {
        //    return;
        //}

        //Effects.scoreSprite(User.moonMoney());

        if (User.energy <= 0) {
            Effects.scoreSprite('Нет энергии', false, 650 + Math.random() * 100, 250 + Math.random() * 200);
            return;
        }

        Effects.scoreSprite(User.moonMoney(), true, 650 + Math.random() * 200, 250 + Math.random() * 200);
        User.energy--;
        Sound.play('click2');
        //$('.js-user-clicks').attr('data-number', User.clicks);
    });

    Vo.api('groups.isMember', {group_id: 99306163, user_id: User.vkId}, function (response) {
        if (User.updateBonus('group', response.response)) {
            $('.js-offer-group-li').hide('slow');
        } else {
            $('.js-offer-group-li').show('slow');
        }
    });

    Vo.api('account.getAppPermissions', {}, function (response) {
        if (User.updateBonus('menu', 256 & response.response)) {
            $('.js-offer-menu-li').hide('slow');
        } else {
            $('.js-offer-menu-li').show('slow');
        }
    });
});

$(document).ready(function () {
    var angle = 0;
    var speed = 200;
    $banner = $('.js-planet-banner');
    $banner.on('click', function () {
        angle += speed;
        speed += 200;
        Effects.scoreSprite('', false);

        if (speed > 2500) {
            $banner.hide('slow');
            User.inClicksCount += 300;
            Message.show(
                '\
                    Ты получаешь 300 кликов бесплатно!<br/>\
                    А ещё сможешь играть на твоём смартфоне!\
                ',
                'Дитя Гагарина теперь мобильная!',
                [
                    {name: 'Скачать', onclick: function () {}, attrs: 'target="_blank"', href: 'https://play.google.com/store/apps/details?id=poznavaka.gagarinchild'},
                    {name: 'Позже'}
                ]
            );
        }

        $banner.css('transform', 'rotate(' + intval(angle) + 'deg)');
    });
});

$(document).ready(function () {
    VK.addCallback('onScrollTop', function (top, windowHeight, offsetTop, active) {
        var currentHeight = 920;
        var currentWidth = window.innerWidth;
        var scale = Math.min(1, Math.max(0.8, (windowHeight - 25) / currentHeight));
        App.scale = scale;
        $(document.body).css({
            'transform': 'scale(' + scale + ')',
            '-ms-transform': 'scale(' + scale + ')',
            '-webkit-transform': 'scale(' + scale + ')'
        });

        if (User.safari) {
            return;
        }
        var width = Math.floor(currentWidth * scale);
        var height = Math.floor((currentHeight + 330) * scale);
        VK.callMethod('resizeWindow', width, height);
    });

    VK.callMethod('scrollSubscribe', true);
    VK.callMethod('scrollTop');

    var $rotate = $('.js-rotate');
    var $rotate2 = $('.js-rotate-2');
    var $moon = $('.js-moon-dynamic');
    var $red = $('.js-circle-red');
    var rotation = 0;
    var rotation2 = 0;
    var red = 0;
    var speed = 0;
    var oldClicks = User.clicks;
    $rotate.add($rotate2).css('transition', 'none');
    $rotate.add($rotate2).css('opacity', '0.5');
    setInterval(function () {
        rotation += speed; rotation %= 360;
        rotation2 += speed * 1.1; rotation2 %= 360;
        //var c = Math.sin(rotation / (360 / (2 * 3.14152926)));
        //var scale = 1 - 0.1 * c / (( + speed) / 2);
        $rotate.css('transform', 'rotate(' + rotation2 + 'deg) scale(' + (1.05 + 0.1 * speed / 40) + ')');
        $rotate2.css('transform', 'rotate(' + rotation + 'deg) scale(0.95)');
        $red.css('box-shadow', '0 0 ' + speed + 'px red, inset 0 0 '+speed * 1.5+'px red');
        //$moon.css('transform', 'scale(' + scale.toFixed(3) + ')');
        if (red < 0) red = 0;
        if (red > 120) red = 120;
    }, 30);

    setInterval(function () {
        if (User.clicks > oldClicks) {
            speed++;
        } else {
            if (speed > 0) speed--;
        }

        oldClicks = User.clicks;
    }, 1000);
});

$(window).load(function () {
    VK.callMethod('scrollWindow', 45, 500);
});

App.page = function (page, quick) {
    if (page == App.currentPage) {
        return;
    }

    App.currentPage = page;
    $('.js-page[data-page=' + page + ']').show();

    if (!quick) {
        Sound.play('out');
    }

    setTimeout(function () {
        document.body.className = '';
        setTimeout(function () {
            Sound.play('in');
            document.body.className = page;
        }, page == 'user-page' ? 500 : (quick ? 1 : 500))
    }, quick ? 1 : 100);

    setTimeout(function () {
        $('.js-page[data-page!="' + page + '"]').hide();
        $('.js-page[data-page=main]').show();
    }, quick ? 1 : 1000);
};

$(document).on('mousemove', function (e) {
    Mouse.x = e.pageX * (1 / App.scale);
    Mouse.y = e.pageY * (1 / App.scale);
}).on('click', '.js-offer-friends', function (e) {
    VK.callMethod('showInviteBox');
}).on('click', '.js-offer-menu', function (e) {
    VK.callMethod('showSettingsBox', 256);
}).on('click', '.js-offer-group', function (e) {
    Vo.api('groups.isMember', {group_id: 99306163, user_id: User.vkId}, function (response) {
        if (response.response) {
            $('.js-offer-group-li').hide('slow');
        } else {
            Message.show('Вступи, пожалуйста', 'Отлично!', [
                {
                    name: 'Подписаться',
                    onclick: function (e) {
                        window.open('http://vk.com/vgagarine');
                        Message.close();
                        Message.show('Уже вступил в наши ряды?', 'Поздравляем!', [{
                            name: 'Конечно!',
                            onclick: function (e) {
                                Message.close();
                                Vo.api('groups.isMember', {group_id: 99306163, user_id: User.vkId}, function (response) {
                                    if (User.updateBonus('group', response.response)) {
                                        $('.js-offer-group-li').hide('slow');
                                    }
                                });
                            }
                        }])
                    }
                },
                {name: 'Чуть позже'}
            ]);
        }
    });
}).on('click', '.js-page-btn', function (e) {
    App.page($(this).attr('data-page'));
}).on('click', '.js-get-free-gift', function (e) {
    App.api('user.bonus').done(function (response) {
        var product = ProductRegistry.getProduct(response.id);
        product.count = response.count;
        Effects.scoreSprite(product.name, false);
        Message.show(product.description, product.name, [{'name': 'Хорошо', onclick: function (e) {
            App.page('main');
            Message.close();
        }}]);

        $('.js-free-gift-timer').attr('data-left', 24*60*60)
        $('.js-free-gift-timer').updateTimer();
        $('.js-free-gift-timer').show('fast');
        $('.js-get-free-gift').hide('fast');

        setTimeout(Product.render, 300);
    })
}).on('click', '.js-user-update', function (e) {
    var $fabrics = $('.js-moon-images .js-fabric-entity');
    $fabrics.hide();
    $fabrics.each(function () {
        var $this = $(this);
        var id = $this.attr('data-id');
        if (User.getFabric(id).count) {
            $this.show();
        }
    });

    User.home = true;
});

Vk.onFriendsLoad = function () {
    var count = Vk.appFriends.length;
    var friendsCount = 3;
    if (User.updateBonus('friends', count >= friendsCount)) {
        $('.js-offer-friends-li').hide('slow');
        $('.js-app-friends-count').html('');
    } else {
        $('.js-offer-friends-li').show('slow');
        $('.js-app-friends-count').html('(' + count + '/' + friendsCount + ')');
    }
};

VK.addCallback('onSettingsChanged', function (settings) {
    if (User.updateBonus('menu', 256 & settings)) {
        $('.js-offer-menu-li').hide('slow');
    } else {
        $('.js-offer-menu-li').show('slow');
    }
});

Sound
    .add('click1', 'sounds/click1.wav')
    .add('click2', 'sounds/click2.wav')
    .add('click3', 'sounds/click3.wav')
    .add('message', 'sounds/message.mp3')
    .add('close', 'sounds/close.mp3')
    .add('out', 'sounds/out.mp3')
    .add('in', 'sounds/in.mp3')
    .add('fabric_buy', 'sounds/fabric_buy.mp3')
;

Sound.onload = function () {
};