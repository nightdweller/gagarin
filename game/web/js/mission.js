var Mission = function (id, type, name, description, image, extra, message, status) {
    this.id = id;
    this.type = type;
    this.name = name;
    this.description = description;
    this.image = image;
    this.extra = extra;
    this.message = message;

    this.status = status;
    this.visited = status || false;
};

Mission.TYPE_OTHER = 0;
Mission.TYPE_MONEY = 1;
Mission.TYPE_FABRIC = 2;

Mission.showing = {};

Mission.update = function () {
    var $missions = $('.js-missions').find('.js-mission');

    if ($missions.size() < 3) {
        var size = 3 - $missions.size();

        for (var i = 0; i < size; i++) {
            var mission = MissionRegistry.shift();
            if (null == mission) {
                break;
            }

            $('.js-missions').append(Templates.createMission(mission));
        }

        var $first = $('.js-missions .js-mission:first');
        if (!Mission.showing[$first.attr('data-id')]) {
            $first.trigger('click');
            Mission.showing[$first.attr('data-id')] = true;
        }
    }
};

$(document).ready(function () {
    Mission.update();
}).on('click', '.js-mission', function () {
    var mission = MissionRegistry.get($(this).attr('data-id'));

    Message.show(mission.description, mission.name);
});

Mission.checkFabric = function () {
    var fabric = User.getFabric(this.extra.fabricId);

    return fabric && fabric.count > 0;
};

Mission.checkMoney = function () {
    return (User.money > this.extra.money);
};

var MissionRegistry = {
    missions: []
};

MissionRegistry.push = function (id, type, name, description, image, extra, message, status) {
    var mission = new Mission(id, type, name, description, image, extra, message, status || 0);

    if (mission.type == Mission.TYPE_FABRIC) {
        mission.check = Mission.checkFabric;
    }

    if (mission.type == Mission.TYPE_MONEY) {
        mission.check = Mission.checkMoney;
    }

    MissionRegistry.missions.push(mission);

};

MissionRegistry.get = function (id) {
    for (var i = 0; i < MissionRegistry.missions.length; i++) {
        if (id == MissionRegistry.missions[i].id) {
            return MissionRegistry.missions[i];
        }
    }

    return null;
};

MissionRegistry.shift = function () {
    for (var i = 0; i < MissionRegistry.missions.length; i++) {
        if (!MissionRegistry.missions[i].visited) {
            MissionRegistry.missions[i].visited = true;
            return MissionRegistry.missions[i];
        }
    }

    return null;
};

MissionRegistry.init = function () {
};

setInterval(function () {
    var completeIds = [];

    for (var i = 0; i < MissionRegistry.missions.length; i++) {
        var mission = MissionRegistry.missions[i];
        if (!mission.status && mission.check) {
            if (mission.check()) {
                completeIds.push(mission.id);
                mission.status = true;
                Message.show(mission.message, 'Отлично!');
            }
        }
    }

    if (completeIds.length) {
        App.api('mission.complete', {ids: completeIds.toString()});
    }

    $('.js-mission').each(function () {
        var $this = $(this);
        var mission = MissionRegistry.get($this.attr('data-id'));
        if (mission.status) {
            $this.hide('slow', function () {
                $this.remove();
                Mission.update();
            });
        }

        Mission.update();
    });
}, 2000);