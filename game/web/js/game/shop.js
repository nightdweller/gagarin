var Shop = {};

Shop.buy = function(item) {
};

$(document).on('click', '.js-shop-buy', function (e) {
    e.preventDefault();

    var $this = $(this);
    Shop.buy($this.attr('data-name'), $this.attr('data-count'));
});

Shop.buy = function (name, count) {
    alert(name + ' ' + count);
    App.api('shop.buy', {name: name, count: count}).done(function (data) {
        if (!data.response) {
            Shop.speedBuy(data, function () {
                Shop.buy(name, count);
            });

            return ;
        }

        alert('Поздравляем!');
        User.update();
    });
};

Shop.speedBuy = function(count, callback) {
    var title = 'Нехватает ' + count + ' ' + Assets.pluralize(title, 'монетки', 'монеток', 'монеток') + '!';
    Shop.speedCallback = callback;
    $('.js-speed-buy-title').html(title);
    $('.js-speed-buy').modal('show');
};


$(document).on('click', '.js-buy', function (e) {
    e.preventDefault();
    var $this = $(this);

    VK.callMethod('showOrderBox', {
        type: 'item',
        item: $this.attr('data-name') + '_' + $this.attr('data-count')
    });
});

VK.addCallback('onOrderSuccess', function (order_id) {
    alert('Отлично! Теперь к новым приключениям!');
    User.update();
    if (Shop.speedCallback) {
        $('.js-speed-buy').modal('hide');

        setTimeout(Shop.speedCallback, 1000);
    }
});
