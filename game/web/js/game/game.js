Game = {
    level: 1,
    appId: 1,
    points: 0,
    outPoints: 0,
    messagePage: ''
};

Game.setPoints = function (points) {
    Game.points = points;
};

Game.incPoints = function (val) {
    Game.points = parseInt(Game.points) + val;
    return Game.points;
};

Game.saveScore = function (points) {
    if (points == undefined) {
        return Api.setPoints(Game.points, Game.level);
    }
    return Api.setPoints(points, Game.level);
};

Game.incLevel = function () {
    Game.level++;
    return Api.incLevel(parseInt(Game.level));
};

Game.getOutPoints = function () {
    return parseInt(Game.outPoints).toString();
};

Game.win = function (callback) {
    Game.sendPoints().done(function () {
        Game.incLevel().done(function () {
            if (callback) {
                callback();
            }
        })
    });
};

Game.over = function () {
    Game.sendPoints().done(function () {
        App.page(Game.messagePage, 'green');
    });
};

//setInterval(function () {
//    Game.outPoints += Math.abs(Game.points - Game.outPoints) / 5;
//
//    if (Math.abs(Game.outPoints - Game.points) < 0.5) {
//        Game.outPoints = Game.points;
//    }
//
//    $('.js-user-points').html(parseInt(Game.outPoints));
//}, 100);

Api = {};

Api.setPoints = function (points, level) {
    return App.api('game.score.save', {
        score: points || 0,
        level: level
    });
};

Api.incLevel = function (level) {
    return App.api('game.level.inc', {
        level: level
    });
};