$.fn.timer = function (format, callback, intervalCallback, endCallback) {
    format = format || '%h часов %m минут %s секунд';

    this.each(function () {
        var $this = $(this);

        $this.data('end', moment().add(1000 * $this.attr('data-left')));
        var wasEnd = false;

        $this.data('interval', setInterval(function () {
            var diff = moment.duration($this.data('end').diff(moment()));
            if (intervalCallback) {
                intervalCallback.call($this, diff);
            }

            if ($this.parent().size() == 0 || diff < 0) {
                if (!wasEnd) {
                    if (endCallback) {
                        endCallback.call($this);
                    }

                    wasEnd = true;
                }

                $this.html('');
                return;
            }

            wasEnd = false;
            if (diff < 0) return callback ? callback.call($this) : null;

            $this.html('function' == typeof format ? format(moment.duration(diff)) : format
                .replace('%h', diff.hours())
                .replace('%m', diff.minutes())
                .replace('%s', diff.seconds())
                .replace('%is', Math.ceil(diff.asSeconds()))
            );
        }, 1000));
    });
};

$.fn.updateTimer = function () {
    this.data('end', moment().add(1000 * this.attr('data-left')));
    return this;
};

$.fn.clearTimer = function () {
    this.data('end', -1);
    clearInterval(this.data('interval'));
    return this;
};

$.fn.mouseFlipTop = function (k, radius) {
    k = k || 20;
    radius = radius || 25;
    var $this = this;

    var defPos = $this.position().top;
    var height = window.innerHeight;
    var real = 0;

    setInterval(function () {
        var mul = 0.5 - Mouse.y / height;
        var pos = $this.position().top;
        real = defPos + mul * radius;
        $this.css('top', parseFloat(pos) + (real - parseFloat(pos)) / k);
    }, 30);
    return this;
};

$.fn.mouseFlipRight = function (k, radius) {
    k = k || 20;
    radius = radius || 25;
    var $this = this;

    var defPos = $this.position().left;
    var height = window.innerHeight;
    var real = 0;

    setInterval(function () {
        var mul = 0.5 - Mouse.x / height;
        var pos = $this.position().left;
        real = defPos + mul * radius;
        $this.css('left', parseFloat(pos) + (real - parseFloat(pos)) / k);
    }, 50);
    return this;
};

$.fn.backgroundFlipRight = function (k, radius) {
    k = k || 20;
    radius = radius || 25;
    var $this = this;

    var defPos = 0;
    var height = window.innerHeight;
    var real = 0;

    setInterval(function () {
        var mul = 0.5 - Mouse.x / height;
        var pos = parseFloat($this.css('background-position-x'));
        real = defPos + mul * radius;
        $this.css('background-position-x', (parseFloat(pos) + (real - parseFloat(pos)) / k) + 'px');
    }, 50);
    return this;
};

$.fn.backgroundFlipTop = function (k, radius) {
    k = k || 20;
    radius = radius || 25;
    var $this = this;

    var defPos = 0;
    var height = window.innerHeight;
    var real = 0;

    setInterval(function () {
        var mul = 0.5 - Mouse.y / height;
        var pos = parseFloat($this.css('background-position-y'));
        real = defPos + mul * radius;
        $this.css('background-position-y', (parseFloat(pos) + (real - parseFloat(pos)) / k) + 'px');
    }, 50);
    return this;
};

$.fn.clickscale = function (selector) {
    var s = 1;
    var $this = this;
    this.on('mousedown', function (e) {
        s += (0.8 - s) / 2;
        $this.find(selector).css({
            'transform': 'scale(' + s + ')',
            '-ms-transform': 'scale(' + s + ')',
            '-webkit-transform': 'scale(' + s + ')'
        });
    });

    setInterval(function () {
        s += (1 - s) / 3;
        $this.find(selector).css({
            'transform': 'scale(' + s + ')',
            '-ms-transform': 'scale(' + s + ')',
            '-webkit-transform': 'scale(' + s + ')'
        });
    }, 100);

    return this;
};

