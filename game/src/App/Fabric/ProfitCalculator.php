<?php

namespace App\Fabric;

use App\Entity\User;

class ProfitCalculator
{
    public static $profits = [0.1, 0.4, 1.6, 4.8, 33, 100, 403, 1209, 9999, 29030, 116121, 348000, 2500000];

    /**
     * @param Fabric[] $fabrics
     */
    public static function calculate(User $user)
    {
        $sum = 0;
        $diff = (new \DateTime())->getTimestamp() - $user->getLastUpdate();

        foreach ($user->getFabrics() as $fabric) {
            $sum += $fabric->getCount() * $fabric->getProfit() * $diff / 1000;
        }
    }
}