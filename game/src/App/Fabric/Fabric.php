<?php

namespace App\Fabric;

use App\Product\ProductRegistry;

class Fabric
{
    protected $index;
    protected $name;
    protected $description;
    protected $fullDescription;
    protected $price = 0;
    protected $marketPrice = 0;
    protected $profit = 0;
    protected $count = 0;
    protected $x = 0;
    protected $y = 0;
    protected $angle = 0;

    public function __construct($index, array $params = [])
    {
        $this->index = $index;
        foreach ($params as $key => $value) {
            $this->{$key} = $value;
        }

        if (!isset($params['count'])) {
            $this->setCount(0);
        }

        $this->profit = ProfitCalculator::$profits[$this->index];
    }

    public function getIndex()
    {
        return (int)$this->index;
    }

    public function setIndex($index)
    {
        $this->index = $index;
        return $this;
    }

    public function getFullDescription()
    {
        return $this->fullDescription;
    }

    public function setFullDescription($fullDescription)
    {
        $this->fullDescription = $fullDescription;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getPrice()
    {
        return (int)$this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    public function getMarketPrice()
    {
        return (int)$this->marketPrice;
    }

    public function setMarketPrice($marketPrice)
    {
        $this->marketPrice = $marketPrice;
        return $this;
    }

    public function getProfit()
    {
        return (float)$this->profit;
    }

    public function getAllProfit()
    {
        return $this->count * $this->profit;
    }

    public function setProfit($profit)
    {
        $this->profit = $profit;
        return $this;
    }

    public function getCount()
    {
        return (int)$this->count;
    }

    public function setCount($count)
    {
        $this->count = $count;
        $this->price = PriceGenerator::getPrice($this->index, $this->count);

        return $this;
    }

    public function incCount()
    {
        $this->count++;

        return $this;
    }

    public function getX()
    {
        return $this->x;
    }

    public function setX($x)
    {
        $this->x = $x;
        return $this;
    }

    public function getY()
    {
        return $this->y;
    }

    public function setY($y)
    {
        $this->y = $y;
        return $this;
    }

    public function getAngle()
    {
        return $this->angle;
    }

    public function setAngle($angle)
    {
        $this->angle = $angle;
        return $this;
    }
}