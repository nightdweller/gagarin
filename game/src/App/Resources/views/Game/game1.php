<style>
</style>

<div class="content js-page" data-page="game-1">
    <div class="kalmar">
        <? for ($i = 1; $i <= 9; $i++): ?>
        <img src="<?=$this->fileAsset('images/game1/kalmar-0' . ($i < 7 ? $i : $i - 3) . '.png')?>" class="k<?=$i?>"/>
        <? endfor ?>
    </div>

    <div class="octopus-bar js-octo-bar">
        <div class="level-info">
            <div class="level-name">Октопус</div>
            <div class="level-status" data-left="60"></div>
        </div>
        <div class="status-bar">
            <div class="status" style="width: 30%"></div>
        </div>
    </div>
</div>

<script>
    var Game1 = {
        points: 0,
        value: 1,
        intervals: [],
        setInterval: function (func, interval) {
            Game1.intervals.push(setInterval(func, interval));
        },
        clearIntervals: function () {
            for (var i = 0; i < Game1.intervals.length; i++) {
                clearInterval(Game1.intervals[i]);
            }

            Game1.intervals = null;
            Game1.intervals = [];
        }
    };

        var $k = $('.k4,.k5,.k6');
    Game1.start = function (win) {
        var $k2 = $('.k7,.k8,.k9');
        var $kalmar = $('.kalmar');
        var $octo = $('.js-octo-bar');
        var $status = $octo.find('.level-status');
        var power = 0.14;


        $kalmar.css({
            'transform': 'scale(0.8) rotate(0deg)',
            '-ms-transform': 'scale(0.8) rotate(0deg)',
            '-webkit-transform': 'scale(0.8) rotate(0deg)',
            'transform-origin': '30px 110px',
            '-webkit-transform-origin': '30px 110px',
            '-ms-transform-origin': '30px 110px',
            left: 0
        });

        setTimeout(function () {
            power = 0.2;
        }, 40);

        $status.timer('%s сек', null, null, function () {
            $kalmar.off('mousedown');

            Message.show('\
                Осьминог поглотил луну...<br/>\
                <i>\
                    И встал Медведь,<br/>\
                    Зарычал Медведь,<br/>\
                    И к Большой Реке<br/>\
                    Побежал Медведь.<br/>\
                </i>\
            ', 'Неееет', [{
                name: 'Ещё раз',
                onclick: function () {
                    Game1.clearIntervals();
                    Game1.start();
                    $status.attr('data-left', 60);
                    $status.updateTimer();
                    Message.close();
                }
            }]);
        });

        $kalmar.css({left: 1000, top: 1000});
        $kalmar.animate({left: 490, top: 289}, {duration: 10, easing: 'linear'});
        $kalmar.on('mousedown', function (e) {
            Effects.scoreSprite(1, false);
            Game1.value += Math.random() * (0.1 + power * Game1.value);
            $kalmar.css({
                'transform': 'scale(0.8) rotate(' + 2 * (Game1.value * 2 + 1) * intval(Math.random() * 40 - 20) + 'deg)',
                '-ms-transform': 'scale(0.8) rotate(' + 2 * (Game1.value * 2 + 1) * intval(Math.random() * 40 - 20) + 'deg)',
                '-webkit-transform': 'scale(0.8) rotate(' + 2 * (Game1.value * 2 + 1) * intval(Math.random() * 40 - 20) + 'deg)'
            });

            if (Game1.value > 1.2) {
                Game1.clearIntervals();
                $kalmar.css({
                    'transform': 'scale(0.8) rotate(1000deg)',
                    '-ms-transform': 'scale(0.8) rotate(1000deg)',
                    '-webkit-transform': 'scale(0.8) rotate(1000deg)',
                    'transform-origin': '800px 130px',
                    '-webkit-transform-origin': '800px 130px',
                    '-ms-transform-origin': '800px 130px',
                    left: 0
                });

                setTimeout(function () {
                    App.page('main');
                    $status.clearTimer();
                    Message.show('Октопус ушел. Его прогнали', 'Победа!');
                    win();
                }, 500);

                $kalmar.off('mousedown');
            }
        });

        setTimeout(function () {
            Game1.setInterval(function () {
                var num = intval((Math.random() - 0.01) * 3);
                $($k[num]).css({
                    'transform': 'rotate(' + Math.floor(Math.random() * 40 - 20) + 'deg)',
                    '-ms-transform': 'rotate(' + Math.floor(Math.random() * 40 - 20) + 'deg)',
                    '-webkit-transform': 'rotate(' + Math.floor(Math.random() * 40 - 20) + 'deg)'
                });

                $($k2[num]).css({
                    'transform': 'rotate(' + Math.floor(Math.random() * 40 - 20) + 'deg) scale(-1,1)',
                    '-ms-transform': 'rotate(' + Math.floor(Math.random() * 40 - 20) + 'deg) scale(-1,1)',
                    '-webkit-transform': 'rotate(' + Math.floor(Math.random() * 40 - 20) + 'deg) scale(-1,1)'
                });
            }, 100);

            Game1.setInterval(function () {
                $kalmar.css({
                    'transform': 'scale(0.8) rotate(' + intval(Math.random() * 40 - 20) + 'deg)',
                    '-ms-transform': 'scale(0.8) rotate(' + intval(Math.random() * 40 - 20) + 'deg)',
                    '-webkit-transform': 'scale(0.8) rotate(' + intval(Math.random() * 40 - 20) + 'deg)',
                    top: (380 + Math.random() * 80 - 40) + 'px'
                });
            }, 500);

            var last = (new Date()).getTime();
            Game1.setInterval(function () {
                var now = (new Date()).getTime();
                Game1.value *= 1 - (now - last) / 1300;
                var fixed = intval(Game1.value * 100) + '%';
                last = now;
                $octo.find('.status').css({width: fixed});

            }, 50)
        }, 10);

        App.page('game-1', true);
    };
</script>