<?
/** @var \App\Entity\User $user */
$user = $this->getUser();
?><html>
<head>
    <link href='//fonts.googleapis.com/css?family=Exo+2:300,400,500,600,700,800,900&subset=latin,cyrillic,latin-ext' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,300,700,200&subset=latin,cyrillic,latin-ext' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="<?= $this->fileAsset('css/styles.css?2') ?>" />
    <script src="//vk.com/js/api/xd_connection.js?2"  type="text/javascript"></script>
    <script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>

    <script type="text/javascript"  src="<?=$this->fileAsset('vendor/moment.js')?>"></script>
    <script  type="text/javascript" src="<?=$this->fileAsset('js/jquery.min.js')?>"></script>

    <script src="<?=$this->fileAsset('js/soundjs.min.js')?>"></script>
    <script src="<?=$this->fileAsset('js/sound.js')?>?2"></script>
<!--    <script src="//code.jquery.com/ui/1.10.3/jquery-ui.min.js"></script>-->
    <script  type="text/javascript" src="<?=$this->fileAsset('js/jquery.easing.js')?>"></script>
    <script  type="text/javascript" src="<?=$this->fileAsset('js/assets.js')?>"></script>
    <script  type="text/javascript" src="<?=$this->fileAsset('js/shop.js')?>"></script>

    <script  type="text/javascript" src="<?=$this->fileAsset('js/vo.js')?>"></script>
    <script  type="text/javascript" src="<?=$this->fileAsset('js/vk.js')?>"></script>
    <script  type="text/javascript" src="<?=$this->fileAsset('js/app.js')?>"></script>

    <script  type="text/javascript" src="<?=$this->fileAsset('js/templates.js')?>"></script>
    <script  type="text/javascript" src="<?=$this->fileAsset('js/message.js')?>"></script>
    <script  type="text/javascript" src="<?=$this->fileAsset('js/effects.js')?>"></script>
    <script  type="text/javascript" src="<?=$this->fileAsset('js/level.js')?>"></script>
    <script  type="text/javascript" src="<?=$this->fileAsset('js/level_registry.js')?>"></script>

    <script  type="text/javascript" src="<?=$this->fileAsset('js/jquery.plugins.js')?>"></script>

    <script  type="text/javascript" src="<?=$this->fileAsset('js/all.js')?>"></script>

    <script  type="text/javascript" src="<?=$this->fileAsset('js/product.js')?>"></script>
    <script  type="text/javascript" src="<?=$this->fileAsset('js/product_registry.js')?>"></script>

    <script  type="text/javascript" src="<?=$this->fileAsset('js/user.js')?>"></script>

    <script  type="text/javascript" src="<?=$this->fileAsset('js/fabric.js')?>"></script>
    <script  type="text/javascript" src="<?=$this->fileAsset('js/fabric_registry.js');?>"></script>

    <script  type="text/javascript" src="<?=$this->fileAsset('js/mission.js')?>"></script>
    <script  type="text/javascript" src="<?=$this->fileAsset('js/mission_registry.js');?>"></script>

    <script  type="text/javascript" src="<?=$this->fileAsset('js/rating.js');?>"></script>
    <script  type="text/javascript" src="<?=$this->fileAsset('js/friends.js')?>"></script>
    <script  type="text/javascript" src="<?=$this->fileAsset('js/gift.js')?>"></script>
</head>
<body>
<div class="grid js-grid"></div>
<div class="top">
    <div class="bar">
        <ul>
            <li><a class="js-page-btn" data-page="gifts" href="#">Подарки каждый день</a></li>
            <li><a class="js-page-btn" data-page="fabric-list" href="#">Моя корпорация</a></li>
            <li><a class="js-page-btn" data-page="rating-page" href="#">Герои Вселенной</a></li>
            <li><a class="js-page-btn" data-page="sponsors" href="#">Спонсоры!</a></li>
        </ul>
    </div>
</div>

<? include($this->prepare('Page/user_page.php')); ?>
<? include($this->prepare('Page/main.php')); ?>
<? include($this->prepare('Page/fabric_list.php')); ?>
<? include($this->prepare('Page/gifts.php')) ?>
<? include($this->prepare('Page/rating.php')) ?>
<? include($this->prepare('Page/clicks_rating.php')) ?>
<? include($this->prepare('Page/seconds_rating.php')) ?>
<? include($this->prepare('Page/sponsors.php')) ?>
<? include($this->prepare('Page/bank.php')) ?>
<? include($this->prepare('Page/shop.php')) ?>
<? include($this->prepare('Game/game1.php')) ?>

<a href="#" class="btn-home btn-white js-user-update js-page-btn" data-page="main">
    Вернуться домой
</a>

<div class="bottom">
    <div class="friends">
        <div class="friends-prev-btn js-friends-prev"></div>
        <div class="friends-list js-friends-list">
            <? for ($i = 0; $i < 0; $i++) { ?>
                <div class="friend js-friend" data-id="">
                    <div class="photo"><img src="photo" class="js-photo" /></div>
                    <div class="profile">
                        <span class="name js-vk-name"></span>
                        <span class="level"><span class="js-score"></span></span>
                    </div>
                    <div class="profile-popup">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <h3 class="js-vk-name">Василек</h3>
                                <a href="#" class="js-request js-page-btn js-user-go" data-page="user-page">Прилететь в гости</a>
                                <a href="#" class="js-gift-message">Подарить подарок</a>
                                <a href="#" class="js-gift-request-message">Попросить подарок</a>
                                <a href="#" class="js-user-invite">Позвать в игру</a>
                            </div>
                        </div>
                    </div>
                </div>
            <? } ?>
        </div>
        <div class="friends-next-btn js-friends-next"></div>
    </div>
</div>

<? if ($gifts = $this->params['gifts']) { ?>
    <div class="message-background js-gifts-modal hidden">
        <div class="gifts-box message bar-lt bar-rb js-message-body">
            <div class="message-title js-message-title">Ничеси! Твои друзья подарили тебе подарки!</div>
            <div class="message-content js-message-content">
            </div>
            <div class="message-footer js-message-buttons">
                <a href="#" class="btn-white js-close-btn">Продолжить играть</a>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            var $gifts = $('.js-gifts-modal');
            var giftsData = <?=json_encode($gifts)?>;
            var vkIds = $.map(giftsData, function (row) {
                return row.vk_id;
            });

            Vk.Users.get({user_ids: vkIds, fields: 'first_name,sex,photo_100'}, function (users) {
                for (var i = 0; i < giftsData.length; i++) {
                    giftsData[i].user = users.getOneBy({id: giftsData[i].vk_id});
                }
                var $box = Templates.createGiftsBox(giftsData);
                $gifts.find('.js-message-content').append($box);
                $gifts.find('.js-close-btn').on('click', function () {
                    $gifts.addClass('hidden');
                    setTimeout(function () {
                        $gifts.remove();
                        $gifts = null;
                    }, 1000);
                });
                $gifts.removeClass('hidden');
            });

        });
    </script>
<? } ?>

<div class="loading js-loading" style="display: none;">
    <img src="<?=$this->fileAsset('images/loading.gif')?>" />
</div>

<style>
    .tender .tender-buy, .tender .tender-discount, .tender .tender-money {
        float: left;
        width: 33%;
        text-align: center;
        font-size: 20px;
        line-height: 1.9;
        font-family: 'Exo 2';
    }

    .tender {
        clear: both;
        display: block;
        padding-bottom: 55px;
    }

    .tender .tender-discount, .tender .tender-money {
        font-weight: 500;
    }

    .tender .tender-discount {
        min-height: 1px;
        text-align: left;
    }

    .tender .tender-money {
        text-align: right;
    }
</style>

<div class="message-background js-bank-modal hidden" style="display: none">
    <div class="gifts-box message bar-lt bar-rb js-bank-body"  style="padding-bottom: 30px;">
        <div class="message-content" style="max-height: 500px;">
            <div class="close-btn js-close-btn link">x</div>

            <div class="tender">
                <div class="tender-money">
                    50% энергии
                </div>
                <div class="tender-buy js-energy-votes-buy" data-count="50">
                    <a href="#" class="btn-white">
                        2 голоса
                    </a>
                </div>
                <div class="tender-discount js-energy-buy" data-count="50" data-price="6">
                    <a href="#" class="btn-white">
                        или 6 септимов
                    </a>
                </div>
            </div>

            <div class="tender">
                <div class="tender-money">
                    100% энергии
                </div>
                <div class="tender-buy js-energy-votes-buy" data-count="100">
                    <a href="#" class="btn-white">
                        3 голоса
                    </a>
                </div>
                <div class="tender-discount">
                    <a href="#" class="btn-white js-energy-buy" data-count="100" data-price="9">
                        или 9 септимов
                    </a>
                </div>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.js-bank-modal .js-close-btn').on('click', function () {
            Shop.close();osest('.js-bank-modal');
        });
    }).on('click', '.js-shop-btn', function () {
        Shop.open();
    });
</script>

<div class="message-background js-message hidden" style="display: none;">
    <div class="message bar-lt bar-rb js-message-body">
        <div class="message-title js-message-title"></div>
        <div class="message-content js-message-content">
            ыалщ йлзйла уулщй азлайул з
        </div>
        <div class="message-footer js-message-buttons">
        </div>
    </div>
</div>

<style>
    .light-banner {
        position: absolute;
        bottom: 0;
        width: 100%;
        height: 72px;
        cursor: pointer;
        display: block;
    }

    .light-banner .light {
        position: absolute;
        width: 100%;
        height: 500px;
        bottom: -498px;
        box-shadow: 0 0 10px rgb(0, 0, 0);
        transition: 1s all;
    }

    .light-banner:hover .light {
        box-shadow: 0 0 150px rgb(0, 0, 0);
    }
</style>

<a href="//vk.com/vgagarine" class="light-banner">
    <div class="light"></div>
</a>
<script>
    User.safari = <?= isset($_GET['safari']) ? 1 : 0 ?>;
    Vk.hash = '<?=$_GET['hash']?>';

    $.ajax({url: '<?=$this->fileAsset('test.php')?>'}).done(function (data) {
        if (data == 1) {
            if (!User.safari) {
                Message.show(
                    'Для запуска игры нужно нажать на кнопку. Этим вы согласитесь, что мы не будем за вами следить и т. д.',
                    'Ой, сафари', [
                    {
                        name: 'Кнопка',
                        href: '<?=$this->fileAsset('test2.php')?>',
                        attrs: 'target="_blank"',
                        onclick: function () {
                            Message.close();
                            setTimeout(function () {
                                location.href += '&safari=1';
                            }, 300);
                        }
                    }
                ]);
            }
        }
    })
</script>

<script src="//media.kahoxa.ru/ad_a.js"></script>
<!--<div id="mediaFrame" style="display:none;"></div>-->
<!--<script>-->
<!--    var uidWeb=221075356;-->
<!--</script>-->
<!--<script src="//media.kahoxa.ru/lib.js?6"></script>-->
<div id="vk_post_-99306163_209" style="
    height: 330px;
    width: 1000px;
    position: absolute;
    top: 940px;
    background: none;
"></div>
<script type="text/javascript">
    (function(d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//vk.com/js/api/openapi.js?117"; fjs.parentNode.insertBefore(js, fjs); }(document, 'script', 'vk_openapi_js'));
    (function() {
        if (!window.VK || !VK.Widgets || !VK.Widgets.Post || !VK.Widgets.Post("vk_post_-99306163_209", -99306163, 209, 'BDkwytlYXAd1a_usQ2dEhyvDl5nF', {width: 1000})) setTimeout(arguments.callee, 50);
    }());
</script>

</body>
</html>