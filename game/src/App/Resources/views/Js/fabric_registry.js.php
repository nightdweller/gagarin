//<script>
<? /** @var \App\Fabric\Fabric $fabric */foreach ($this->get('fabrics') as $fabric) { ?>
User.pushFabric(<?=mb_strcut(json_encode([
        $fabric->getIndex(),
        $fabric->getName(),
        $fabric->getDescription(),
        $fabric->getPrice(),
        $fabric->getProfit(),
        $fabric->getCount(),
        $fabric->getFullDescription(),
        ['x' => $fabric->getX(), 'y' => $fabric->getY(), 'angle' => $fabric->getAngle()]
    ], JSON_UNESCAPED_UNICODE), 1, -1)?>);
<? } ?>

var PriceGenerator = {};
PriceGenerator.prices = <?=json_encode(\App\Fabric\PriceGenerator::$prices)?>;

PriceGenerator.getPrice = function (index, count) {
    if (count >= PriceGenerator.prices[index].length) {
        return 888523009277331000;
    }

    return PriceGenerator.prices[index][count];
};

//</script>