//<script>
LevelRegistry.add(0, 'Ничто', {});

<? /** @var \App\Entity\Level $level */
   foreach ($this->get('levels') as $level) {
?>
LevelRegistry.add(<?=mb_strcut(json_encode([
    $level->getId(),
    $level->getName(),
    $level->getCriteria()
], JSON_UNESCAPED_UNICODE), 1, -1)?>);
<? } ?>
LevelRegistry.init();

//</script>
