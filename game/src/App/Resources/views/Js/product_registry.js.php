//<script>
<? /** @var \App\Product\Product $product */
   foreach ($this->get('products') as $product) {
?>
ProductRegistry.pushProduct(<?=mb_strcut(json_encode([
    $product->getId(),
    $product->getName(),
    $product->getDescription(),
    $product->getPrice(),
    $product->getCount(),
    $product->getRemainTime(),
    $product->getMarketPrice()
], JSON_UNESCAPED_UNICODE), 1, -1)?>);
<? } ?>
ProductRegistry.init();

//</script>
