//<script>
<? /** @var \App\Entity\Mission $mission */
   foreach ($this->get('missions') as $mission) {
?>
MissionRegistry.push(<?=mb_strcut(json_encode([
    $mission->getId(),
    $mission->getType(),
    $mission->getName(),
    $mission->getDescription(),
    $mission->getImage(),
    json_decode('{' . $mission->getExtra() . '}'),
    $mission->getMessage(),
    false
], JSON_UNESCAPED_UNICODE), 1, -1)?>);
<? } ?>
MissionRegistry.init();

//</script>
