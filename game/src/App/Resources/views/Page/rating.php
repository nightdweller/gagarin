<div class="content js-page js-rating-page rating-page" data-page="rating-page">
    <div class="rating-choice">
        <div class="rating-header js-rating-type" data-type="all">Мировой топ</div>
        <div class="rating-header js-rating-type" data-type="friends">Друзья</div>
        <div class="rating-header js-rating-type" data-type="city">По городу</div>
    </div>

    <div class="up js-rating-up">
        ВВЕРХ
    </div>

    <div class="lead-board js-lead-board">
    </div>

    <div class="down js-rating-down">
        ВНИЗ
    </div>

    <script>
        (function () {
            var $board = $('.js-rating-page .js-lead-board');

            var Rating = {
                currentPage: 1,
                limit: 5,
                type: 'all'
            };

            Rating.page = function (page) {
                if (page <= 0) {
                    page = 1;
                }

                var params = {page: page};
                if (Rating.type == 'friends') {
                    params.vk_ids = Vk.appFriends.map(function (user) {
                        return user.id;
                    }).join(',');
                }

                if (Rating.type == 'city') {
                    params.city = 1;
                }

                App.loading(function (callback) {
                    App.api('rating.' + Rating.type, {
                        page: page,
                        vk_ids: Vk.appFriends.map(function (user) {
                            return user.id;
                        }).join(',')
                    }).done(function (data) {
                        if (!data.length) {
                            callback();
                            return;
                        }

                        Rating.currentPage = page;

                        var ids = data.map(function (row) {
                            return row[0];
                        });

                        Vk.Users.get({
                            user_ids: ids.toString(),
                            fields: 'first_name,last_name,photo_100'
                        }, function (users) {
                            $board.find('.lead-profile').remove();
                            for (var i = 0; i < data.length; i++) {
                                var user = users.get(data[i][0]);
                                if (user) {
                                    data[i].place = (Rating.currentPage - 1) * Rating.limit + i + 1;
                                    data[i].name = user.first_name + ' ' + user.last_name;
                                    data[i].points = Assets.money(parseInt(data[i][1]));
                                    data[i].photo = user.photo_100;
                                    data[i].vk_id = data[i][0];
                                    var $profile = Templates.createLeadProfile(data[i]);
                                    $board.append($profile);
                                }
                            }

                            callback();
                        })
                    });
                })
            };

            $(document).ready(function () {
                $('.js-rating-page').on('click', '.js-rating-down', function () {
                    Rating.page(1 + Rating.currentPage);
                }).on('click', '.js-rating-up', function () {
                    Rating.page(Rating.currentPage - 1);
                }).on('click', '.js-rating-type', function () {
                    Rating.type = $(this).attr('data-type');
                    Rating.page(1);

                    $('.js-page[data-page=rating-page]').find('.rating-header').removeClass('active');
                    $(this).addClass('active');
                })
            }).on('click', '.js-page-btn[data-page=rating-page]', function () {
                Rating.page(1);
            });
        })();
    </script>
</div>
