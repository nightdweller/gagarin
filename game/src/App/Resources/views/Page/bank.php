<div class="content js-page bank-page" data-page="bank">
    <div class="set bar-lt bar-rb">
        <div class="set-title">
            СТАРТ
        </div>
        <div class="set-price">
            ЗА 2 ГОЛОСА
        </div>
        <div class="set-description">
            <img src="<?=$this->asset('images/goldmoney.png')?>"/> 30 септимов
        </div>
        <div class="set-footer">
            <a href="#" class="btn-white js-tender-buy" data-count="30">Купить</a>
        </div>
    </div>

    <div class="set bar-lt bar-rb set-middle">
        <div class="set-title">
            ПРЕМИУМ
        </div>
        <div class="set-price">
            ЗА 10 ГОЛОСОВ
        </div>
        <div class="set-description">
            <img src="<?=$this->asset('images/goldmoney.png')?>"/> 210 септимов
            <div class="profit">
                Выгода: <br />
                12 септимов
            </div>
        </div>
        <div class="set-footer">
            <a href="#" class="btn-white js-tender-buy" data-count="210">Купить</a>
        </div>
    </div>

    <div class="set bar-lt bar-rb">
        <div class="set-title">
            ЭЛИТА
        </div>
        <div class="set-price">
            ЗА 50 ГОЛОСОВ
        </div>
        <div class="set-description">
            <img src="<?=$this->asset('images/goldmoney.png')?>"/> 1200 септимов<br />
            <div class="profit">
                Выгода: <br />
                90 септимов
            </div>
        </div>
        <div class="set-footer">
            <a href="#" class="btn-white js-tender-buy" data-count="1200">Купить</a>
        </div>
    </div>

<!--    <div class="set bar-lt bar-rb" style="-->
<!--        width: 920px;-->
<!--        height: 160px;-->
<!--        margin-top: 30px;-->
<!--        margin-right: 0;-->
<!--    ">-->
<!--        <div class="set-title">-->
<!--            Бесплатно-->
<!--        </div>-->
<!---->
<!--        <div class="set-footer">-->
<!--            <a href="#" class="btn-white js-tender-free" data-count="10">Купить бесплатно</a>-->
<!--        </div>-->
<!--    </div>-->
</div>

<script>
//    $('.js-tender-free').on('click', function () {
//        VK.callMethod('showOrderBox', {type: 'offers'});
//    })
</script>
