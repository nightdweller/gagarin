<div class="content js-page active" data-page="gifts">
    <div class="page">
        <ul class="todo js-todo">
            <li class="js-offer-friends-li">
                <div class="js-friends-ok todo-status"></div>
                <div class="title">Позови своих друзей <span class="todo-friends-count js-app-friends-count">0</span></div>
                <div class="description">
                    Чтобы играть с ними вместе, дарить друг другу бесплатные подарки!
                    <a href="#" class="btn-white js-offer-friends">Конечно позову!</a>
                </div>
            </li>
            <li class="js-offer-menu-li">
                <div class="js-menu-ok todo-status"></div>
                <div class="title">Добавь игру в своё меню!</div>
                <div class="description">
                    Ты будешь видеть, сколько новых подарков тебе подарили твои друзья!
                </div>
                <a href="#" class="btn-white js-offer-menu">Уже добавляю!</a>
            </li>
            <li class="js-offer-group-li">
                <div class="js-group-ok todo-status"></div>
                <div class="title">Вступи в группу твоей игры!</div>
                <div class="description">
                    Ты всегда будешь в курсе событий и акций, которые происходят!
                    У нас уютно и ты сможешь быстро найти компанию
                </div>

                <a href="#" class="btn-white js-offer-group">Уже вступаю!</a>
            </li>
            <li>
                <div class="title">Бесплатные подарки!</div>
                <div class="description">
                    Ты хорошо потрудился, и как награда, будешь получать раз в день по одному подарку, когда зайдёшь в игру!
                </div>
            </li>
        </ul>
        <a href="#" class="btn-white js-get-free-gift get-free-gift" style="display: none;">Получить бесплатный подарок!</a>
        <div class="free-gift-message js-free-gift-message">
            До следующего подарка осталось
        </div>
        <div class="js-free-gift-timer gifts-left-time" style="display: none;" data-left="<?=$user->getLastGiftDiff()?>"></div>
    </div>
</div>