<div class="content js-page active" data-page="user-page">
    <div class="money-info active">
        <div class="money js-user-page-money">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span class="word"></span>
        </div>
        <div class="money-diff"><span class="js-money-diff js-dynamic-number" money="6" data-number="<?=$this->getUser()->getIncValue()?>"></span>/СЕК</div>
    </div>

    <div class="user-description js-user-description">
        <div class="user-photo js-user-photo">
            <a href="#" class="js-user-link" target="_blank"><img src="" /></a>
        </div>
        <h2 class="js-user-name">Антон Матюх</h2>
        <div class="description">
            <div><strong>Стоимость корпорации: </strong> <span class="js-desc-user-sum">1000$</span></div>
            <div><strong>Деньги: </strong> <span class="js-desc-user-money">1000$</span></div>
            <div><strong>Фабрики: </strong> <span class="js-desc-user-fabrics"></span></div>
        </div>
        <a href="#" class="js-user-main-gift js-gift-message btn-white" style="top: -50px; position: absolute; z-index: 99999; left: 299px;">Подарить подарок</a>
    </div>
</div>