<?
$sponsors = $this->params['sponsors'];
function sponsor($type, $number, $sponsors) {
?>
    <div data-type="<?=\App\Entity\Sponsor::$types[$type]?>" data-index="<?=$number?>"
         class="sponsor <? if (!isset($sponsors[$type][$number])) { ?>js-buy-sponsor free<? } else { ?>js-sponsor js-user-go js-page-btn<?}?>"
         <?if (isset($sponsors[$type][$number])):?>
             data-vk-id="<?=$sponsors[$type][$number]->getVkId()?>"
             data-page="user-page"
         <?endif?>
    >
        <img src="https://pp.vk.me/c622621/v622621356/2cbb6/OXOi4JaPyfE.jpg" />
        <div class="sponsor-content">
            <div class="sponsor-name">
                Здесь можешь быть ты
            </div>
            <div class="sponsor-donate">
                <?if (isset($sponsors[$type][$number])) {?>
                    <?=$sponsors[$type][$number]->getVotes()?>
                <? } else { ?>
                    <?=\App\Product\ProductRegistry::get('sponsor_' . \App\Entity\Sponsor::$types[$type])->getPrice()?>
                <? } ?>
                голосов
            </div>
        </div>
    </div>
<? } ?>

<div class="content js-page active" data-page="sponsors">
    <div class="page">
        <div class="sponsors-container">
            <div class="sponsor-board sponsor-board-big">
                <? for ($i = 0; $i < 4; $i++): ?>
                    <? sponsor(\App\Entity\Sponsor::TYPE_BIG, $i, $sponsors) ?>
                <? endfor ?>
            </div>

            <div class="sponsor-board sponsor-board-medium">
                <? for ($i = 0; $i < 6; $i++): ?>
                    <? sponsor(\App\Entity\Sponsor::TYPE_MEDIUM, $i, $sponsors) ?>
                <? endfor ?>
            </div>

            <div class="sponsor-board sponsor-board-small">
                <? for ($i = 0; $i < 8; $i++): ?>
                    <? sponsor(\App\Entity\Sponsor::TYPE_SMALL, $i, $sponsors) ?>
                <? endfor ?>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('.js-buy-sponsor').on('click', function (e) {
            e.preventDefault();
            var $this = $(this);

            VK.callMethod('showOrderBox', {
                type: 'item',
                item: 'sponsor_' + $this.attr('data-type') + ':' + $this.attr('data-index') + '|1'
            });
        });

        var vkIds = [];
        $('.js-sponsor').each(function () {
            vkIds.push($(this).attr('data-vk-id'));
        });

        Vk.Users.get({user_ids: vkIds.join(','), fields: 'first_name,last_name,photo_100'}, function (users) {
            $('.js-sponsor').each(function () {
                var $this = $(this);
                var user = users.get($this.attr('data-vk-id'));
                $this.find('img').attr('src', user.getPhoto());
                $this.find('.sponsor-name').html(user.first_name + ' ' + user.last_name);
            });
        });

    });
</script>