<div class="content js-page" data-page="main">
    <!--    <div class="logo js-logo"><img src="--><?//=$this->fileAsset('images/gagarin_logo_original.png')?><!--" /></div>-->
    <div class="money-info">
        <div class="money js-user-money">
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span></span>
            <span class="word"></span>
        </div>
        <div class="money-diff js-page-btn" data-page="rating-seconds-page"><span class="js-money-diff js-dynamic-number" money="6" data-number="<?=$this->getUser()->getIncValue()?>"></span>/СЕК</div>
    </div>

    <div class="menu-panel">
        <div class="menu-link">
            <div class="planet-banner">
                <img class="js-planet-banner" src="<?=$this->fileAsset('images/planet.png')?>" />
            </div>
        </div>
    </div>

    <div class="items js-fabrics">
        <div class="products-container js-products-container">
            <div class="settings-panel">
                <a class="link js-music-btn active" href="#"><img src="<?=$this->fileAsset('images/ico_sound-02.png')?>"></a>
                <a class="link js-sound-btn active" href="#"><img src="<?=$this->fileAsset('images/ico_sound-01.png')?>"></a>
                <script type="text/javascript" src="//vk.com/js/api/openapi.js?117"></script>

                <!-- VK Widget -->
                <div class="group-widget js-offer-group-li" id="vk_subscribe"></div>
                <script type="text/javascript">
                    VK.Widgets.Subscribe("vk_subscribe", {mode: 1, soft: 1}, -99306163);
                </script>
            </div>
            <div class="product-tooltip js-product-tooltip">
                <div class="product-header js-header">Название</div>
                <div class="product-description js-description">Описание продукта даёт радость, ощущение жизни, космос, гагарин морж ест колбасу</div>
            </div>
            <a href="#" class="btn-green btn-buy js-buy-btn">Купить</a>
            <div class="products js-products"></div>
            <div class="balance js-balance js-page-btn" data-page="bank">
                <span style="margin-right: 34px;"><?=$user->getBalance()?></span>
                <img src="<?=$this->fileAsset('images/goldmoney.png')?>">
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $('.js-item').find('.item-content').hide();
            $('.js-fabrics, .js-all-fabrics').on('click', '.js-item-info-btn', function () {
                var $content = $(this).closest('.js-item').find('.item-content');
                if ($content.is(':hidden')) {
                    $('.js-item').find('.item-content').slideUp();
                    $content.slideDown();
                } else {
                    $content.slideUp();
                }
            });

            $('.js-fabrics, .js-all-fabrics').on('click', '.js-item-buy', function (e) {
                var $this = $(this);
                var $fabric = $this.closest('.js-item');

                var index = $fabric.attr('data-index');
                var fabric = User.fabrics[index];
                $fabric = $('.js-item[data-index=' + fabric.id + ']');

                if (fabric.getPrice() <= User.money) {
                    Effects.scoreSprite(1, false);
//                    console.log(PriceGenerator);
                    fabric.price = PriceGenerator.getPrice(index, fabric.count);
                    $fabric.each(function () {
                        $(this).find('.item-price').attr('data-number', fabric.getPrice());
                    });
                    Sound.play('fabric_buy');

                    App
                        .api('fabric.buy', {i: index})
                        .done(function (data) {
                            if (data >= 0) {
                                User.money = parseInt(data);
                                User.incValue += fabric.profit;
                                fabric.count++;

                                $fabric.each(function () {
                                    var $this = $(this);
                                    $this.find('.item-count').html(fabric.count);
                                    $fabric.find('.js-profit-all').html(Math.ceil(fabric.profit * fabric.count * 100) / 100);
                                });
                                setTimeout(Level.update, 100);

                                $('.js-money-diff').attr('data-number', User.incValue);
                                fabric.$entity.show('slow');
                            }
                        })
                    ;
                } else {
                    $fabric.find('.js-fabric-votes-buy').trigger('click');
                }
            });
        });

        User.id = <?=$user->getId()?>;
        User.vkId = <?=$user->getVkId()?>;
        User.balance = <?=$user->getBalance()?>;
        User.money = <?=$user->getMoney()?>;
        User.incValue = <?=$user->getIncValue()?>;
        User.home = true;
        User.level = <?=$user->getLevel()?>;
        User.first = <?=$this->params['first'] ? 'true' : 'false'?>;
        User.clicks = <?=$user->getClicks()?>;
        User.energy = <?=$user->getEnergy()?>;
        User.maxEnergy = <?=$user->getMaxEnergy()?>;

        App.scheme = '<?=$this->container->getRequest()->getScheme()?>';

        User.render();

        (function () {
            var $numbers = $('.js-dynamic-number');
            var numbers$;
            numbers$ = [];
            $numbers.each(function () {
                numbers$.push($(this));
            });

            setInterval(function () {
                for (var i = 0; i < numbers$.length; i++) {
                    var $this = numbers$[i];
                    var number = Number($this.attr('data-number')).toFixed(2) || 0;
                    var showNumber = $this.data('show-number') || 0;
                    showNumber += Number(Math.floor((number - showNumber) / 2).toFixed(2));

                    if (showNumber == number) {
                        continue;
                    }

                    if (Math.abs(showNumber - number) < 1) {
                        $this.data('show-number', number);
                        continue;
                    }

                    $this.data('show-number', showNumber);
                    $this.html($this.attr('money') ? Assets.money(showNumber, $this.attr('money')) : showNumber);
                }
            }, 60);

            setInterval(function () {
                $numbers = $('.js-dynamic-number');
                numbers$ = [];
                $numbers.each(function () {
                    numbers$.push($(this));
                });
            }, 400);
        })();

        var $fabrics = $('.js-fabrics');
        var FAB_COUNT = 3;

        var updateFabrics = function () {
            for (var i = User.fabrics.length-1; i >= 0 && User.fabrics[i].count == 0; i--);
            i = Math.max(0, i-1);
            i = Math.min(User.fabrics.length - FAB_COUNT, i);
            var ids = {};
            $fabrics.find('.js-item').each(function () {
                ids[$(this).attr('data-index')] = true;
            });

            var end = Math.min(i+FAB_COUNT-1, User.fabrics.length - 1);

            for (; i <= end; i++) {
                var fabric = User.fabrics[i];
                if (!ids[fabric.id]) {
                    var $fabric = Templates.createFabric(fabric, parseInt(FAB_COUNT - end + i));
                    $fabrics.append($fabric);
                    $fabric.hide();
                    $fabric.show('slow');
                }
            }

            if ($fabrics.find('.js-item').size() > FAB_COUNT) {
                var $first = $($fabrics.find('.js-item').get(0));
                $first.hide('slow', function () {
                    $first.remove();
                });
            }

            $('.js-item').each(function () {
                var $this = $(this);
                if (User.money > User.getFabric($this.attr('data-index')).getPrice()) {
                    $this.removeClass('disabled');
                } else {
                    $this.addClass('disabled');
                }
            });
        };

        setInterval(updateFabrics, 2000);
        updateFabrics();

        Vo.api('users.get', {}, function (data) {
//            console.log(data);
        });
    </script>


    <div class="energy-bar js-energy-bar">
        <div class="level-info">
            <div class="level-name link js-shop-btn">Энергия [пополнить]</div>
            <div class="level-status js-energy-status">30%</div>
        </div>
        <div class="status-bar">
            <div class="status" style="width: <?=$user->getEnergyPercent()?>%"></div>
        </div>
    </div>

    <div class="level-bar">
        <div class="level-info">
            <div class="level-name js-level-name">Ночь дня Гагарина</div>
            <div class="level-name level-fabrics js-level-fabrics">
            </div>
            <div class="level-status js-level-status">30%</div>
        </div>
        <div class="status-bar">
            <div class="status js-level-status-bar" style="width: 30%"></div>
        </div>
    </div>

    <div class="moon">
        <div class="js-moon-dynamic">
            <img class="moon-main moon-moon" src="<?=$this->fileAsset('images/gagarin_moon_4x.png')?>" />
            <div class="moon-main moon-images js-moon-images"></div>
            <img class="moon-main moon-circle" src="<?=$this->fileAsset('images/gagarin_moon_circle2_4x.png')?>" />
            <img class="moon-main moon-circle" src="<?=$this->fileAsset('images/gagarin_moon_circle1_4x.png')?>" />
            <img class="moon-main moon-circle js-rotate" src="<?=$this->fileAsset('images/moon_rotate.png')?>" />
            <img class="moon-main moon-circle js-rotate-2" src="<?=$this->fileAsset('images/moon_rotate.png')?>" />
            <div class="moon-main moon-circle circle-red js-circle-red"></div>
        </div>
    </div>

    <div class="missions js-missions"></div>

    <div class="mini-rating js-friends-list js-mini-rating">
<!--        <div class="place js-page-btn" data-page="rating-page"><span class="js-place">50</span> место</div>-->
        <div class="left-btn js-friends-prev"><img src="<?=$this->fileAsset('images/icons/left.png')?>"></div>
        <?for ($i = 0; $i < 3; $i++) { ?>
            <div class="user js-user js-friend">
                <div class="profile-popup">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <h3 class="js-vk-name">Василек</h3>
                            <a href="#" class="js-request js-page-btn js-user-go" data-page="user-page">Прилететь в гости</a>
                            <a href="#" class="js-gift-message">Подарить подарок</a>
                            <a href="#" class="js-gift-request-message">Попросить подарок</a>
                            <a href="#" class="js-user-invite">Позвать в игру</a>
                        </div>
                    </div>
                </div>
                <div class="name"><span class="js-score js-money" money="4" data-number="10000">0</span></div>
                <div class="photo js-page-btn js-user-go" data-page="user-page">
                    <img class="js-photo" src="http://cs624831.vk.me/v624831207/2b078/w-Y78iRYNAg.jpg" />
                </div>
                <div class="name js-name js-vk-name">Иван</div>
            </div>
        <? } ?>
        <div class="right-btn js-friends-next"><img src="<?=$this->fileAsset('images/icons/right.png')?>"/></div>
    </div>
</div>

<script>
    setTimeout(function () {
        var $station = $('.js-fabric-entity[data-id=2]');
        $station.css({
            'transition': '150s all',
            '-webkit-transition': '150s all'
        });

        var a1, a2;
        a1 = function () {
            setTimeout(function () {
                $station.css({
                    'transform': 'rotate(3600deg)',
                    '-ms-transform': 'rotate(3600deg)',
                    '-webkit-transform': 'rotate(3600deg)'
                });
                setTimeout(a2, 100000);
            }, 1000);
        };

        a2 = function () {
            setTimeout(function () {
                $station.css({
                    'transform': 'rotate(0deg)',
                    '-ms-transform': 'rotate(0deg)',
                    '-webkit-transform': 'rotate(0deg)'
                });
                setTimeout(a1, 100000);
            }, 1000);
        };

        a1();
    }, 1000);
</script>