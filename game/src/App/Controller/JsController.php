<?php

namespace App\Controller;

use App\Fabric\FabricRegistry;
use App\Mission\MissionRegistry;
use App\Product\ProductRegistry;
use Components\Request;

class JsController extends BaseController
{
    public function levelRegistryAction(Request $request)
    {
        $currentLevel = $this->container->getUser()->getLevel();

        $levels = $this
            ->container
            ->getLevelRepository()
            ->createQueryBuilder()
            ->andWhere('id >= :level')
            ->setParameter('level', $currentLevel)
            ->addOrderBy('id', 'ASC')
            ->limit(10)
            ->getQuery()
            ->getResult()
        ;

        return $this->render('Js/level_registry.js.php', [
            'levels' => $levels
        ]);
    }

    public function fabricRegistryAction(Request $request)
    {
        $fabrics = FabricRegistry::all();
        foreach ($this->container->getUser()->getFabrics() as $fabric) {
            $fabrics[$fabric->getIndex()] = $fabric;
        }

        return $this->render('Js/fabric_registry.js.php', [
            'fabrics' => $fabrics
        ]);
    }

    public function productRegistryAction(Request $request)
    {
        $products = ProductRegistry::all();
        foreach ($this->container->getUser()->getProducts() as $product) {
            $products[$product->getId()] = $product;
        }

        return $this->render('Js/product_registry.js.php', [
            'products' => $products
        ]);
    }

    public function missionRegistryAction(Request $request)
    {
        $missions = $this->container->getMissionRepository()->findBy([
            'id' => $this->container->getUser()->getMissions()->getOutset(25)
        ]);

        return $this->render('Js/mission_registry.js.php', [
            'missions' => $missions
        ]);
    }
}