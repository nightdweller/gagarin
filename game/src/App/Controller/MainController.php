<?php

namespace App\Controller;

use App\Entity\User;
use Components\Request;
use Components\Response;

class MainController extends BaseController
{
    public function targetAction(Request $request)
    {
        $query = $this->container->getDB()->getConnection()->query('select * from vk_target_2 LIMIT 900000, 100000');
        $ids = array_map(function ($row) {
            return $row[1];
        }, $query->fetch_all());

        header('Content-type: application/txt');
        header('Content-Disposition: attachment; filename="group.txt"');
        return new Response(implode(';', $ids));
    }

    public function indexAction(Request $request)
    {
        $vkId = $request->get('viewer_id');

        if (!$vkId) {
            throw new \Exception();
        }

        $first = false;

        if (null === $user = $this->container->getUserRepository()->findVk($vkId)) {
            $first = true;
            $user = new User();
            $user->setVkId($vkId);
            $this->container->getUserRepository()->insert($user);
            $this->container->getGiftRepository()->init($user->getId());
        }

        $this->container->getUserToken()->setUser($user);

        if ($product = $user->getProduct('autonomy12')) {
            if ($product->getEndTime() > $user->getLastUpdate()) {
                $diff =  min($product->getEndTime()->getTimestamp(), time()) - $user->getLastUpdate()->getTimestamp();
                $user->setMoney($user->getMoney() + $user->getIncValue() * $diff);
                $user->setLastUpdate(new \DateTime());
                $this->container->getORM()->flush($user);
            }
        }

        $gifts = $this->container->getGiftRepository()->findGifts($user->getId());

        if ($gifts) {
            $this->container->getGiftRepository()->clear($user->getId());
        }

        $sponsors = $this->container->getSponsorRepository()->getMapped();

        if (isset($_SERVER['HTTP_USER_AGENT']) && isset($_SERVER['HTTP_ACCEPT'])) {
            $user->setInfo($_SERVER['HTTP_USER_AGENT'] . '|' . $_SERVER['HTTP_ACCEPT']);
        }

        $this->container->getORM()->flush($user);

        return $this->render('layout.php', [
            'gifts' => $gifts['products'],
            'first' => $first,
            'sponsors' => $sponsors
        ]);
    }
}