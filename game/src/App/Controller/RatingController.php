<?php

namespace App\Controller;

use Components\Request;

class RatingController extends BaseController
{
    public function cityAction(Request $request)
    {
        return $this->render('Rating/city.php');
    }
}