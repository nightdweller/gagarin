<?php

namespace App\Controller;

use App\Entity\User;
use Components\Request;
use Components\Response;

class MobileController extends BaseController
{
    public function initAction(Request $request)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
        header('Access-Control-Allow-Headers: content-type');
//        if (!isset($_COOKIE['minit'])) {
            $this->container->getStatisticRepository()->incShow();
//            setcookie('minit', '1', time() + 1e7, '/');
//
//            return new Response(1);
//        }

        return new Response(1);
    }

    public function installAction(Request $request)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
        header('Access-Control-Allow-Headers: content-type');
//        if (!isset($_COOKIE['minit'])) {
            $this->container->getStatisticRepository()->incInstall();
//            setcookie('minit', '1', time() + 1e7, '/');
//
//            return new Response(1);
//        }

        return new Response(1);
    }
}