<?php

namespace App\Controller;

use App\Entity\User;
use Components\Request;

class PopupController extends BaseController
{
    public function bankAction(Request $request)
    {
        return $this->render('Popup/bank.php');
    }

    public function friendsLifesAction(Request $request)
    {
        return $this->render('Popup/friends_lifes.php');
    }

    public function offersAction(Request $request)
    {
        return $this->render('Popup/offers.php', [
            'word' => 'hello'
        ]);
    }
}