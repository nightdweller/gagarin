<?php

namespace App\Container;

use Admin\Controller\StatisticController;
use App\Entity\Repository\GiftRepository;
use App\Entity\Repository\LevelRepository;
use App\Entity\Repository\RatingRepository;
use App\Entity\Repository\StatisticRepository;
use App\Entity\User;
use Vk\Vk;

class Container extends \Components\Container\Container
{
    public $clientId = '4985569';
    public $clientSecret = 'I7qpSn6oGESUBPXbUdmU';

    public function getRouter()
    {
        return $this->get('router');
    }

    /**
     * @return Vk
     */
    public function getVk()
    {
        return $this->get('vk_api');
    }

    /**
     * @return Vk
     */
    public function getVkSimple()
    {
        return $this->get('vk_api_simple');
    }

    /**
     * @return \Components\Request
     */
    public function getRequest()
    {
        return $this->get('request');
    }

    /**
     * @return \App\Shop\Shop
     */
    public function getShop()
    {
        return $this->get('shop');
    }

    public function getTemplate()
    {
        return $this->get('template');
    }

    /**
     * @return \Components\UserToken
     */
    public function getUserToken()
    {
        return $this->get('user_token');
    }

    /** @return User */
    public function getUser()
    {
        return $this->getUserToken()->getUser();
    }

    /**
     * @return StatisticRepository
     */
    public function getStatisticRepository()
    {
        return $this->get('statistic_repository');
    }

    /**
     * @return \Components\DB\DB
     */
    public function getDB()
    {
        return $this->get('db');
    }

    /**
     * @return \Components\DB\ObjectManager
     */
    public function getORM()
    {
        return $this->get('orm');
    }

    /**
     * @return \App\Entity\Repository\UserRepository
     */
    public function getUserRepository()
    {
        return $this->getORM()->getRepository('App\\Entity\\User');
    }

    /**
     * @return \App\Entity\Repository\SponsorRepository
     */
    public function getSponsorRepository()
    {
        return $this->getORM()->getRepository('App\\Entity\\Sponsor');
    }

    /**
     * @return GiftRepository
     */
    public function getGiftRepository()
    {
        return $this->get('gift_repository');
    }

    /**
     * @return \App\Entity\Repository\PaymentRepository
     */
    public function getPaymentRepository()
    {
        return $this->getORM()->getRepository('App\\Entity\\Payment');
    }

    /**
     * @return \App\Entity\Repository\MissionRepository
     */
    public function getMissionRepository()
    {
        return $this->getORM()->getRepository('App\\Entity\\Mission');
    }

    /**
     * @return LevelRepository
     */
    public function getLevelRepository()
    {
        return $this->getORM()->getRepository('App\\Entity\\Level');
    }

    /**
     * @return \App\Entity\Repository\UpgradeRepository
     */
    public function getUpgradeRepository()
    {
        return $this->getORM()->getRepository('App\\Entity\\Upgrade');
    }

    /**
     * @return RatingRepository
     */
    public function getRatingRepository()
    {
        return $this->get('rating_repository');
    }
}
