<?php

namespace App\Converter;

class MoneyConverter
{
    public static function convert($money)
    {
        return (int)($money / 10);
    }
}