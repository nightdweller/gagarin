<?php

namespace App\EventListener;

use App\Container\Container;
use App\Entity\User;

class EventListener
{
    public static function preHandle(Container $container)
    {
        if (!isset($_SESSION['_user'])) {
            if (null == $uid = $container->getRequest()->get('viewer_id')) {
                return ;
            }

            if (null == $user = $container->getUserRepository()->findOneBy(['vkId' => $uid])) {
                return ;
            }

            $container->getUserToken()->setUser($user);

            return ;
        }

        $user = $_SESSION['_user'];
        if ($user) {
            /** @var User $user */
            $container->getORM()->initInstance($user = unserialize($user));
            $container->getUserToken()->setUser($user);
        }
    }

    public static function postHandle(Container $container)
    {
        if ($container->getUserToken()->getUser()) {
            $user = $container->getUserToken()->getUser();
            $_SESSION['_user'] = serialize($user);
        }
    }
}