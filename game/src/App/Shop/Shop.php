<?php

namespace App\Shop;

use App\Container\Container;
use App\Entity\Payment;
use App\Entity\User;
use App\Product\ProductRegistry;

class Shop
{
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function getInfo($name, $count)
    {
        if (count($explode = explode(':', $name)) > 1) {
            $product = ProductRegistry::get($explode[0], $explode[1]);
        } else {
            $product = ProductRegistry::get($name);
        }

        return [
            'title' => $product->getName(),
            'image' => '',
            'price' => $product->getPrice(),
            'expiration' => 0
        ];
    }

    public function handleVk($name, $count, $id, User $user)
    {
        if (count($explode = explode(':', $name)) > 1) {
            $product = ProductRegistry::get($explode[0], $explode[1]);
        } else {
            if (null === $product = $user->getProduct($name)) {
                $product = ProductRegistry::get($name);
                $user->addProduct($product);
            }
        }

        $itemIndex = $product->buy($this->container);

        $payment = new Payment();
        $payment
            ->setOrderId($id)
            ->setUserId($user->getId())
            ->setType($explode[0])
        ;

        if ($itemIndex) {
            $payment->setIndex($itemIndex);
        }

        $this->container->getPaymentRepository()->insert($payment);
        $this->container->getORM()->flush($user);

        return ['order_id' => $id];
    }

    public function handle($name, $count, User $user)
    {
        if (count($explode = explode(':', $name)) > 1) {
            $product = ProductRegistry::get($explode[0], $explode[1]);
        } else {
            if (null === $product = $user->getProduct($name)) {
                $product = ProductRegistry::get($name);
                $user->addProduct($product);
            }
        }

        $price = $product->getMarketPrice();
        $itemIndex = $product->buy($this->container);

        $payment = new Payment();
        $payment
            ->setUserId($user->getId())
            ->setType($explode[0])
        ;

        if ($itemIndex) {
            $payment->setIndex($itemIndex);
        }

        $user->setBalance(max(0, $user->getBalance() - $price));
        $this->container->getPaymentRepository()->insert($payment);
        $this->container->getORM()->flush($user);

        return $payment;
    }
}