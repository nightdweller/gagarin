<?php

namespace App\Product;

use App\Container\Container;

class ClickPower extends Product
{
    public function getId()
    {
        return 'click_power';
    }

    public function getDescription()
    {
        return 'В два раза больше денег за клик целую минуту';
    }

    public function getName()
    {
        return 'Повысить давление';
    }

    public function release(Container $container)
    {
        $this->endTime = max($this->endTime, new \DateTime())->add(new \DateInterval('PT1M'));
        return array_merge(parent::release($container), []);
    }

    public function getPrice()
    {
        return 1;
    }
}