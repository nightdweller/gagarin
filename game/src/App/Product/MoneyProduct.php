<?php

namespace App\Product;

use App\Container\Container;
use App\Converter\MoneyConverter;
use App\Entity\User;
use App\Fabric\FabricRegistry;
use Components\UserToken;
use Components\Util\AssetsUtil;

class MoneyProduct extends Product
{
    protected $count;

    public function __construct($count = null)
    {
        $this->count = $count;
    }

    public function setIndex($count)
    {
        $this->count = $count;
        return $this;
    }

    public function getIndex()
    {
        return $this->count;
    }

    public function getId()
    {
        return 'money';
    }

    public function getDescription()
    {
        return '';
    }

    public function isAvailable()
    {
        return false;
    }

    public function buy(Container $container)
    {
        /** @var User $user */
        $user = UserToken::getUserStatic();
        $user->setBalance($user->getBalance() + $this->count);

        $container->getORM()->flush($user);

        return $this->count;
    }

    public function getName()
    {
        return $this->count . ' ' . AssetsUtil::pluralize($this->count, 'септим', 'септима', 'септимов');
    }

    public function release(Container $container)
    {
        return array_merge([]);
    }

    public function getPrice()
    {
        return self::getPriceStatic($this->count);
    }

    public static function getPriceStatic($count)
    {
        if ($count < 90) {
            return round($count / 15);
        }

        if ($count < 210) {
            return round($count / 18);
        }

        if ($count < 450) {
            return round($count / 21);
        }

        if ($count < 1200) {
            return round(($count * 2) / 45);
        }

        return round($count / 24);
    }

    public static function getProfitStatic($count)
    {
        return round($count / 5) - self::getPriceStatic($count);
    }
}