<?php

namespace App\Product;

use App\Container\Container;

class FabricPrice extends Product
{
    public function getId()
    {
        return 'fabric_price';
    }

    public function getDescription()
    {
        return 'Стоимость фабрик уменьшается в два раза на пять минут!';
    }

    public function getName()
    {
        return 'Напугать землян';
    }

    public function getPrice()
    {
        return 2;
    }

    public function release(Container $container)
    {
        $this->endTime = max($this->endTime, new \DateTime())->add(new \DateInterval('PT5M'));
        return array_merge(parent::release($container), []);
    }
}