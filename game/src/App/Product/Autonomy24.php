<?php

namespace App\Product;

use App\Container\Container;
use App\Entity\User;
use Components\UserToken;

class Autonomy24 extends Product
{
    public function getId()
    {
        return 'autonomy24';
    }

    public function getDescription()
    {
        return 'Когда выходишь из игры, деньги всё равно продолжают зарабатываться!';
    }

    public function getName()
    {
        return 'Сутки автономии!';
    }

    public function getPrice()
    {
        return 15;
    }

    public function release(Container $container)
    {
        $user = $container->getUser();
        if (null === $product = $user->getProduct('autonomy12')) {
            $product = ProductRegistry::get('autonomy12');
            $user->addProduct($product);
        }

        $this->decCount();
        $product->incCount()->incCount();
        $product->release($container);
        $result = $product->release($container);

        $container->getORM()->flush($user);
        return [
            'count' => $this->count,
            'product' => array_merge($result, [
                'id' => $product->getId()
            ])
        ];
    }
}