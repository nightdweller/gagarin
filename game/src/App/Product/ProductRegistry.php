<?php

namespace App\Product;

class ProductRegistry
{
    private static $config2 = [
        ['name' => 'Повысить давление', 'description' => 'В два раза больше денег за клик целую минуту', 'price' => 1],
        ['name' => 'Лунная вечеринка', 'description' => 'Скорость добычи в два раза быстрее!', 'price' => 2],
        ['name' => 'Напугать землян', 'description' => 'Стоимость фабрик уменьшается в два раза на пять минут!', 'price' => 2],
        ['name' => '12 часов автономии', 'description' => 'Когда выходишь из игры, деньги всё равно продолжают зарабатываться!', 'price' => 10],
        ['name' => 'Сутки автономии', 'description' => 'Когда выходишь из игры, деньги всё равно продолжают зарабатываться!', 'price' => 15],
    ];

    private static $config = [
        'click_power' => 'App\\Product\\ClickPower',
        'moon_party' => 'App\\Product\\MoonParty',
        'fabric_price' => 'App\\Product\\FabricPrice',
        'autonomy12' => 'App\\Product\\Autonomy12',
        'autonomy24' => 'App\\Product\\Autonomy24',
        'fabric' => 'App\\Product\\FabricProduct',
        'sponsor_big' => 'App\\Product\\SponsorBigProduct',
        'sponsor_medium' => 'App\\Product\\SponsorMediumProduct',
        'sponsor_small' => 'App\\Product\\SponsorSmallProduct',
        'money' => 'App\\Product\\MoneyProduct',
        'energy' => 'App\\Product\\EnergyProduct'
    ];

    public static $items = [];

    /**
     * @return Product
     */
    public static function get($alias, $index = null)
    {
        if (!isset(self::$items[$alias])) {
            $class = self::$config[$alias];

            if ($index) {
                self::$items[$alias] = new $class($index);
            } else {
                self::$items[$alias] = new $class();
            }
        }

        return self::$items[$alias];
    }

    public static function all()
    {
        $all = [];
        foreach (array_keys(self::$config) as $key) {
            $product = self::get($key);

            if ($product->isAvailable()) {
                $all[$key] = self::get($key);
            }
        }

        return $all;
    }

    public static function keys()
    {
        return array_keys(self::$config);
    }

    public static function allArray()
    {
        $array = [];
        foreach (array_keys(self::$config) as $key) {
            $product = self::get($key);

            if ($product->isAvailable()) {
                $array[] = $product;
            }
        }

        return $array;
    }
}