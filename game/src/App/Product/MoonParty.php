<?php

namespace App\Product;

use App\Container\Container;

class MoonParty extends Product
{
    public function getId()
    {
        return 'moon_party';
    }

    public function getDescription()
    {
        return 'Скорость добычи в два раза быстрее!';
    }

    public function getName()
    {
        return 'Лунная вечеринка';
    }

    public function getPrice()
    {
        return 2;
    }

    public function release(Container $container)
    {
        $this->endTime = max($this->endTime, new \DateTime())->add(new \DateInterval('PT1M'));
        return array_merge(parent::release($container), []);
    }
}