<?php

namespace App\Product;

use App\Container\Container;

class Autonomy12 extends Product
{
    public function getId()
    {
        return 'autonomy12';
    }

    public function getDescription()
    {
        return 'Когда выходишь из игры, деньги всё равно продолжают зарабатываться!';
    }

    public function getName()
    {
        return '12 часов автономии';
    }

    public function getPrice()
    {
        return 10;
    }

    public function release(Container $container)
    {
        $this->endTime = max($this->endTime, new \DateTime())->add(new \DateInterval('PT12H'));
        return array_merge(parent::release($container), []);
    }
}