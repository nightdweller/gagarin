<?php

namespace App\Product;

use App\Container\Container;
use App\Entity\User;
use App\Fabric\FabricRegistry;
use Components\UserToken;

class FabricProduct extends Product
{
    protected $index;

    public function __construct($index = null)
    {
        $this->index = $index;
    }

    public function getIndex()
    {
        return $this->index;
    }

    public function getId()
    {
        return 'fabric';
    }

    public function getDescription()
    {
        return '';
    }

    public function isAvailable()
    {
        return false;
    }

    public function buy(Container $container)
    {
        /** @var User $user */
        $user = UserToken::getUserStatic();
        if (null === $fabric = $user->getFabric($this->index)) {
            $fabric = FabricRegistry::get($this->index);
            $user->addFabric($fabric);
        }

        $fabric->incCount();
        $user->setIncValue($user->getIncValue() + $fabric->getProfit());

        return $fabric->getIndex();
    }

    public function getName()
    {
        /** @var User $user */
        $user = UserToken::getUserStatic();
        $fabrics = $user->getFabricsInRange();
        foreach ($fabrics as $fabric) {
            if ($this->index == $fabric->getIndex()) {
                return $fabric->getName();
            }
        }

        return $user->getId();
    }

    public function release(Container $container)
    {
        return array_merge([]);
    }

    public function getPrice()
    {
        /** @var User $user */
        $user = UserToken::getUserStatic();
        $fabrics = $user->getFabricsInRange();
        for ($i = 0; $i < 3; $i++) {
            $fabric = $fabrics[$i];
            if ($this->index == $fabric->getIndex()) {
                return $i + 1;
            }
        }

        return 1;
    }
}