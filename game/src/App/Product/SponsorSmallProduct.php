<?php

namespace App\Product;

use App\Container\Container;
use App\Entity\Sponsor;

class SponsorSmallProduct extends SponsorProduct
{
    protected $type = Sponsor::TYPE_SMALL;

    public function getName()
    {
        return 'Место промоут спонсора';
    }

    public function getPrice()
    {
        return 100;
    }
}