<?php

namespace App\Product;

use App\Container\Container;

class Product
{
    protected $name;
    protected $description;
    protected $price = 0;
    protected $marketPrice = 0;
    protected $count = 0;
    protected $endTime;

    public function __construct()
    {
        $this->endTime = new \DateTime();
    }

    public function isAvailable()
    {
        return true;
    }

    public function releaseOnBuy()
    {
        return false;
    }

    public function getId()
    {
        return '';
    }

    public function getEndTime()
    {
        return $this->endTime ? $this->endTime : new \DateTime();
    }

    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getPrice()
    {
        return (int)$this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    public function getMarketPrice()
    {
        return (int)$this->getPrice() * 3;
    }

    public function setMarketPrice($marketPrice)
    {
        $this->marketPrice = $marketPrice;
        return $this;
    }

    public function setProfit($profit)
    {
        $this->profit = $profit;
        return $this;
    }

    public function getCount()
    {
        return (int)$this->count;
    }

    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    public function incCount()
    {
        $this->count++;

        return $this;
    }

    public function decCount()
    {
        $this->count--;

        return $this;
    }

    public function release(Container $container)
    {
        $this->decCount();
        return [
            'count' => $this->count,
            'remain_time' => $this->getRemainTime(),
        ];
    }

    public function buy(Container $container)
    {
        $this->incCount();

        return null;
    }

    public function getRemainTime()
    {
        $now = new \DateTime();
        return $this->getEndTime()->getTimestamp() - $now->getTimestamp();
    }
}