<?php

namespace App\Product;

use App\Container\Container;
use App\Entity\Sponsor;

class SponsorBigProduct extends SponsorProduct
{
    protected $type = Sponsor::TYPE_BIG;

    public function getName()
    {
        return 'Место VIP спонсора';
    }

    public function getPrice()
    {
        return 500;
    }
}