<?php

namespace App\Product;

use App\Container\Container;
use App\Converter\MoneyConverter;
use App\Entity\User;
use App\Fabric\FabricRegistry;
use Components\UserToken;
use Components\Util\AssetsUtil;

class EnergyProduct extends Product
{
    protected $count;

    public function __construct($count = null)
    {
        $this->count = $count;
    }

    public function setIndex($count)
    {
        $this->count = $count;
        return $this;
    }

    public function getIndex()
    {
        return $this->count;
    }

    public function getId()
    {
        return 'energy';
    }

    public function getDescription()
    {
        return '';
    }

    public function isAvailable()
    {
        return false;
    }

    public function buy(Container $container)
    {
        /** @var User $user */
        $user = UserToken::getUserStatic();
        $user->veryIncEnergy(floor($this->count * $user->getMaxEnergy() / 100));

        $container->getORM()->flush($user);

        return $this->count;
    }

    public function getName()
    {
        return $this->count . ' ' . AssetsUtil::pluralize($this->count, '% энергии', '% энергии', '% энергии');
    }

    public function release(Container $container)
    {
        return array_merge([]);
    }

    public function getPrice()
    {
        return self::getPriceStatic($this->count);
    }

    public static function getPriceStatic($count)
    {
        if ($count == 50) {
            return 2;
        }

        if ($count == 100) {
            return 3;
        }

        return 1000000000;
    }

    public static function getProfitStatic($count)
    {
        return round($count / 5) - self::getPriceStatic($count);
    }
}