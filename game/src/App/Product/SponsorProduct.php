<?php

namespace App\Product;

use App\Container\Container;
use App\Entity\Sponsor;

abstract class SponsorProduct extends Product
{
    protected $type;
    protected $index;

    public function __construct($index = null)
    {
        $this->index = $index;
    }

    public function getIndex()
    {
        return $this->index;
    }

    public function getId()
    {
        return 'sponsor_' . $this->type;
    }

    public function isAvailable()
    {
        return false;
    }

    public function buy(Container $container)
    {
        $sponsor = new Sponsor();
        $sponsor
            ->setNumber($this->index)
            ->setType($this->type)
            ->setVkId($container->getUser()->getVkId())
            ->setVotes($this->getPrice())
        ;
        
        $container->getSponsorRepository()->insert($sponsor);

        return $this->index;
    }

    public function release(Container $container)
    {
        return array_merge([]);
    }
}