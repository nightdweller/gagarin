<?php

namespace App\Product;

use App\Container\Container;
use App\Entity\Sponsor;

class SponsorMediumProduct extends SponsorProduct
{
    protected $type = Sponsor::TYPE_MEDIUM;

    public function getName()
    {
        return 'Место спонсора';
    }

    public function getPrice()
    {
        return 250;
    }
}