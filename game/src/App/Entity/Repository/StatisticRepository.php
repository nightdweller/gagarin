<?php

namespace App\Entity\Repository;

use App\Container\Container;
use Components\DB\Repository;

class StatisticRepository extends Repository
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function all()
    {
        $sql = 'SELECT * FROM mobile_statistic ORDER BY d DESC';
        $query = $this->container->getDB()->getConnection()->query($sql);
        return $query->fetch_all(1);
    }

    public function incShow()
    {
        $date = (new \DateTime())->format('Y-m-d');
        $sql = 'INSERT INTO mobile_statistic (d) VALUES ("' . $date . '")';
        $this->container->getDB()->getConnection()->query($sql);
        $this->container->getDB()->getConnection()->query('UPDATE mobile_statistic SET shows = shows+1 WHERE d = "' . $date . '"');
    }

    public function incInstall()
    {
        $date = (new \DateTime())->format('Y-m-d');
        $sql = 'INSERT INTO mobile_statistic (d) VALUES ("' . $date . '")';
        $this->container->getDB()->getConnection()->query($sql);
        $this->container->getDB()->getConnection()->query('UPDATE mobile_statistic SET installs = installs+1 WHERE d = "' . $date . '"');
    }
}