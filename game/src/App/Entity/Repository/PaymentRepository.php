<?php

namespace App\Entity\Repository;

use Admin\Filter\PaymentFilter;
use App\Entity\Payment;
use Components\DB\QueryBuilder;
use Components\DB\Repository;

/**
 * @method Payment find($id)
 * @method Payment findOneBy($criteria)
 */
class PaymentRepository extends Repository
{
    public function createFilteredQueryBuilder(PaymentFilter $filter)
    {
        $qb = $this->createQueryBuilder();

        if ($filter->getVotes() == 'votes') {
            $qb
                ->andWhere('order_id > 0')
                ->andWhere('item_index > 0')
            ;
        }

        if ($filter->getType()) {
            $qb
                ->andWhere('type = ":type"')
                ->setParameter('type', $filter->getType())
            ;
        }

        return $qb;
    }
}