<?php

namespace App\Entity\Repository;

use App\Container\Container;
use App\Entity\Sponsor;
use App\Entity\User;
use Components\DB\Repository;

class SponsorRepository extends Repository
{
    public function getMapped()
    {
        /** @var Sponsor[] $sponsors */
        $sponsors = $this->createQueryBuilder()->getQuery()->getResult();
        $map = [
            Sponsor::TYPE_SMALL => [],
            Sponsor::TYPE_MEDIUM => [],
            Sponsor::TYPE_BIG => [],
        ];

        foreach ($sponsors as $sponsor) {
            $map[$sponsor->getType()][$sponsor->getNumber()] = $sponsor;
        }

        return $map;
    }
}