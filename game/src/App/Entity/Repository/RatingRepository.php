<?php

namespace App\Entity\Repository;

use App\Container\Container;

class RatingRepository
{
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function getPlace($money)
    {
        $sql = 'SELECT count(*) as cnt FROM users WHERE money >= ' . intval($money);
        $place = $this->container->getDB()->getConnection()
            ->query($sql)
            ->fetch_all(MYSQLI_ASSOC)
        ;

        return $place[0]['cnt'];
    }

    public function getNear($money)
    {
        $sql = sprintf('
            (SELECT vk_id, money FROM users WHERE money = (
                SELECT max(money) FROM users WHERE money < %s
            ) LIMIT 0, 1) UNION
            (SELECT vk_id, money FROM users WHERE money = (
                SELECT min(money) as money FROM users WHERE money > %s
            ) LIMIT 0, 1)
        ', $money, $money);

        $result = $this->container->getDB()->getConnection()
            ->query($sql)
            ->fetch_all(MYSQLI_ASSOC)
        ;

        $response = [];
        foreach ($result as $row) {
            if (null !== $row['vk_id'] && null !== $row['money']) {
                $response[] = $row;
            }
        }

        return $response;
    }
}