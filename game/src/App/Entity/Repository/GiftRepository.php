<?php

namespace App\Entity\Repository;

use App\Container\Container;
use App\Entity\User;
use Components\DB\Repository;

class GiftRepository
{
    private $container;
    const TYPE_PRODUCT = 'products';

    public static $types = [
        self::TYPE_PRODUCT
    ];

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function findGifts($id)
    {
        $result = $this
            ->container
            ->getDB()
            ->getConnection()
            ->query('SELECT * FROM user_gifts WHERE id = ' . intval($id))
            ->fetch_assoc()
        ;

        if (!$result) {
            return [
                'products' => []
            ];
        }

        $data = reset($result);
        $data = explode(',', $result['products']);

        $result = [
            'products' => []
        ];

        for ($i = 1; $i < count($data); $i += 2) {
            $result['products'][] = ['vk_id' => $data[$i], 'index' => $data[$i+1]];
        }

        return $result;
    }

    public function push($uid, $type, $index, User $owner)
    {
        $string = ',' . $owner->getVkId() . ',' . $index;
        $sql = 'UPDATE user_gifts SET %s = CONCAT(%s, "%s") WHERE id = %s';
        $sql = sprintf($sql, $type, $type, $string, $uid);

        $query = $this->container->getDB()->getConnection()->query($sql);
        return $query;
    }

    public function clear($uid)
    {
        $sql = 'UPDATE user_gifts SET ' . implode(', ', array_map(function ($key) {
            return $key . ' = ""';
        }, self::$types)) . ' WHERE id = ' . $uid;

        return $this->container->getDB()->getConnection()->query($sql);
    }

    public function init($uid)
    {
        $sql = 'INSERT INTO user_gifts (id) VALUES (' . $uid . ')';

        $this->container->getDB()->getConnection()->query($sql);
    }
}