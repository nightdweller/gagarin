<?php

namespace App\Entity\Repository;

use Admin\Filter\UserFilter;
use App\Entity\User;
use Components\DB\Repository;

/**
 * @method User find($id)
 */
class UserRepository extends Repository
{
    public function findVk($id)
    {
        return $this->findOneBy(['vkId' => $id]);
    }

    public function createFilteredQueryBuilder(UserFilter $filter)
    {
        $qb = $this->createQueryBuilder();

        if ($filter->getVkId()) {
            $qb
                ->andWhere('vk_id = :vkId')
                ->setParameter('vkId', $filter->getVkId())
            ;
        }

        return $qb;
    }
}