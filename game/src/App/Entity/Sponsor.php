<?php

namespace App\Entity;

use App\Fabric\Fabric;
use App\Fabric\FabricRegistry;
use App\Product\Product;
use Components\Struct\BitSet;

class Sponsor
{
    const TYPE_BIG = 2;
    const TYPE_MEDIUM = 1;
    const TYPE_SMALL = 0;

    public static $types = [
        self::TYPE_BIG => 'big',
        self::TYPE_MEDIUM => 'medium',
        self::TYPE_SMALL => 'small',
    ];

    protected $id;
    protected $vkId;
    protected $number;
    protected $type;
    protected $votes;

    public function getTypeName()
    {
        return self::$types[$this->type];
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getVkId()
    {
        return $this->vkId;
    }

    public function setVkId($vkId)
    {
        $this->vkId = $vkId;
        return $this;
    }

    public function getVotes()
    {
        return $this->votes;
    }

    public function setVotes($votes)
    {
        $this->votes = $votes;
        return $this;
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function setNumber($number)
    {
        $this->number = $number;
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }
}