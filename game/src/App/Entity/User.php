<?php

namespace App\Entity;

use App\Fabric\Fabric;
use App\Fabric\FabricRegistry;
use App\Product\Product;
use Components\Struct\BitSet;

class User
{
    const ENERGY_UPDATE_TIME = 24000;

    protected $maxEnergy = 2000;

    protected $id;
    protected $vkId;
    protected $city;
    protected $level = 0;

    protected $money = 0;
    protected $incValue = 1;

    protected $lastUpdate;
    protected $lastGift;
    protected $giftsCount;
    protected $sum = 0;
    protected $balance = 10;
    protected $info = '';
    protected $clicks = 0;
    protected $lastNotify;
    protected $energy = 2000;
    protected $lastEnergyUpdate;
    protected $lastLevelUpdate = 0;
    protected $lastNotification = 0;

    protected $fabrics = [];
    protected $products = [];

    /**
     * @var BitSet $missions
     */
    protected $missions;

    public function __construct()
    {
        $this->lastUpdate = new \DateTime();
        $this->lastGift = new \DateTime();
        $this->lastNotify = new \DateTime();
        $this->lastEnergyUpdate = time();
    }

    public function getLastNotification()
    {
        return $this->lastNotification;
    }

    public function setLastNotification($lastNotification)
    {
        $this->lastNotification = $lastNotification;
    }

    public function getLastLevelUpdate()
    {
        return $this->lastLevelUpdate;
    }

    public function setLastLevelUpdate($lastLevelUpdate)
    {
        $this->lastLevelUpdate = $lastLevelUpdate;
    }

    public function getLastEnergyUpdate()
    {
        return $this->lastEnergyUpdate ?: $this->lastEnergyUpdate = time();
    }

    public function update()
    {
        $last = $this->getLastEnergyUpdate();
        $diff = time() - $last;
        $inc = floor(($this->maxEnergy * $diff) / self::ENERGY_UPDATE_TIME);
        $save = $diff - ($inc * self::ENERGY_UPDATE_TIME) / $this->maxEnergy;

        if ($this->getEnergy() < $this->maxEnergy) {
            $this->incEnergy($inc);
            if ($this->getEnergy() > $this->maxEnergy) {
                $this->energy = $this->maxEnergy;
            }

            $this->lastEnergyUpdate = time() - $save;
        } 
    }

    public function getMaxEnergy()
    {
        return $this->maxEnergy;
    }

    public function getEnergyPercent()
    {
        return floor(($this->energy / $this->maxEnergy) * 100);
    }

    public function getEnergy()
    {
        return $this->energy;
    }

    public function setEnergy($energy)
    {
        if ($this->energy > $this->maxEnergy) {
            return $this;
        }

        $this->energy = $energy;
        return $this;
    }

    public function incEnergy($value)
    {
        $this->setEnergy($this->getEnergy() + $value);
    }

    public function veryIncEnergy($value)
    {
        $this->energy += $value;
    }

    public function decEnergy($value)
    {
        $this->energy = max($this->getEnergy() - $value, 0);
    }

    public function getClicks()
    {
        return $this->clicks;
    }

    public function setClicks($clicks)
    {
        $this->clicks = $clicks;
        return $this;
    }

    public function getLastNotify()
    {
        return $this->lastNotify;
    }

    public function setLastNotify($lastNotify)
    {
        $this->lastNotify = $lastNotify;
        return $this;
    }

    public function getInfo()
    {
        return $this->info;
    }

    public function setInfo($info)
    {
        $this->info = $info;
        return $this;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function setLevel($level)
    {
        $this->level = $level;
        return $this;
    }

    public function getBalance()
    {
        return $this->balance;
    }

    public function setBalance($balance)
    {
        $this->balance = $balance;
        return $this;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    public function getSum()
    {
        return $this->sum;
    }

    public function setSum($sum)
    {
        $this->sum = $sum;
        return $this;
    }

    public function incSum($value = 1)
    {
        $this->sum += $value;
        return $this;
    }

    public function decSum($value = 1)
    {
        $this->sum -= $value;
        return $this;
    }

    public function getGiftsCount()
    {
        return $this->giftsCount;
    }

    public function setGiftsCount($giftsCount)
    {
        $this->giftsCount = $giftsCount;
        return $this;
    }

    public function getLastGift()
    {
        return $this->lastGift;
    }

    public function getLastGiftDiff()
    {
        if (is_string($this->lastGift)) {
            $this->lastGift = new \DateTime($this->lastGift);
        }

        return $this->lastGift->getTimestamp() - time();
    }

    public function setLastGift($lastGift)
    {
        $this->lastGift = $lastGift;
        return $this;
    }

    public function getMissions()
    {
        if ($this->missions) {
            return $this->missions;
        }

        return $this->missions = new BitSet();
    }

    public function setMissions($missions)
    {
        $this->missions = $missions;
        return $this;
    }

    public function getIncValue()
    {
        return $this->incValue;
    }

    public function setIncValue($incValue)
    {
        $this->incValue = $incValue;

        return $this;
    }

    /** @return Fabric[] */
    public function getFabrics()
    {
        return $this->fabrics;
    }

    /**
     * @return Fabric|null
     */
    public function getFabric($index)
    {
        foreach ($this->fabrics as $fabric) {
            if ($fabric->getIndex() == $index) {
                return $fabric;
            }
        }

        return null;
    }

    public function addFabric(Fabric $fabric)
    {
        $this->fabrics[] = $fabric;

        return $this;
    }

    /**
     * @return Fabric[]
     */
    public function getFabricsInRange()
    {
        /** @var Fabric[] $fabrics */
        $fabrics = array_slice($this->fabrics, max(count($this->fabrics) - 2, 0), 3);
        $fabric = $fabrics ? end($fabrics) : null;

        if ($fabrics[0]->getIndex() >= FabricRegistry::count() - 2) {
            $fabrics = array_merge([FabricRegistry::getPrev($fabrics[0])], $fabrics);
        }

        while (count($fabrics) < 3) {
            if ($fabric = FabricRegistry::getNext($fabric)) {
                $fabrics[] = $fabric;
            }
        }

        return $fabrics;
    }

    public function setFabrics($fabrics)
    {
        $this->fabrics = $fabrics;

        usort($this->fabrics, function (Fabric $left, Fabric $right) {
            return $left->getIndex() - $right->getIndex();
        });

        return $this;
    }

    /** @return Product[] */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @return Product|null
     */
    public function getProduct($id)
    {
        foreach ($this->products as $product) {
            if ($product->getId() == $id) {
                return $product;
            }
        }

        return null;
    }

    public function addProduct(Product $product)
    {
        $this->products[] = $product;

        return $this;
    }

    public function setProducts($products)
    {
        $this->products = $products;

        usort($this->products, function (Product $left, Product $right) {
            return $left->getIndex() - $right->getIndex();
        });

        return $this;
    }

    public function getLastUpdate()
    {
        if (is_string($this->lastUpdate)) {
            $this->lastUpdate = new \DateTime($this->lastUpdate);
        }

        return $this->lastUpdate;
    }

    public function setLastUpdate($lastUpdate)
    {
        if (is_string($lastUpdate)) {
            $lastUpdate = new \DateTime($lastUpdate);
        }

        $this->lastUpdate = $lastUpdate;
        return $this;
    }

    public function getRealMoney()
    {
        return $this->money;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getVkId()
    {
        return $this->vkId;
    }

    public function setVkId($vkId)
    {
        $this->vkId = $vkId;
        return $this;
    }

    public function getMoney()
    {
        return (int)$this->money;
    }

    public function incMoney($value)
    {
        $this->money += $value;

        return $this;
    }

    public function decMoney($money)
    {
        $this->money -= $money;

        return $this;
    }

    public function setMoney($money)
    {
        $this->money = (int)$money;
        return $this;
    }
}