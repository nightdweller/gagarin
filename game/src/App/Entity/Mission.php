<?php

namespace App\Entity;

class Mission
{
    const TYPE_MONEY = 1;
    const TYPE_FABRIC = 2;
    const TYPE_OTHER = 0;

    public static $types = [
        self::TYPE_MONEY => 'Деньги',
        self::TYPE_FABRIC => 'Фабрика',
        self::TYPE_OTHER => 'Другое'
    ];

    protected $id;
    protected $name;
    protected $description;
    protected $image;
    protected $type;
    protected $message;
    protected $extra;
    protected $money = 0;
    protected $number = 3;

    public function getNumber()
    {
        return $this->number;
    }

    public function setNumber($number)
    {
        $this->number = $number;
    }

    public function setMoney($money)
    {
        $this->money = $money;
    }

    public function getMoney()
    {
        return $this->money;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    public function getExtra()
    {
        return $this->extra;
    }

    public function setExtra($extra)
    {
        $this->extra = $extra;
        return $this;
    }
}