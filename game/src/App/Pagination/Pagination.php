<?php

namespace App\Pagination;

use Components\DB\QueryBuilder;
use Components\Request;

class Pagination implements \ArrayAccess, \Iterator, \Countable
{
    protected $qb;
    protected $countQb;

    protected $page;
    protected $limit;
    protected $request;

    protected $items;
    protected $count;

    public function __construct(QueryBuilder $qb, $page = 1, $limit = 15, Request $request = null)
    {
        if ($request) {
            $page = $request->get('page');
        }

        $this->qb = clone $qb;
        $this->countQb = clone $qb;
        $this->page = $page ?: 1;
        $this->limit = $limit;
        $this->request = $request;
    }

    public function update()
    {
        $this->items = $this->qb
            ->skip(($this->page - 1) * $this->limit)
            ->limit($this->limit)
            ->getQuery()
            ->getResult()
        ;

        return $this;
    }

    public function getItems()
    {
        if (null === $this->items) {
            $this->update();
        }

        return $this->items;
    }

    public function getCount()
    {
        if ($this->count) {
            return $this->count;
        }

        $result = $this
            ->countQb
            ->skip(null)
            ->limit(null)
            ->select('count(id)')
            ->getQuery()
            ->getArrayResult()
        ;

        $row = reset($result);

        return $this->count = intval(reset($row));
    }

    public function getCurrentPage()
    {
        return $this->page;
    }

    public function getNextPage()
    {
        if ($this->isLastPage()) {
            return $this->page;
        }

        return $this->page + 1;
    }

    public function getPrevPage()
    {
        return $this->page == 1 ? 1 : $this->page - 1;
    }

    public function isFirstPage()
    {
        return $this->page == 1;
    }

    public function isLastPage()
    {
        return $this->page * $this->limit >= $this->getCount();
    }

    public function getLastPage()
    {
        return intval($this->getCount() / $this->limit) + intval($this->getCount() % $this->limit != 0);
    }

    public function rewind()
    {
        if (null === $this->items) {
            $this->update();
        }

        return reset($this->items);
    }

    public function current()
    {
        return current($this->items);
    }

    public function key()
    {
        return key($this->items);
    }

    public function next()
    {
        next($this->items);
    }

    public function valid()
    {
        return key($this->items) !== null;
    }

    public function count()
    {
        if (null === $this->items) {
            $this->update();
        }

        return count($this->items);
    }

    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->items);
    }

    public function offsetGet($offset)
    {
        return $this->items[$offset];
    }

    public function offsetSet($offset, $value)
    {
        if (null === $offset) {
            $this->items[] = $value;
        } else {
            $this->items[$offset] = $value;
        }
    }

    public function offsetUnset($offset)
    {
        unset($this->items[$offset]);
    }
}