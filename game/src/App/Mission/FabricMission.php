<?php

namespace App\Mission;

use App\Container\Container;

class FabricMission extends Mission
{
    protected $fabricId;

    public function __construct($id, array $params)
    {
        parent::__construct($id, $params);
        $this->fabricId = $params['fabricId'];
    }

    public function getExtra()
    {
        return [
            'fabricId' => $this->fabricId
        ];
    }

    public function getFabricId()
    {
        return $this->fabricId;
    }

    public function setFabricId($fabricId)
    {
        $this->fabricId = $fabricId;
        return $this;
    }

    public function check(Container $container)
    {
        if (null === $fabric = $container->getUser()->getFabric($this->fabricId)) {
            return false;
        }

        return $fabric->getCount() > 0;
    }
}