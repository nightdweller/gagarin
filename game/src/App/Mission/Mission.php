<?php

namespace App\Mission;

use App\Container\Container;

class Mission
{
    protected $id;
    protected $name;
    protected $description;
    protected $image;

    protected $status;

    public function __construct($id, $params = [])
    {
        $this->id = $id;
        $this->name = $params['name'];
        $this->description = $params['description'];
        $this->image = $params['image'];
    }

    public function getExtra()
    {
        return [];
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getImage()
    {
        return $this->image;
    }

    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    public function check(Container $container)
    {
        return false;
    }
}