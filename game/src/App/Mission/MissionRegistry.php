<?php

namespace App\Mission;

class MissionRegistry
{
    public static $config = [
        1 => ['App\\Mission\\FabricMission', [
            'fabricId' => 0,
            'name' => 'Купить первую ракету',
            'description' => 'Купить первую ракету',
            'event' => 'купил ракету',
            'image' => 'https://cs7063.vk.me/c540108/v540108001/6a05/oaRFEFH7OGI.jpg'
        ]],
        2 => ['App\\Mission\\MoneyMission', [
            'money' => 1000,
            'name' => 'Накопить 1000$',
            'description' => 'Ты справился с задачей! Теперь будешь получать по 10 центов в секунду с каждой ракеты! Хорошо бы поднакопить начальный капитал.',
            'event' => 'заработал первую 1000 долларов',
            'image' => 'https://cs7063.vk.me/c540108/v540108001/6a05/oaRFEFH7OGI.jpg'
        ]],
        3 => ['App\\Mission\\FabricMission', [
            'fabricId' => 1,
            'name' => 'Купить шатл',
            'description' => 'Подсобирай денег на шатл. Ты сможешь',
            'event' => 'купил свой первый шатл',
            'image' => 'https://cs7063.vk.me/c540108/v540108001/6a05/oaRFEFH7OGI.jpg'
        ]],
        4 => ['App\\Mission\\FabricMission', [
            'fabricId' => 2,
            'name' => 'Купить oqkfep',
            'description' => 'Подсобирай денег на шатл. Ты сможешь',
            'event' => 'купил свой первый шатл',
            'image' => 'https://cs7063.vk.me/c540108/v540108001/6a05/oaRFEFH7OGI.jpg'
        ]],
        5 => ['App\\Mission\\FabricMission', [
            'fabricId' => 3,
            'name' => 'Купить oqkfep',
            'description' => 'Подсобирай денег на шатл. Ты сможешь',
            'event' => 'купил свой первый шатл',
            'image' => 'https://cs7063.vk.me/c540108/v540108001/6a05/oaRFEFH7OGI.jpg'
        ]],
        6 => ['App\\Mission\\MoneyMission', [
            'money' => 5000,
            'name' => 'Накопить 1000$',
            'description' => 'Ты справился с задачей! Теперь будешь получать по 10 центов в секунду с каждой ракеты! Хорошо бы поднакопить начальный капитал.',
            'event' => 'заработал первую 1000 долларов',
            'image' => 'https://cs7063.vk.me/c540108/v540108001/6a05/oaRFEFH7OGI.jpg'
        ]],
    ];

    protected static $items = [];

    /**
     * @return Mission
     */
    public static function get($id)
    {
        if (isset(self::$items[$id])) {
            return self::$items[$id];
        }

        $className = self::$config[$id][0];
        if (isset(self::$config[$id][1])) {
            return self::$items[$id] = new $className($id, self::$config[$id][1]);
        }

        return self::$items[$id] = new $className($id);
    }

    /**
     * @return Mission[]
     */
    public static function all()
    {
        $result = [];
        foreach (array_keys(self::$config) as $key) {
            $result[] = self::get($key);
        }

        return $result;
    }
}