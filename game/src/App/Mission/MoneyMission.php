<?php

namespace App\Mission;

use App\Container\Container;

class MoneyMission extends Mission
{
    protected $money;

    public function __construct($id, array $params)
    {
        parent::__construct($id, $params);
        $this->money = $params['money'];
    }

    public function getExtra()
    {
        return [
            'money' => $this->money
        ];
    }

    public function getMoney()
    {
        return $this->money;
    }

    public function setMoney($money)
    {
        $this->money = $money;
        return $this;
    }

    public function check(Container $container)
    {
        return $container->getUser()->getMoney() > $this->money;
    }
}