<?php

namespace App\Transformer;

use App\Fabric\Fabric;
use App\Fabric\FabricRegistry;
use App\Product\Product;
use App\Product\ProductRegistry;
use Components\Struct\BitSet;

class LevelTransformer
{
    public static function criteriaTransform(array $criteria = null)
    {
        $result = '';
        foreach ($criteria as $key => $value) {
            if ($value) {
                if ($result) {
                    $result .= ',';
                }

                $result .= $key . ':' . $value;
            }
        }

        return $result;
    }

    public static function criteriaReverseTransform($criteria)
    {
        $result = [];
        $array = explode(',', $criteria);
        foreach ($array as $row) {
            if ($row) {
                list($key, $value) = explode(':', $row);
                $result[$key] = $value;
            }
        }

        return $result;
    }
}