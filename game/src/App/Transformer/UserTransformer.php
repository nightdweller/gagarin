<?php

namespace App\Transformer;

use App\Fabric\Fabric;
use App\Fabric\FabricRegistry;
use App\Product\Product;
use App\Product\ProductRegistry;
use Components\Struct\BitSet;

class UserTransformer
{
    public static function missionsTransform($missions = null)
    {
        return (string)$missions;
    }

    public static function missionsReverseTransform($missions)
    {
        return new BitSet($missions);
    }

    public static function productsTransform(array $products)
    {
        return implode(',', array_map(function (Product $product) {
            return $product->getId() . ':' . $product->getCount() . ':' . $product->getEndTime()->getTimestamp();
        }, $products));
    }

    public static function productsReverseTransform($data)
    {
        if (!$data) {
            return [];
        }

        $result = array_map(function ($data) {
            return explode(':', $data);
        }, explode(',', $data));

        foreach ($result as &$value) {
            $value = ProductRegistry::get($value[0])
                ->setCount($value[1])
                ->setEndTime(
                    (new \DateTime())->setTimestamp($value[2])
                )
            ;
        }

        return $result;
    }

    public static function fabricsTransform(array $fabrics)
    {
        return self::itemsTransform($fabrics);
    }

    public static function fabricsReverseTransform($data)
    {
        return self::itemsReverseTransform('App\\Fabric\\FabricRegistry', $data);
    }

    private static function itemsTransform(array $items)
    {
        return implode(',', array_map(function ($item) {
            /** @var Fabric $item */
            return $item->getIndex() . ':' . $item->getCount();
        }, $items));
    }

    private static function itemsReverseTransform($registryClass, $data)
    {
        if (!$data) {
            return [];
        }

        $result = array_map(function ($data) {
            return explode(':', $data);
        }, explode(',', $data));

        foreach ($result as &$value) {
            $value = $registryClass::get($value[0])->setCount($value[1]);
        }

        return $result;
    }
}