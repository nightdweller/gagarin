<?php

namespace App\Command;

use App\Container\Container;
use Components\Console\ArgvInput;

/**
 * @property Container $container
 */
class NotifyCommand extends BaseCommand
{
    protected $duration = 3600;

    public function execute(ArgvInput $input)
    {
        for ($i = 0; $i < 15; $i++) {
            $this->exec();
            var_dump('end');
            sleep(3);
        }
    }

    protected function exec()
    {
        for ($t = 0; $t < 5; $t++) {
            $now = time();

            $api = $this->container->getVk();

            $users = $this
                ->container
                ->getUserRepository()
                ->createQueryBuilder()
                ->select('id, vk_id, last_notification')
                ->andWhere('last_notification < :time')
                ->andWhere('vk_id > 0')
                ->setParameter('time', $now - 60 * 90)
                ->addOrderBy('last_notification', 'asc')
                ->limit(100)
                ->getQuery()
                ->getArrayResult()
            ;

            $ids = array_filter(array_map(function ($row) {
                return $row[1];
            }, $users), function ($row) {
                return $row != 0;
            });

            $result = $this->container->getVkSimple()->post('users.get', [
                'user_ids' => implode(',', $ids),
                'fields' => 'online'
            ])->getResponse();

            $map = [];
            foreach ($result as $user) {
                if ($user['online']) {
                    $map[$user['uid']] = true;
                }
            }

            $idsOnline = array_keys($map);
            $ids = array_diff($ids, $idsOnline);

            $sql = 'update users set last_notification = ' . ($now - 60 * 70) . ' where vk_id in (' . implode(',', $ids) . ')';
            $query = $this->container->getDB()->getConnection()->query($sql);

            if (!$idsOnline) {
                continue;
            }

            $sql = 'update users set last_notification = ' . $now . ' where vk_id in (' . implode(',', $idsOnline) . ')';
            $query = $this->container->getDB()->getConnection()->query($sql);

            $result = $api->post('secure.sendNotification', [
                'user_ids' => implode(',', $idsOnline),
                'message' => 'Почему не заходишь?'
            ])->getArrayResult();

            var_dump($result);
        }
    }

    public function getName()
    {
        return 'notify:send';
    }
}
