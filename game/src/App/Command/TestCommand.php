<?php

namespace App\Command;

use App\Container\Container;
use App\Entity\User;
use App\Fabric\Fabric;
use App\Fabric\FabricRegistry;
use App\Fabric\PriceGenerator;
use App\Product\ProductRegistry;
use Components\Console\ArgvInput;

/**
 * @property Container $container
 */
class TestCommand extends BaseCommand
{
    public function execute(ArgvInput $input)
    {
        $api = $this->container->getVk();
        $api->post('secure.addAppEvent', [
            'activity_id' => 3,
            'user_id' => 11771509
        ]);
    }

    public function getName()
    {
        return 'test';
    }
}
