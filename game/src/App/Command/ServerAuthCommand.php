<?php

namespace App\Command;

use Components\Console\ArgvInput;

class ServerAuthCommand extends BaseCommand
{
    public function execute(ArgvInput $input)
    {
        $query = 'https://oauth.vk.com/access_token?client_id=%s&client_secret=%s&v=5.37&grant_type=client_credentials';
        $content = file_get_contents(sprintf($query, $this->container->clientId, $this->container->clientSecret));
        echo $content;
    }

    public function getName()
    {
        return 'server:auth';
    }
}
