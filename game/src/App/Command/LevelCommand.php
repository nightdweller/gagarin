<?php

namespace App\Command;

use App\Container\Container;
use App\Entity\User;
use App\Fabric\Fabric;
use App\Fabric\FabricRegistry;
use App\Fabric\PriceGenerator;
use App\Product\ProductRegistry;
use Components\Console\ArgvInput;

/**
 * @property Container $container
 */
class LevelCommand extends BaseCommand
{
    protected $duration = 3600;

    public function execute(ArgvInput $input)
    {
        $now = time();

        $api = $this->container->getVk();
        $users = $this
            ->container
            ->getUserRepository()
            ->createQueryBuilder()
            ->select('id, vk_id, last_level_update, level')
            ->andWhere('last_level_update < :time')
            ->setParameter('time', $now - 60*90)
            ->addOrderBy('last_level_update', 'asc')
            ->limit(200)
            ->getQuery()
            ->getArrayResult()
        ;

        $users = array_map(function ($user) {
            return [
                'id' => $user[0],
                'vk_id' => $user[1],
                'last_level_update' => $user[2],
                'level' => $user[3] + 1
            ];
        }, $users);

        $ids = array_map(function ($user) {
            return $user['id'];
        }, $users);

        if (!$ids) {
            return;
        }


        $sql = 'update users set level = level + 1, last_level_update = ' . $now . ' where id in (' . implode(',', $ids) . ')';
        $query = $this->container->getDB()->getConnection()->query($sql);

        $query = implode(',', array_map(function ($user) {
            return $user['vk_id'] . ':' . $user['level'];
        }, $users));

        $result = $api->post('secure.setUserLevel', [
            'levels' => $query
        ])->getArrayResult();

        var_dump(implode(',', $ids));
    }

    public function getName()
    {
        return 'level:send';
    }
}
