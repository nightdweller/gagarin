<?php

namespace Components\Container;

interface ContainerInterface
{
    public function getConfigDir();

    public function getSrcDir();

    public function getViewsDir();

    public function getAppDir();

    public function getEnv();

    public function add($id, array $config);

    public function init($id, $class, array $config = []);

    public function get($id);

    public function set($id, $object);
}
