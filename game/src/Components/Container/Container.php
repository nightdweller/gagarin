<?php

namespace Components\Container;

class Container implements ContainerInterface
{
    const KERNEL_ID = 'kernel';
    const CONTAINER_ID = 'container';

    protected $env = 'App';

    protected $configDir;
    protected $srcDir;
    protected $viewsDir;
    protected $appDir;
    protected $config;
    protected $services = [];

    public function __construct($kernel, $config = null)
    {
        $this->configDir = WEB_DIR . DIRECTORY_SEPARATOR . '../app/config/';
        $this->srcDir = WEB_DIR . DIRECTORY_SEPARATOR . '../src';
        $this->appDir = $this->srcDir . DIRECTORY_SEPARATOR . $this->env;
        $this->viewsDir = $this->appDir . '/Resources/views';

        $config = require_once($this->configDir . 'config.php');
        $components = require_once($this->configDir . 'components.php');

        $this->config = array_merge($config, $components);

        $this
            ->add(self::KERNEL_ID, ['class' => get_class($kernel)])
            ->set(self::KERNEL_ID, $kernel)
            ->add(self::CONTAINER_ID, ['class' => get_class($this)])
            ->set(self::CONTAINER_ID, $this)
        ;
    }

    public function getEnv()
    {
        return $this->env;
    }

    public function getViewsDir()
    {
        return $this->viewsDir;
    }

    public function getSrcDir()
    {
        return $this->srcDir;
    }

    public function getAppDir()
    {
        return $this->appDir;
    }

    public function getConfigDir()
    {
        return $this->configDir;
    }

    public function add($id, array $config)
    {
        $this->config[$id] = $config;

        return $this;
    }

    public function init($id, $class, array $config = [])
    {
        $reflect = new \ReflectionClass($class);
        $object = $this->services[$id] =
            $reflect->newInstanceArgs(isset($config['arguments']) ?
                $this->prepareArguments($config['arguments']) :
                []
            )
        ;

        return $object;
    }

    public function get($id)
    {
        if (array_key_exists($id, $this->services)) {
            return $this->services[$id];
        }

        if (!array_key_exists($id, $this->config)) {
            throw new \Exception();
        }

        $config = $this->config[$id];

        return $this->init($id, $config['class'], $config);
    }

    public function set($id, $object)
    {
        $this->services[$id] = $object;

        return $this;
    }

    private function prepareArguments(array $args)
    {
        return array_map(function ($arg) {
            return $this->prepareArgument($arg);
        }, $args);
    }

    private function prepareArgument($arg)
    {
        if (!is_string($arg)) {
            return $arg;
        }

        if ($arg && $arg[0] == '@') {
            return $this->get(mb_strcut($arg, 1));
        }

        return $arg;
    }
}
