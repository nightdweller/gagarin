<?php

namespace Components\Util;

class AssetsUtil
{
    public static function pluralize($number, $one, $two, $five)
    {
        if (5 <= $number && $number <= 20) {
            return $five;
        }

        $digit = $number % 10;

        if ($digit == 0) {
            return $five;
        }

        if ($digit == 1) {
            return $one;
        }

        if ($digit <= 4) {
            return $two;
        }

        return $five;
    }
}