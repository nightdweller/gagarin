<?php

namespace Components;

class UserToken
{
    private static $user;

    public static function getUserStatic()
    {
        if (self::$user) {
            return self::$user;
        }

        return null;
    }

    public function setUser($user)
    {
        self::$user = $user;
    }

    public function getUser()
    {
        return self::getUserStatic();
    }
}