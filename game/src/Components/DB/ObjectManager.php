<?php

namespace Components\DB;

use Components\Container\ContainerInterface;

class ObjectManager
{
    /** @var DB */
    private $db;
    private $container;

    private $repository = [];
    private $mapping;

    public $values = [];

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->db = $container->get('db');
        $this->mapping = require_once($this->container->getConfigDir() . '/mapping.php');
    }

    public function createInstance(\ReflectionClass $class, $values)
    {
        $className = $class->getName();
        $object = new $className();
        $transformers = isset($this->mapping[$class->getName()]['transformers']) ? $this->mapping[$class->getName()]['transformers'] : [];

        foreach ($values as $key => $value) {
            $prop = $class->getProperty($key);
            $prop->setAccessible(true);

            if ($transformers && isset($transformers[$key])) {
                $transformerClass = $transformers[$key];
                $value = $transformerClass::{$key . 'ReverseTransform'}($value);
            }

            $prop->setValue($object, $value);
        }

        $this->values[$className][$object->getId()] = $values;

        return $object;
    }

    public function initInstance($object)
    {
        $class = new \ReflectionClass(get_class($object));
        $values = [];
        $transformers = isset($this->mapping[$class->getName()]['transformers']) ? $this->mapping[$class->getName()]['transformers'] : [];
        foreach ($class->getProperties() as $prop) {
            $prop->setAccessible(true);
            $value = $prop->getValue($object);

            if ($transformers && isset($transformers[$prop->getName()])) {
                $transformer = $transformers[$prop->getName()];
                $value = $transformer::{$prop->getName() . 'Transform'}($value);
            }

            $values[$prop->getName()] = $value;
        }

        $this->values[$class->getName()][$object->getId()] = $values;
    }

    public function flush($object)
    {
        $diff = [];
        $class = new \ReflectionClass($object);
        $mapping = $this->mapping[$class->getName()]['mapping'];
        $transformers = isset($this->mapping[$class->getName()]['transformers']) ? $this->mapping[$class->getName()]['transformers'] : [];
        $old = $this->values[$class->getName()][$object->getId()];

        foreach ($class->getProperties() as $prop) {
            $prop->setAccessible(true);
            if (!isset($mapping[$prop->getName()])) {
                continue;
            }

            $value = $prop->getValue($object);

            if ($transformers && isset($transformers[$prop->getName()])) {
                $transformer = $transformers[$prop->getName()];
                $value = $transformer::{$prop->getName() . 'Transform'}($value);
            }

            if ($old[$prop->getName()] != $value) {
                $diff[$mapping[$prop->getName()]] = $value;
            }
        }

        $table = $this->mapping[$class->getName()]['table'];
        return $this->db->updateOne($table, [$object->getId()], $diff);
    }

    /**
     * @param $class
     * @return Repository
     */
    public function getRepository($class)
    {
        if (array_key_exists($class, $this->repository)) {
            return $this->repository[$class];
        }

        $mapping = $this->mapping[$class];
        $repositoryClass = isset($mapping['repository']) ? $mapping['repository'] : '\Components\DB\Repository';

        if (!isset($this->values[$class])) {
            $this->values[$class] = [];
        }

        return $this->repository[$class] = new $repositoryClass($this, $class, $this->mapping[$class]);
    }

    public function getDB()
    {
        return $this->db;
    }
}