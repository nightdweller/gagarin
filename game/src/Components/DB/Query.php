<?php

namespace Components\DB;

class Query
{
    protected $repository;
    protected $sql;
    protected $parameters;

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    public function getResult()
    {
        $sql = $this->getPreparedSql();

        return $this->repository->fetchSQL($sql);
    }

    public function getArrayResult()
    {
        $sql = $this->getPreparedSql();

        return $this->repository->getConnection()->query($sql)->fetch_all();
    }

    public function getPreparedSql()
    {
        $result = '';
        for ($i = 0; $i < strlen($this->sql); $i++) {
            if ($this->sql[$i] == ':') {
                for ($key = '', $i++; $i < strlen($this->sql) && ctype_alpha($this->sql[$i]); $i++) {
                    $key .= $this->sql[$i];
                }

                $result .= $this->parameters[$key];
            }

            $result .= $this->sql[$i];
        }

        return $result;
    }

    public function getSql()
    {
        return $this->sql;
    }

    public function setSql($sql)
    {
        $this->sql = $sql;
        return $this;
    }

    public function getParameters()
    {
        return $this->parameters;
    }

    public function setParameters($parameters)
    {
        $this->parameters = $parameters;
        return $this;
    }
}