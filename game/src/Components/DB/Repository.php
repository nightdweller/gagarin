<?php

namespace Components\DB;

class Repository
{
    private $class;
    private $reflection;

    private $table;
    private $dbMapping;
    private $objectMapping;
    private $transformers;

    private $values = [];

    private $orm;
    private $connection;

    public function __construct(ObjectManager $orm, $class, array $mapping)
    {
        $this->orm = $orm;
        $this->connection = $orm->getDB()->getConnection();

        $this->class = $class;
        $this->reflection = new \ReflectionClass($class);

        $this->table = $mapping['table'];
        $this->objectMapping = $mapping['mapping'];
        $this->dbMapping = array_flip($mapping['mapping']);
        $this->transformers = $mapping['transformers'] ?: [];
        $this->fields = array_keys($this->dbMapping);
    }

    public function createQueryBuilder()
    {
        $qb = new QueryBuilder($this);
        $qb->setTable($this->table);

        return $qb;
    }

    public function find($id)
    {
        $result = $this->findBy(['id' => $id]);
        return reset($result);
    }

    public function findOneBy(array $criteria)
    {
        $result = $this->findBy($criteria);
        return reset($result) ?: null;
    }

    public function findBy(array $criteria)
    {
        $equations = [];
        foreach ($criteria as $key => $value) {
            if (is_array($value)) {
                $equations[] = $this->objectMapping[$key] . ' IN (' . implode(',', $value) . ')';
            } else {
                $equations[] = $this->objectMapping[$key] . ($value === null ? ' IS NULL ' : ' = "' . (string)$value . '"');
            }
        }

        $sql = 'SELECT * FROM ' . $this->table;
        if ($equations) {
            $sql .= ' WHERE ' . implode(' AND ', $equations);
        }

        return $this->fetchSQL($sql);
    }

    public function fetchSQL($sql)
    {
        $result = [];

        $query = $this->connection->query($sql);
        while ($query && $row = $query->fetch_assoc()) {
            $result[] = $this->createAndFetch($row);
        }

        return $result;
    }

    public function insert($object)
    {
        if (null !== $object->getId()) {
            return $this;
        }

        $rows = [];
        foreach ($this->objectMapping as $key => $dbKey) {
            if ($key != 'id') {
                $value = $object->{'get' . ucfirst($key)}();

                if ($value instanceof \DateTime) {
                    $value = $value->format('Y-m-d H:i:s');
                }

                if ($this->transformers && isset($this->transformers[$key])) {
                    $class = $this->transformers[$key];
                    $value = $class::{$key . 'Transform'}($value);
                }

                $rows[$dbKey] = '"' . $this->connection->real_escape_string($value) . '"';
            }
        }

        $sql = 'INSERT INTO ' . $this->table . ' (' . implode(', ', array_keys($rows)) . ')';

        $sql .= ' VALUES ' . '(' . implode(', ', $rows) .  ')';

        $query = $this->connection->query($sql);
        if ($query) {
            $object->setId($this->findMaxId());
        }

        $this->getManager()->initInstance($object);

        return $this;
    }

    public function findMaxId()
    {
        $result = $this->connection->query('SELECT max(id) as id FROM users')->fetch_array();
        return reset($result);
    }

    private function createAndFetch(array $row)
    {
        $mappedRow = [];
        foreach ($row as $key => $value) {
            $mappedRow[$this->dbMapping[$key]] = $value;
        }

        return $this->orm->createInstance($this->reflection, $mappedRow);
    }

    public function getConnection()
    {
        return $this->connection;
    }

    protected function getManager()
    {
        return $this->orm;
    }
}