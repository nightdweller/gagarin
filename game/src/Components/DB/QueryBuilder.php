<?php

namespace Components\DB;

class QueryBuilder
{
    protected $repository;
    protected $select = '*';
    protected $table;
    protected $and = [];
    protected $order = [];
    protected $limit = null, $offset = null;
    protected $parameters = [];

    public function __construct(Repository $repository)
    {
        $this->repository = $repository;
    }

    public function select($select)
    {
        $this->select = $select;

        return $this;
    }

    public function setTable($table)
    {
        $this->table = $table;

        return $this;
    }

    public function andWhere($sql)
    {
        $this->and[] = $sql;

        return $this;
    }

    public function addOrderBy($field, $type)
    {
        $this->order[$field] = $type;

        return $this;
    }

    public function skip($offset)
    {
        $this->offset = $offset;

        return $this;
    }

    public function limit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    public function setParameter($key, $value)
    {
        $this->parameters[$key] = $value;

        return $this;
    }

    public function getQuery()
    {
        $query = new Query($this->repository);

        $sql = sprintf('SELECT %s FROM %s', $this->select, $this->table);
        if ($this->and) {
            $sql .= ' WHERE ' . implode(' AND ', $this->and);
        }

        if ($this->order) {
            $orderBy = '';
            foreach ($this->order as $field => $type) {
                if ($orderBy) {
                    $orderBy .= ', ';
                }

                $orderBy .= $field . ' ' . $type;
            }

            $sql .= ' ORDER BY ' . $orderBy;
        }

        if ($this->offset || $this->limit) {
            $sql .= ' LIMIT ' . ($this->offset ?: 0) . ($this->limit ? ', ' . $this->limit : '');
        }

        $query
            ->setSql($sql)
            ->setParameters($this->parameters)
        ;

        return $query;
    }
}