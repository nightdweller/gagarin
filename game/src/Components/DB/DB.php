<?php

namespace Components\DB;

class DB
{
    protected $connection;

    public function __construct($host, $user, $password, $dbname)
    {
        $this->connection = new \mysqli($host, $user, $password, $dbname);
    }

    /**
     * @return \mysqli
     */
    public function getConnection()
    {
        return $this->connection;
    }

    public function updateOne($table, $ids, $data)
    {
        $values = [];
        foreach ($data as $key => $value) {
            if ($value instanceof \DateTime) {
                $value = $value->format('Y:m:d H:i:s');
            }

            $values[] = $key . ' = ' . '"' . $this->connection->real_escape_string($value) . '"';
        }
        $sql = 'UPDATE ' . $table . ' SET ' . implode(', ', $values);
        if (count($ids) == 1) {
            $sql .= ' WHERE id = ' . reset($ids);
        } else {
            $sql .= ' WHERE id IN (' . implode(', ', $ids) . ')';
        }

        return $this->connection->query($sql);
    }
}