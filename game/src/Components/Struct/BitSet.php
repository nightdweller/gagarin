<?php

namespace Components\Struct;

class BitSet
{
    const BASE = 32;
    const LEN = 5;

    protected $mask;

    public function __construct($mask = '')
    {
        $this->mask = $mask;
    }

    public function getMask()
    {
        return $this->mask;
    }

    public function decodeNumber($based)
    {
        return base_convert($based, self::BASE, 10);
    }

    public function encodeNumber($number)
    {
        return base_convert($number, 10, self::BASE);
    }

    public function add($id)
    {
        $index = (int)(--$id / self::LEN);
        $id -= $index * self::LEN;
        $digit = (1 << $id);

        $this->mask .= str_repeat('0', $index - strlen($this->mask) + 1);
        $this->mask[$index] = base_convert(intval($this->mask[$index], self::BASE) | $digit, 10, self::BASE);

        return $this;
    }

    public function remove($id)
    {
        $index = (int)(--$id / self::LEN);

        if ($index > strlen($this->mask)) {
            return $this;
        }

        $id -= $index * self::LEN;
        $digit = (1 << $id);

        $this->mask[$index] = base_convert(intval($this->mask[$index], self::BASE) & ~$digit, 10, self::BASE);

        return $this;
    }

    public function has($id)
    {
        if (!$this->mask) {
            return false;
        }

        $index = (int)(--$id / self::LEN);
        if ($index > strlen($this->mask)) {
            return false;
        }

        $id -= $index * self::LEN;
        $digit = (1 << $id);

        return (bool)(intval($this->mask[$index], self::BASE) & $digit);
    }

    public function getInset($count)
    {
        $ids = [];
        for ($len = 0, $index = 0, $i = 0; $i < strlen($this->mask); $i++, $index += self::LEN) {
            for ($x = intval($this->mask[$i], self::BASE), $m = $x--; $m; $x = $m &= $x, --$x) {
                $num = $m & ~$x;
                $ids[] = 1 + $index + ($num <= 4 ? $num >> 1 : 3 + ($num != 8));

                if (++$len >= $count    ) {
                    return $ids;
                }
            }
        }

        return $ids;
    }

    public function getOutset($count)
    {
        $ids = [];
        for ($len = 0, $index = 0, $i = 0, $num = 0; $i < strlen($this->mask); $i++, $index += self::LEN) {
            for ($x = intval($this->mask[$i], self::BASE), $m = $x++; $x < 31; $x = $m |= $x, ++$x) {
                $num = ~($m | ~$x);
                $ids[] = 1 + $index + ($num <= 4 ? $num >> 1 : 3 + ($num != 8));

                if (++$len >= $count) {
                    return $ids;
                }
            }
        }

        $inc = $num == (self::BASE >> 2) ? 2 : 1;

        if (empty($ids)) {
            $ids[] = ++$index;
        }

        while (count($ids) < $count) {
            $ids[] = end($ids) + $inc;
        }

        return $ids;
    }

    public function __toString()
    {
        return (string)$this->mask;
    }
}