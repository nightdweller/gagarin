<?php

namespace Components;

class JsonResponse extends Response
{
    public function sendContent()
    {
        echo json_encode($this->content);

        return $this;
    }
}