<?php

namespace Components\Form;

interface FormTypeInterface
{
    public function build(FormBuilder $builder);

    public function getName();
}