<?php

namespace Components;

class RedirectResponse
{
    protected $location;

    public function __construct($location)
    {
        $this->location = $location;
    }

    public function send()
    {
        $this->sendHeaders();
        $this->sendContent();
    }

    public function sendHeaders()
    {
        header('Content-Type: text/html; charset=utf-8');
        header('Location: ' . $this->location);
    }

    public function sendContent()
    {
        return $this;
    }
}