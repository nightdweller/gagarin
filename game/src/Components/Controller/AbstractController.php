<?php

namespace Components\Controller;

use Components\Container\ContainerInterface;
use Components\Form\FormBuilder;
use Components\PhpTemplate\Template;
use Components\RedirectResponse;
use Components\Response;

class AbstractController
{
    protected $container;
    protected $dir;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    protected function render($view, array $parameters = [])
    {
        /** @var Template $template */
        $template = $this->container->get('php_template');

        return new Response($template->render($this->container->getViewsDir(), $view, $parameters));
    }

    protected function getResource($path)
    {
        return $this->container->getViewsDir() . DIRECTORY_SEPARATOR . $path;
    }

    protected function redirect($location)
    {
        return new RedirectResponse($location);
    }

    protected function createFormBuilder($data)
    {
        return new FormBuilder('form', null, $data);
    }
}