<?php

namespace Components\Console;

use Components\Container\ContainerInterface;

class Console
{
    protected $commands;
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function initCommands()
    {
        if (!is_dir($dir = $this->container->getAppDir() . '/Command')) {
            return;
        }

        $commands = array_map(function ($path) {
            $matches = [];
            preg_match('/(\w+)Command.php/', $path, $matches);
            return $this->container->getEnv() . '\\Command\\' . $matches[1] . 'Command';
        }, glob($dir . '/*Command.php'));

        foreach ($commands as $class) {
            $this->add(new $class($this->container));
        }
    }

    public function add(CommandInterface $command)
    {
        $this->commands[$command->getName()] = $command;
    }

    public function run(ArgvInput $input)
    {
        if (!isset($this->commands[$input->getCommandName()])) {
            throw new \Exception('Нет такой команды');
        }

        $this->commands[$input->getCommandName()]->execute($input);
    }
}