<?php

namespace Components\Console;

use Symfony\Component\Console\Input\Input;

interface CommandInterface
{
    public function getName();

    public function execute(ArgvInput $input);
}