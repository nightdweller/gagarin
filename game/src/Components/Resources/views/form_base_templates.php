<?php

function form_attributes($form)
{
    if (!isset($form['attr'])) {
        return;
    }

    foreach ($form['attr'] as $key => $value) {
        echo(' ' . $key . '="' . $value . '"');
    }
}

function form_value($form)
{
    if (!isset($form['value'])) {
        return;
    }

    echo ('value="' . $form['value'] . '"');
}

return [
    'text' => function ($form, \Components\Form\FormView $view) { ?>
        <input type="text" name="<?=$form['name']?>" <?form_value($form)?> <?form_attributes($form)?> />
    <? },
    'integer' => function ($form, \Components\Form\FormView $view) { ?>
        <input type="number" name="<?=$form['name']?>" <?form_value($form)?> <?form_attributes($form)?> />
    <? },
    'email' => function ($form, \Components\Form\FormView $view) {?>
        <input type="email" name="<?=$form['name']?>" <?form_value($form)?> <?form_attributes($form)?> />
    <? },
    'choice' => function ($form, \Components\Form\FormView $view) {?>
        <select name="<?=$form['name']?>" <?form_attributes($form)?>>
            <? foreach ($form['choices'] as $key => $value) { ?>
                <option value="<?=$key?>" <?= isset($form['value']) && $form['value'] == $key ? 'selected' : '' ?> ><?=$value?></option>
            <? } ?>
        </select>
    <?},
    'textarea' => function ($form, \Components\Form\FormView $view) {?>
        <textarea name="<?=$form['name']?>" <?form_attributes($form)?>><?=isset($form['value']) ? $form['value'] : ''?></textarea>
    <?},
];