<?php

namespace Components;

class Utils
{
    public static function pluralize($number, $array)
    {
        if (10 <= $number && $number < 20) {
            return $array[2];
        }

        $pd = $number % 10;
        if (!$pd || $pd > 4) {
            return $array[2];
        }

        if ($pd == 1) {
            return $array[0];
        }

        return $array[1];
    }
}