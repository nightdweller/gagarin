<?php

namespace Components;

class Response
{
    protected $content;
    protected $headers = [];

    public function __construct($content)
    {
        $this->content = $content;
    }

    public function send()
    {
        $this->sendHeaders();
        $this->sendContent();
    }

    public function sendHeaders()
    {
        if ($this->headers) {
            foreach ($this->headers as $key => $value) {
                header($key . ': ' . $value);
            }
        } else {
            header('Content-Type: text/html; charset=utf-8');
        }
    }

    public function addHeader($key, $value)
    {
        $this->headers[$key] = $value;
        return $this;
    }

    public function sendContent()
    {
        echo $this->content;

        return $this;
    }
}