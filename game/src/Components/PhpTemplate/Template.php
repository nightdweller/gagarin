<?php

namespace Components\PhpTemplate;

use Components\Container\Container;
use Components\Container\ContainerInterface;

class Template
{
    public $container;
    public $view = null;
    public $params;
    public $dir;
    public $basePath;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->basePath = $_SERVER['BASE'];
    }

    public function render($path, $view, $parameters = [])
    {
        $this->dir = $path;
        $this->params = $parameters;

        $this->view = $view;

        ob_start();
        while ($this->view) {
            $view = $this->view;
            $this->view = null;

            include($path . DIRECTORY_SEPARATOR . $view);
        }

        $result = ob_get_contents();
        ob_clean();

        $this->params = null;
        return $result;
    }

    public function getUser()
    {
        return $this->container->get('user_token')->getUser();
    }

    public function url($path, $params = null)
    {
        $scheme = isset($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] : ($_SERVER['HTTP_X_FORWARDED_PROTO']);

        return $scheme . '://' . $_SERVER['HTTP_HOST'] . $this->basePath . '/' . $path . ($params ? '?' . http_build_query($params) : '');
    }

    public function asset($path)
    {
        return $this->basePath . '/' . $path;
    }

    public function fileAsset($path)
    {
        return $this->basePath . '/' . $path;
    }

    public function urlMatch($pattern)
    {
        return preg_match($pattern, $this->container->get('request')->getPath());
    }

    private function extendsFrom($view)
    {
        $this->view = $view;
    }

    private function prepare($view)
    {
        return $this->dir . DIRECTORY_SEPARATOR . $view;
    }

    private function pluralize($number, $one, $two, $five)
    {
        if (5 <= $number && $number <= 20) {
            return $five;
        }

        $digit = $number % 10;

        if ($digit == 1) {
            return $one;
        }

        if ($digit && $digit <= 4) {
            return $two;
        }

        return $five;
    }

    public function get($key)
    {
        return $this->params[$key];
    }
}