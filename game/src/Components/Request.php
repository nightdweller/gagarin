<?php

namespace Components;

class Request
{
    protected $path;

    public function __construct()
    {
        $this->path = mb_strcut($_SERVER['REDIRECT_URL'], strlen($_SERVER['BASE']));
    }

    public function get($key, $default = null) {
        if (array_key_exists($key, $_GET)) {
            return $_GET[$key];
        }

        return $default;
    }

    public function post($key, $default = null) {
        if (array_key_exists($key, $_POST)) {
            return $_POST[$key];
        }

        return $default;
    }

    public function request($key, $default = null) {
        if (array_key_exists($key, $_REQUEST)) {
            return $_REQUEST[$key];
        }

        return $default;
    }

    public function asset($path, array $params = null)
    {
        $scheme = isset($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] : ($_SERVER['HTTP_X_FORWARDED_PROTO']);

        return $scheme . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['BASE'] . '/' . $path . ($params ? '?' . http_build_query($params) : '');
    }

    public function getScheme()
    {
        return isset($_SERVER['REQUEST_SCHEME']) ? $_SERVER['REQUEST_SCHEME'] : ($_SERVER['HTTP_X_FORWARDED_PROTO']);
    }

    public function all($method = 'get')
    {
        if ($method == 'get') {
            return $_GET;
        }

        return $_POST;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }
}