<?php

namespace Vk\HttpClient;

class Response extends \Buzz\Message\Response
{
    private $arrayResult;

    public function getObjectResult()
    {
        if (null == $this->arrayResult) {
            $this->arrayResult = json_decode($this->getContent());
            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new \Exception($this->getContent());
            }
        }

        return $this->arrayResult;
    }

    public function getArrayResult($field = null)
    {
        if (null == $this->arrayResult) {
            $this->arrayResult = json_decode($this->getContent(), true);
            if (json_last_error() !== JSON_ERROR_NONE) {
                throw new \Exception($this->getContent());
            }
        }

        if (null !== $field) {
            if (null !== $this->arrayResult && isset($this->arrayResult[$field])) {
                return $this->arrayResult[$field];
            }
            return null;
        }

        return $this->arrayResult;
    }

    public function getResponse()
    {
        $result = $this->getArrayResult();
        if (!isset($result['response'])) {
            return [];
        }

        $result = $result['response'];
        if (isset($result[0]) && !is_array($result[0])) {
            unset($result[0]);
        }
        return $result;
    }

    public function getItems()
    {
        $result = $this->getResponse();
        return array_slice($result, 1);
    }

    public function getSingleResponse()
    {
        $response = $this->getResponse();
        return reset($response);
    }

    public function isSuccessful()
    {
        if (parent::isSuccessful()) {
            $result = $this->getArrayResult();
            return !array_key_exists('error', $result);
        }

        return false;
    }

    public function getReasonPhrase()
    {
        $response = $this->getArrayResult();

        if (array_key_exists('error_msg', $response)) {
            return $response['error_msg'];
        }

        if (array_key_exists('error_description', $response)) {
            return $response['error_description'];
        }

        return 'Unknown error';
    }
}