<?php

namespace Vk\HttpClient;

use Buzz\Client\Curl;
use Buzz\Message\MessageInterface;
use Buzz\Message\Request;
use Buzz\Message\RequestInterface;

class HttpClient
{
    protected $host = 'https://api.vk.com/method/';
    protected $params;
    protected $client;

    public function __construct($params = [], $host = null)
    {
        $this->params = $params;

        if ($host) {
            $this->host = $host;
        }
    }

    public function set($key, $value)
    {
        $this->params[$key] = $value;
    }

    public function get($method, $params = [])
    {
        $resource = $this->generateResource($method, $this->prepareData($params));
        $request = $this->createRequest(Request::METHOD_GET, $resource);

        return $this->send($request);
    }

    public function post($method, $params = [])
    {
        $resource = $this->generateResource($method);

        $request = $this->createRequest(Request::METHOD_POST, $resource);
        $request->addHeader('Content-type: application/json');
        $request->setContent($this->prepareData($params));

        return $this->send($request);
    }

    public function send(RequestInterface $request, MessageInterface $response = null)
    {
        if (is_null($response)) {
            $response = $this->createResponse();
        }

        $client = $this->getClient();
        $client->send($request, $response);

        return $response;
    }

    protected function createRequest($method, $resource)
    {
        return new Request($method, $resource, $this->host);
    }

    protected function createResponse()
    {
        return new Response();
    }

    protected function getClient()
    {
        if ($this->client) {
            return $this->client;
        }

        $this->client = new Curl();

        return $this->client;
    }

    protected function generateResource($method, $params = [])
    {
        $resource = $method;

        if ($params) {
            return $resource . '?' . http_build_query($this->prepareData($params));
        }

        return $resource;
    }

    protected function prepareData($params = [])
    {
        if (!$this->params) {
            return $params;
        }

        return array_merge($params, $this->params);
    }
}