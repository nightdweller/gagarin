<?php

namespace Vk;

use Vk\HttpClient\HttpClient;

abstract class Api
{
    protected $client;

    public function __construct(HttpClient $client)
    {
        $this->client = $client;
    }

    public function getClient()
    {
        return $this->client;
    }
}