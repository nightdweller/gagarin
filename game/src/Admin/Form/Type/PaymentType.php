<?php

namespace Admin\Form\Type;

use Components\Form\FormBuilder;
use Components\Form\FormType;
use Components\Form\FormTypeInterface;

class PaymentType implements FormTypeInterface
{
    public function build(FormBuilder $builder)
    {
        $builder
            ->add('orderId', 'text')
            ->add('userId', 'text')
            ->add('type', 'text')
        ;
    }

    public function getName()
    {
        return 'payment';
    }
}