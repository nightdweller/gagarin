<?php

namespace Admin\Form\Type;

use App\Container\Container;
use App\Fabric\FabricRegistry;
use Components\Form\FormBuilder;
use Components\Form\FormTypeInterface;

class CriteriaType implements FormTypeInterface
{
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function build(FormBuilder $builder)
    {
        $fabrics = FabricRegistry::all();

        foreach ($fabrics as $key => $fabric) {
            $builder
                ->add($fabric->getIndex(), 'integer', [
                    'label' => $fabric->getName()
                ])
            ;
        }
    }

    public function getName()
    {
        return 'criteria';
    }
}