<?php

namespace Admin\Container;

class Container extends \App\Container\Container
{
    protected $env = 'Admin';

    public function __construct($kernel, $config = null)
    {
        $webDir = WEB_DIR . '/..';
        $this->configDir = $webDir . DIRECTORY_SEPARATOR . '../app/admin/';
        $this->srcDir = $webDir . DIRECTORY_SEPARATOR . '../src';
        $this->appDir = $this->srcDir . DIRECTORY_SEPARATOR . $this->env;
        $this->viewsDir = $this->appDir . '/Resources/views';

        $config = require_once($this->configDir . 'config.php');
        $components = require_once($this->configDir . 'components.php');

        $this->config = array_merge($config, $components);

        $this
            ->add(self::KERNEL_ID, ['class' => get_class($kernel)])
            ->set(self::KERNEL_ID, $kernel)
            ->add(self::CONTAINER_ID, ['class' => get_class($this)])
            ->set(self::CONTAINER_ID, $this)
        ;
    }
}
