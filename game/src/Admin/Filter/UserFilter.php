<?php

namespace Admin\Filter;

class UserFilter
{
    protected $vkId;

    public function getVkId()
    {
        return $this->vkId;
    }

    public function setVkId($vkId)
    {
        $this->vkId = $vkId;
        return $this;
    }
}