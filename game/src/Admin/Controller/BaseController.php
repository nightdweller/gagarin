<?php

namespace Admin\Controller;
use Components\Controller\AbstractController;
use Admin\Container\Container;

/**
 * @property Container $container
 */
class BaseController extends AbstractController
{
    protected $container;
}