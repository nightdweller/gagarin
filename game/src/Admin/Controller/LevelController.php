<?php

namespace Admin\Controller;

use Admin\Form\Type\CriteriaType;
use App\Entity\Level;
use App\Pagination\Pagination;
use Components\Request;

class LevelController extends BaseController
{
    public function listAction(Request $request)
    {
        $qb = $this
            ->container
            ->getLevelRepository()
            ->createQueryBuilder()
            ->addOrderBy('id', 'asc')
        ;

        $pagination = new Pagination($qb, $request->get('page', 1));

        return $this->render('Level/list.php', [
            'items' => $pagination
        ]);
    }

    public function editAction(Request $request)
    {
        $id = $request->get('id');
        $item = $this->container->getLevelRepository()->find($id);

        return $this->edit($request, $item);
    }

    public function newAction(Request $request)
    {
        $item = new Level();

        return $this->edit($request, $item);
    }

    public function edit(Request $request, Level $item)
    {
        $form = $this->createFormBuilder($item)
            ->add('name', 'text', ['label' => 'Название'])
            ->add('number', 'integer', ['label' => 'Номер'])
            ->add('criteria', new CriteriaType($this->container), [
                'label' => 'Фабрики'
            ])
            ->getForm()
        ;

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->container->getLevelRepository()->insert($item);
            $this->container->getORM()->flush($item);

            return $this->redirect($request->asset('levels'));
        }

        return $this->render('Level/edit.php', [
            'item' => $item,
            'form' => $form->createView()->setTheme($this->getResource('Form/level.php'))
        ]);
    }
}