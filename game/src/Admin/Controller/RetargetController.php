<?php

namespace Admin\Controller;

use App\Entity\Mission;
use App\Pagination\Pagination;
use Components\Request;
use Components\Response;

class RetargetController extends BaseController
{
    public function listAction(Request $request)
    {
        return $this->render('Retarget/list.php');
    }

    public function allAction(Request $request)
    {
        $ids = $this
            ->container
            ->getUserRepository()
            ->createQueryBuilder()
            ->select('vk_id')
            ->andWhere('vk_id > 0')
            ->getQuery()
            ->getArrayResult()
        ;
        $ids = array_map(function ($row) {
            return reset($row);
        }, $ids);

        $response = new Response(implode("\n", $ids));
        $response
            ->addHeader('Content-Type', 'application/octet-stream; name="gagarin.txt"')
            ->addHeader('Content-Disposition', 'attachment; filename="gagarin.txt"')
        ;

        return $response;
    }
}