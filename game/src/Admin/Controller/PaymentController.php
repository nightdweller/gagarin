<?php

namespace Admin\Controller;

use Admin\Filter\PaymentFilter;
use Admin\Form\Type\PaymentType;
use App\Entity\Payment;
use App\Pagination\Pagination;
use Components\Form\FormBuilder;
use Components\Request;

class PaymentController extends BaseController
{
    public function listAction(Request $request)
    {
        $filter = new PaymentFilter();
        $form = $this
            ->createFormBuilder($filter)
            ->setMethod('get')
            ->add('votes', 'choice', [
                'choices' => [
                    'all' => 'Все',
                    'votes' => 'За голоса'
                ],
                'label' => 'Вид платежа'
            ])
            ->add('type', 'choice', [
                'choices' => [
                    '' => 'Все',
                    'money' => 'Септимы',
                    'fabric' => 'Фабрики',
                    'energy' => 'Энергия'
                ],
                'label' => 'Товар'
            ])
            ->getForm()
        ;
        $form->handleRequest($request);

        $qb = $this
            ->container
            ->getPaymentRepository()
            ->createFilteredQueryBuilder($filter)
            ->addOrderBy('id', 'asc')
        ;


        $pagination = new Pagination($qb, $request->get('page', 1), 500);

        $item = ($pagination->getItems()[0]);
        /** @var Payment $payment */
        foreach ($pagination as $payment) {
            $payment->setUser($this->container->getUserRepository()->find($payment->getUserId()));
        }

        return $this->render('Payment/list.php', [
            'payments' => $pagination,
            'form' => $form->createView()
        ]);
    }

    public function editAction(Request $request)
    {
        $id = $request->get('id');
        $item = $this->container->getPaymentRepository()->find($id);

        return $this->edit($request, $item);
    }

    public function edit(Request $request, Payment $item)
    {
        $form = $this->createFormBuilder($item)
            ->add('orderId')
            ->add('userId')
            ->add('type')
            ->getForm()
        ;

        $form->handleRequest($request);

        return $this->render('Payment/edit.php', [
            'item' => $item,
            'form' => $form->createView()
        ]);
    }
}