<?php

namespace Admin\Controller;

use App\Entity\Mission;
use App\Pagination\Pagination;
use Components\Request;

class MissionController extends BaseController
{
    public function listAction(Request $request)
    {
        $qb = $this
            ->container
            ->getMissionRepository()
            ->createQueryBuilder()
            ->addOrderBy('id', 'asc')
        ;

        $pagination = new Pagination($qb, $request->get('page', 1));

        return $this->render('Mission/list.php', [
            'items' => $pagination
        ]);
    }

    public function editAction(Request $request)
    {
        $id = $request->get('id');
        $item = $this->container->getMissionRepository()->find($id);

        return $this->edit($request, $item);
    }

    public function newAction(Request $request)
    {
        $item = new Mission();

        return $this->edit($request, $item);
    }

    public function edit(Request $request, Mission $item)
    {
        $form = $this->createFormBuilder($item)
            ->add('name', 'text', ['label' => 'Название'])
            ->add('number', 'text', ['label' => 'Number'])
            ->add('type', 'choice', [
                'choices' => Mission::$types,
                'label' => 'Тип'
            ])
            ->add('money', 'integer', ['label' => 'Септимы за прохождение'])
            ->add('description', 'textarea', ['label' => 'Описание'])
            ->add('message', 'textarea', ['label' => 'Сообщение после выполнения'])
            ->add('image', 'text', ['label' => 'Изображение'])
            ->add('extra', 'textarea', ['label' => 'JSON объект Mission.extra'])
            ->getForm()
        ;

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->container->getMissionRepository()->insert($item);
            $this->container->getORM()->flush($item);

            return $this->redirect($request->asset('missions'));
        }

        return $this->render('Mission/edit.php', [
            'item' => $item,
            'form' => $form->createView()
        ]);
    }
}