<?php

namespace Admin\Controller;

use Admin\Form\Type\PaymentType;
use Components\Form\FormBuilder;
use Components\Request;

class DefaultController extends BaseController
{
    public function testAction(Request $request)
    {
        $user = $this->container->getUser();

        $user->update();
        var_dump($user->getEnergy());
        var_dump($user->getLastEnergyUpdate());
    }

    public function indexAction(Request $request)
    {
        return $this->render('Default/index.php', []);
    }
}