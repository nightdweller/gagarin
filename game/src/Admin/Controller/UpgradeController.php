<?php

namespace Admin\Controller;

use App\Entity\Mission;
use App\Entity\Upgrade;
use App\Fabric\FabricRegistry;
use App\Pagination\Pagination;
use Components\Request;

class UpgradeController extends BaseController
{
    public function listAction(Request $request)
    {
        $qb = $this
            ->container
            ->getUpgradeRepository()
            ->createQueryBuilder()
            ->addOrderBy('id', 'asc')
        ;

        $pagination = new Pagination($qb, $request->get('page', 1));

        return $this->render('Upgrade/list.php', [
            'items' => $pagination
        ]);
    }

    public function editAction(Request $request)
    {
        $id = $request->get('id');
        $item = $this->container->getUpgradeRepository()->find($id);

        return $this->edit($request, $item);
    }

    public function newAction(Request $request)
    {
        $item = new Upgrade();

        return $this->edit($request, $item);
    }

    public function edit(Request $request, Upgrade $item)
    {
        $form = $this->createFormBuilder($item)
            ->add('fabric', 'choice', [
                'label' => 'Фабрика',
                'choices' => FabricRegistry::getChoices()
            ])
            ->add('name', 'text', ['label' => 'Название'])
            ->add('description', 'textarea', ['label' => 'Описание'])
            ->add('message', 'textarea', ['label' => 'Сообщение после покупки'])
            ->add('code', 'textarea', ['label' => 'php код'])
            ->getForm()
        ;

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->container->getUpgradeRepository()->insert($item);
            $this->container->getORM()->flush($item);

            return $this->redirect($request->asset('upgrades'));
        }

        return $this->render('Upgrade/edit.php', [
            'item' => $item,
            'form' => $form->createView()
        ]);
    }
}