<?php

namespace Admin\Controller;

use Admin\Filter\UserFilter;
use Admin\Form\Type\PaymentType;
use App\Entity\Payment;
use App\Entity\User;
use App\Pagination\Pagination;
use Components\Form\FormBuilder;
use Components\Request;

class UserController extends BaseController
{
    public function listAction(Request $request)
    {
        $filter = new UserFilter();
        $form = $this
            ->createFormBuilder($filter)
            ->setMethod('get')
            ->add('vkId', 'integer', ['label' => 'vk id'])
            ->getForm()
        ;

        $form->handleRequest($request);

        $qb = $this
            ->container
            ->getUserRepository()
            ->createFilteredQueryBuilder($filter)
            ->addOrderBy('clicks', 'desc')
        ;

        $pagination = new Pagination($qb, $request->get('page', 1), 25);

        return $this->render('User/list.php', [
            'users' => $pagination,
            'form' => $form->createView()
        ]);
    }

    public function editAction(Request $request)
    {
        $id = $request->get('id');
        $item = $this->container->getUserRepository()->find($id);

        return $this->edit($request, $item);
    }

    public function edit(Request $request, User $item)
    {
        $form = $this->createFormBuilder($item)
            ->add('id')
            ->add('vkId')
            ->add('city')
            ->add('level')
            ->add('money')
            ->getForm()
        ;

        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->container->getORM()->flush($item);

            return $this->redirect($request->asset('users'));
        }

        return $this->render('User/edit.php', [
            'item' => $item,
            'form' => $form->createView()
        ]);
    }
}