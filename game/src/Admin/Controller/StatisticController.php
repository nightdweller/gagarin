<?php

namespace Admin\Controller;

use App\Entity\Mission;
use App\Pagination\Pagination;
use Components\Request;

class StatisticController extends BaseController
{
    public function dateAction(Request $request)
    {
        $rows = $this->container->getStatisticRepository()->all();
        return $this->render('Statistic/date.php', [
            'rows' => $rows
        ]);
    }
}