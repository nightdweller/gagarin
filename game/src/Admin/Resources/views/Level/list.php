<?
    include $this->prepare('Pagination/pagination.php');
    $this->extendsFrom('layout.php');
    $title = 'Уровни';
    $description = '<a href="' . $this->url('levels/new') . '">Добавить уровень</a>';
    $breadcrumbs = [
        ['name' => 'Уровни']
    ]
?>
<? function content(\Components\PhpTemplate\Template $template) { ?>
    <?
    /**
     * @var \App\Entity\Level[] $items
     */
    $items = $template->params['items'];
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tr>
                        <th>id</th>
                        <th>Название</th>
                        <th>Номер</th>
                        <th>Операции</th>
                    </tr>
                    <? foreach ($items as $item) { ?>
                        <tr>
                            <td><?=$item->getId()?></td>
                            <td><?=$item->getName()?></td>
                            <td><?=$item->getNumber()?></td>
                            <td>
                                <a href="<?=$template->url('levels/edit', ['id' => $item->getId()])?>" class="btn btn-xs btn-info">
                                    редактировать
                                </a>
                            </td>
                        </tr>
                    <? } ?>
                </table>
            </div>
            <?=pagination($items)?>
        </div>
    </div>
<? } ?>