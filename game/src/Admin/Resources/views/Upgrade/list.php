<?
    include $this->prepare('Pagination/pagination.php');
    $this->extendsFrom('layout.php');
    $title = 'Модификации';
    $description = '<a href="' . $this->url('upgrades/new') . '">Добавить модификацию</a>';
    $breadcrumbs = [
        ['name' => 'Модификации']
    ]
?>
<? function content(\Components\PhpTemplate\Template $template) { ?>
    <?
    /**
     * @var \App\Entity\Upgrade[] $items
     */
    $items = $template->params['items'];
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tr>
                        <th>id</th>
                        <th>Фабрика</th>
                        <th>Название</th>
                        <th>Операции</th>
                    </tr>
                    <? foreach ($items as $item) { ?>
                        <tr>
                            <td><?=$item->getId()?></td>
                            <td><?=\App\Fabric\FabricRegistry::getChoices()[$item->getFabric()]?></td>
                            <td><?=$item->getName()?></td>
                            <td>
                                <a href="<?=$template->url('upgrades/edit', ['id' => $item->getId()])?>" class="btn btn-xs btn-info">
                                    редактировать
                                </a>
                            </td>
                        </tr>
                    <? } ?>
                </table>
            </div>
            <?=pagination($items)?>
        </div>
    </div>
<? } ?>