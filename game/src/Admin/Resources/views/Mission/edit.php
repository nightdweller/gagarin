<?
    include $this->prepare('Pagination/pagination.php');
    $this->extendsFrom('layout.php');
    $title = 'Миссии';
    $description = 'Редактирование миссии';
    $breadcrumbs = [
        ['name' => 'Миссии', 'url' => $this->url('missions')],
        ['name' => '#' . $this->params['item']->getId() ?: 'Добавление миссии']
    ]
?>
<? function content(\Components\PhpTemplate\Template $template) { ?>
    <?
    /**
     * @var \Components\Form\FormView $form
     */
    $form = $template->params['form'];
    ?>
    <div class="row">
        <div class="col-lg-12">
            <?=$form->formStart()?>
                <?foreach ($form as $child) { ?>
                    <div class="form-group">
                        <label><?=$child['label']?></label>
                        <?=$form->widget($child, ['attr' => ['class' => 'form-control']])?>
                    </div>
                <? } ?>
                <button type="submit" class="btn btn-default">Сохранить</button>
            <?=$form->formEnd()?>
        </div>
    </div>
<? } ?>