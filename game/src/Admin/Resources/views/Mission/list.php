<?
    include $this->prepare('Pagination/pagination.php');
    $this->extendsFrom('layout.php');
    $title = 'Миссии';
    $description = '<a href="' . $this->url('missions/new') . '">Добавить миссию</a>';
    $breadcrumbs = [
        ['name' => 'Миссии']
    ]
?>
<? function content(\Components\PhpTemplate\Template $template) { ?>
    <?
    /**
     * @var \App\Entity\Mission[] $items
     */
    $items = $template->params['items'];
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tr>
                        <th>id</th>
                        <th>Название</th>
                        <th>Картинка</th>
                        <th>Тип</th>
                        <th>Операции</th>
                    </tr>
                    <? foreach ($items as $item) { ?>
                        <tr>
                            <td><?=$item->getId()?></td>
                            <td><?=$item->getName()?></td>
                            <td><?=$item->getImage()?></td>
                            <td><?=\App\Entity\Mission::$types[$item->getType()]?></td>
                            <td>
                                <a href="<?=$template->url('missions/edit', ['id' => $item->getId()])?>" class="btn btn-xs btn-info">
                                    редактировать
                                </a>
                            </td>
                        </tr>
                    <? } ?>
                </table>
            </div>
            <?=pagination($items)?>
        </div>
    </div>
<? } ?>