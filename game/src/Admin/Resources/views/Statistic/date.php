<?
    include $this->prepare('Pagination/pagination.php');
    $this->extendsFrom('layout.php');
    $title = 'Мобайл стата';
    $description = 'Мобайл стата';
    $breadcrumbs = [
        ['name' => 'Мобайл стата']
    ]
?>
<? function content(\Components\PhpTemplate\Template $template) { ?>
    <?
    $rows = $template->params['rows'];
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tr>
                        <th>Дата</th>
                        <th>Запуски</th>
                        <th>Установки</th>
                    </tr>
                    <? foreach ($rows as $row) { ?>
                        <tr>
                            <td><?=$row['d']?></td>
                            <td><?=$row['shows']?></td>
                            <td><?=$row['installs']?></td>
                        </tr>
                    <? } ?>
                </table>
            </div>
        </div>
    </div>
<? } ?>