<?php

return [
    'criteria' => function ($form, \Components\Form\FormView $view) { ?>
        <div class="row">
            <tr>
        <? foreach ($form['children'] as $child): ?>
            <div class="col-lg-1">
            <label><?=$child['label']?></label>
            <?=$view->widget($child, ['attr' => ['class' => 'form-control']])?>
            </div>
        <? endforeach ?>
            </tr>
        </div>
    <? },
];