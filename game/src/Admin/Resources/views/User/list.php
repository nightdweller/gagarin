<?
    include $this->prepare('Pagination/pagination.php');
    $this->extendsFrom('layout.php');
    $title = 'Пользователи';
    $description = 'Список пользователей';
    $breadcrumbs = [
        ['name' => 'Список']
    ]
?>
<? function content(\Components\PhpTemplate\Template $template) { ?>
    <?
    /**
     * @var \Components\Form\FormView $form
     */
    $form = $template->params['form'];
    ?>
    <div class="row">
        <div class="col-lg-12">
            <?=$form->formStart()?>
            <?foreach ($form as $child) { ?>
                <div class="form-group">
                    <label><?=$child['label']?></label>
                    <?=$form->widget($child, ['attr' => ['class' => 'form-control']])?>
                </div>
            <? } ?>
            <button type="submit" class="btn btn-default">Фильтровать</button>
            <?=$form->formEnd()?>
        </div>
    </div>
    <?
    /**
     * @var \App\Entity\User[] $users
     */
    $users = $template->params['users'];
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tr>
                        <th>id</th>
                        <th>vk id</th>
                        <th>city</th>
                        <th>level</th>
                        <th>money</th>
                        <th>incValue</th>
                        <th>sum</th>
                        <th>info</th>
                        <th></th>
                        <th>Операции</th>
                    </tr>
                    <? foreach ($users as $user) {
                        $fabrics = $user->getFabrics();
                        $str = '';
                        foreach ($fabrics as $fabric) {
                            $str .= $fabric->getName() . ':' . $fabric->getCount() . '; ';
                        }
                    ?>
                        <tr>
                            <td><?=$user->getId()?></td>
                            <td><a href="//vk.com/id<?=$user->getVkId()?>" target="_blank"><?=$user->getVkId()?></a></td>
                            <td>
                                <?=$user->getCity()?>
                            </td>
                            <td><?=$user->getLevel()?></td>
                            <td><?=$user->getMoney()?></td>
                            <td><?=$user->getIncValue()?></td>
                            <td><?=$user->getSum() - $user->getMoney()?></td>
                            <td><?=$user->getInfo()?></td>
                            <td><?=$str?></td>
                            <td>
                                <a href="<?=$template->url('users/edit', ['id' => $user->getId()])?>" class="btn btn-xs btn-info">
                                    редактировать
                                </a>
                            </td>
                        </tr>
                    <? } ?>
                </table>
            </div>
            <?=pagination($users)?>
        </div>
    </div>
<? } ?>