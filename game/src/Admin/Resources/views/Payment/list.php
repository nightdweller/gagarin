<?
    include $this->prepare('Pagination/pagination.php');
    $this->extendsFrom('layout.php');
    $title = 'Платежи';
    $description = 'Список платежей';
    $breadcrumbs = [
        ['name' => 'Список']
    ]
?>
<? function content(\Components\PhpTemplate\Template $template) { ?>
    <?
    /**
     * @var \Components\Form\FormView $form
     */
    $form = $template->params['form'];
    ?>
    <div class="row">
        <div class="col-lg-12">
            <?=$form->formStart()?>
            <?foreach ($form as $child) { ?>
                <div class="form-group">
                    <label><?=$child['label']?></label>
                    <?=$form->widget($child, ['attr' => ['class' => 'form-control']])?>
                </div>
            <? } ?>
            <button type="submit" class="btn btn-default">Фильтровать</button>
            <?=$form->formEnd()?>
        </div>
    </div>
    <?
    /**
     * @var \App\Entity\Payment[] $payments
     */
    $payments = $template->params['payments'];
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tr>
                        <th>id</th>
                        <th>Тип платежа</th>
                        <th>index</th>
                        <th>vk id</th>
                        <th>order id</th>
                        <th>Операции</th>
                    </tr>
                    <? foreach ($payments as $payment) { ?>
                        <?
                            /** @var \App\Entity\User $user */
                            $user = $template->container->getUserRepository()->find($payment->getUserId());
                        ?>
                        <tr>
                            <td><?=$payment->getId()?></td>
                            <td><?=$payment->getType()?></td>
                            <td><?=$payment->getIndex()?></td>
                            <td>
                                <?if ($payment->getUser()) : ?>
                                    <a href="//vk.com/id<?=$payment->getUser()->getVkId();?>" target="_blank">
                                        <?=$payment->getUser()->getVkId();?>
                                    </a>
                                <?endif?>
                            </td>
                            <td><?=$payment->getOrderId()?></td>
                            <td>
                                <a href="<?=$template->url('payments/edit', ['id' => $payment->getId()])?>" class="btn btn-xs btn-info">
                                    редактировать
                                </a>
                            </td>
                        </tr>
                    <? } ?>
                </table>
            </div>
            <?=pagination($payments)?>
        </div>
    </div>
<? } ?>