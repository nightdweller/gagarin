<? function pagination(\App\Pagination\Pagination $pagination) { ?>
    <nav style="float: right;">
        <ul class="pagination">
            <? if (!$pagination->isFirstPage()) { ?>
                <li>
                    <a href="?page=<?=$pagination->getPrevPage()?>" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
            <? } ?>
            <?
                $firstPage = max(1, $pagination->getCurrentPage() - 2);
                $lastPage = min($firstPage + 4, $pagination->getLastPage());
                if ($lastPage - $firstPage < 4) {
                    $firstPage = max(1, $lastPage - 4);
                }
            ?>

            <? if ($firstPage > 1) { ?>
                <li><a href="?page=1">1</a></li>
                <? if ($firstPage > 2) { ?>
                    <li><a>...</a></li>
                <? } ?>
            <? } ?>

            <? for ($page = $firstPage; $page <= $lastPage; $page++) { ?>
                <li <?=$page == $pagination->getCurrentPage() ? 'class="active"' : ''?> ><a href="?page=<?=$page?>"><?=$page?></a></li>
            <? } ?>

            <? if ($lastPage < $pagination->getLastPage()) { ?>
                <? if ($lastPage < $pagination->getLastPage() - 1) { ?>
                    <li><a>...</a></li>
                <? } ?>
                <li><a href="?page=<?=$pagination->getLastPage()?>"><?=$pagination->getLastPage()?></a></li>
            <? } ?>

            <? if (!$pagination->isLastPage()) { ?>
                <li>
                    <a href="?page=<?=$pagination->getNextPage()?>" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            <? } ?>
        </ul>
    </nav>
<? } ?>