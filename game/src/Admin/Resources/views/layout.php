<?
$breadcrumbs = array_merge([
    ['name' => 'Главная', 'url' => $this->fileAsset(''), 'icon' => 'dashboard'],
], isset($breadcrumbs) ? $breadcrumbs : []);
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Гагарин админка</title>

    <link href="<?=$this->fileAsset('css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?=$this->fileAsset('css/sb-admin.css')?>" rel="stylesheet">
    <link href="<?=$this->fileAsset('font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <script src="<?=$this->fileAsset('js/jquery.js')?>"></script>
    <script src="<?=$this->fileAsset('js/bootstrap.min.js')?>"></script>
</head>

<body>

<div id="wrapper">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?$this->url('/')?>">Гагарин</a>
        </div>

        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <?
            $menu = [
                ['name' => 'Дашборд', 'test' => '/^\/$/', 'url' => $this->url('')],
                ['name' => 'Платежи', 'test' => '/^\/payments/', 'url' => $this->url('payments'), 'icon' => 'fa-bar-chart-o'],
                ['name' => 'Пользователи', 'test' => '/^\/users/', 'url' => $this->url('users'), 'icon' => 'fa-bar-chart-o'],
                ['name' => 'Миссии', 'test' => '/^\/missions/', 'url' => $this->url('missions'), 'icon' => 'fa-bar-chart-o'],
                ['name' => 'Уровни', 'test' => '/^\/levels/', 'url' => $this->url('levels'), 'icon' => 'fa-bar-chart-o'],
                ['name' => 'Моб стата', 'test' => '/^\/mobile/date', 'url' => $this->url('mobile/date'), 'icon' => 'fa-bar-chart-o'],
            ];
            ?>
            <ul class="nav navbar-nav side-nav">
                <? foreach ($menu as $item) { ?>
                    <li class="<?= $this->urlMatch($item['test']) ? 'active' : '' ?>">
                        <a href="<?=$item['url']?>"><i class="fa fa-fw <?=isset($item['icon']) ? $item['icon'] : 'fa-dashboard'?>"></i> <?=$item['name']?></a>
                    </li>
                <? } ?>
            </ul>
        </div>
    </nav>

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <?=isset($title) ? $title : 'Dashboard'?>
                        <small><?=isset($description) ? $description : 'Общая информация'?></small>
                    </h1>
                    <ol class="breadcrumb">
                        <? foreach ($breadcrumbs as $item) { ?>
                        <li>
                            <? if (isset($item['icon'])) { ?>
                                <i class="fa fa-<?= isset($item['icon']) ? $item['icon'] : '' ?>"></i>
                            <? } ?>

                            <? if (isset($item['url'])) { ?>
                                <a href="<?=$item['url']?>"><?=$item['name']?></a>
                            <? } else { ?>
                                <?=$item['name']?>
                            <? } ?>
                        </li>
                        <? } ?>
                    </ol>
                </div>
            </div>

            <?=content($this)?>
        </div>
    </div>
</div>
</body>

</html>
