<?php

namespace Api\Controller;

use App\Product\ProductRegistry;
use Components\JsonResponse;
use Components\Request;
use Components\Response;

class ProductController extends BaseController
{
    public function releaseAction(Request $request)
    {
        $id = $request->post('id');

        $user = $this->container->getUser();
        if (null === $product = $user->getProduct($id)) {
            $user->addProduct($product = ProductRegistry::get($id));
        }

        if ($product->getCount()) {
            $result = $product->release($this->container);
            $this->container->getORM()->flush($user);

            return new JsonResponse($result);
        }

        return new Response(0);
    }
}