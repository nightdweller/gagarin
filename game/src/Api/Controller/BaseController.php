<?php

namespace Api\Controller;
use App\Container\Container;
use Components\Controller\AbstractController;

/**
 * @property Container $container
 */
class BaseController extends AbstractController
{
    protected $container;
}