<?php

namespace Api\Controller;
use App\Container\Container;
use App\Fabric\FabricRegistry;
use App\Product\ProductRegistry;
use Components\Controller\AbstractController;
use Components\JsonResponse;
use Components\Request;

class FabricController extends BaseController
{
    public function buyAction(Request $request)
    {
        $index = $request->post('i');

        $user = $this->container->getUser();
        if (null === $fabric = $user->getFabric($index)) {
            $user->addFabric($fabric = FabricRegistry::get($index));
        }

        $product = $user->getProduct('fabric_price');
        $k = $product && $product->getRemainTime() > 0 ? 2 : 1;

        if ($fabric->getPrice() / $k <= $user->getMoney()) {
            $fabric->incCount();
            $user
                ->setIncValue($user->getIncValue() + $fabric->getProfit())
                ->incSum($fabric->getPrice())
            ;

            $user->decMoney($fabric->getPrice() / $k);
            $this->container->getORM()->flush($user);
            return new JsonResponse($user->getMoney());
        }

        return new JsonResponse(-1);
    }
}