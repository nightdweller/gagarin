<?php

namespace Api\Controller;

use App\Entity\User;
use App\Fabric\Fabric;
use App\Product\Product;
use App\Product\ProductRegistry;
use Components\JsonResponse;
use Components\Request;
use Components\Response;

class UserController extends BaseController
{
    public function levelAction(Request $request)
    {
        $user = $this->container->getUser();
        $user->setLevel($request->post('level'));
        $this->container->getORM()->flush($user);

        $this->container->getVk()->get('secure.setUserLevel', [
            'user_id' => 11771509,
            'level' => $user->getLevel()
        ]);

        return new Response(1);
    }

    public function setAction(Request $request)
    {
        $user = $this->container->getUser();

        if ($city = $request->post('city')) {
            $user->setCity($city);
        }

        $this->container->getORM()->flush($user);

        return new Response(1);
    }

    public function moneyAction(Request $request)
    {
        $money = $request->post('money');

        if (!$request->post('id') || null == $user = $this->container->getUserRepository()->find($request->post('id'))) {
            $user = $this->container->getUser();   
        }

        if ($clicks = $request->post('clicks')) {
            if ($clicks > $user->getClicks()) {
                $clicks = min($clicks, $user->getClicks() + $user->getLastUpdate()->getTimestamp() * 15, $user->getClicks() + $user->getEnergy());
                $clicksDiff = $clicks - $user->getClicks();

                $user
                    ->setClicks($clicks)
                    ->decEnergy($clicksDiff)
                ;
            }
        }
        
        $user->update();

        if ($user->getMoney() < $money) {
            $diff = time() - $user->getLastUpdate()->getTimestamp();
            $diff *= 25 * $user->getIncValue();
            $incValue = min($diff, $money - $user->getMoney());
            $user
                ->incSum($incValue)
                ->incMoney($incValue)
                ->setLastUpdate(new \DateTime())
            ;
        }

        $this->container->getORM()->flush($user);

        return new JsonResponse([
            'energy' => $user->getEnergy()
        ]);
    }

    public function getAction(Request $request)
    {
        $ids = explode(',', $request->post('ids'));
        $users = $this->container->getUserRepository()->findBy(['vkId' => $ids]);

        return new JsonResponse(array_map(function (User $user) {
            $corp = $user->getMoney();
            foreach ($user->getFabrics() as $fabric) {
                $corp += $fabric->getPrice() * $fabric->getCount();
            }

            return [
                'id' => $user->getId(),
                'vk_id' => $user->getVkId(),
                'money' => $user->getMoney(),
                'corp_price' => $corp,
                'incValue' => $user->getIncValue(),
                'fabrics' => array_map(function (Fabric $fabric) {
                    return $fabric->getIndex();
                }, $user->getFabrics())
            ];
        }, $users));
    }

    public function bonusAction(Request $request)
    {
        $user = $this->container->getUser();
        $products = ProductRegistry::allArray();
        $product = $products[rand(0, count($products)-1)];

        if ($user->getProduct($product->getId())) {
            $product = $user->getProduct($product->getId());
        } else {
            $user->addProduct($product);
        }

        $user->setLastGift(new \DateTime('+1day'));

        $product->buy($this->container);
        $this->container->getORM()->flush($user);

        return new JsonResponse([
            'id' => $product->getId(),
            'count' => $product->getCount()
        ]);
    }

    public function getProductsAction(Request $request)
    {
        $id = $this->container->getUser()->getId();
        $user = $this->container->getUserRepository()->find($id);
        $this->container->getUserToken()->setUser($user);

        $products = $user->getProducts();

        return new JsonResponse(array_map(function (Product $product) {
            return [
                'id' => $product->getId(),
                'count' => $product->getCount()
            ];
        }, $products));
    }
}
