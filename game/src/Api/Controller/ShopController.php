<?php

namespace Api\Controller;
use App\Container\Container;
use App\Entity\Payment;
use App\Fabric\FabricRegistry;
use App\Product\MoneyProduct;
use App\Product\ProductRegistry;
use Components\Controller\AbstractController;
use Components\JsonResponse;
use Components\Request;

class ShopController extends BaseController
{
    public function buyAction(Request $request)
    {
        list ($name, $count) = explode('|', $request->post('item'));

        $user = $this->container->getUser();
        $this->container->getUserToken()->setUser($user);

        $payment = $this->container->getShop()->handle($name, $count, $user);

        return new JsonResponse(array_merge([
            'balance' => $user->getBalance(),
        ], $this->getOrderResponsePipe($payment)));
    }

    public function buyVkAction(Request $request)
    {
        $type = $request->post('notification_type');

        if ($type == 'order_status_change' || $type == 'order_status_change_test') {
            return $this->order($request);
        }

        $userId = $request->post('receiver_id');

        $user = $this->container->getUserRepository()->findVk($userId);
        $this->container->getUserToken()->setUser($user);

        list ($name, $count) = explode('|', $request->post('item'));

        return new JsonResponse([
            'response' => $this->container->getShop()->getInfo($name, $count)
        ]);
    }

    public function updateAction(Request $request)
    {
        $orderId = $request->post('order_id');
        $payment = $this->container->getPaymentRepository()->findOneBy([
            'orderId' => $orderId,
        ]);

        $user = $this->container->getUserRepository()->find($this->container->getUser()->getId());
        $this->container->getUserToken()->setUser($user);

        return new JsonResponse($this->getOrderResponsePipe($payment));
    }

    private function getOrderResponsePipe(Payment $payment)
    {
        $user = $this->container->getUserRepository()->find($this->container->getUser()->getId());
        $this->container->getUserToken()->setUser($user);

        if ($payment->getType() == 'money') {
            return [
                'balance' => $user->getBalance()
            ];
        }

        if ($payment->getType() == 'energy') {
            return [
                'energy' => $user->getEnergy()
            ];
        }

        if ($payment->getType() == Payment::TYPE_FABRIC) {
            if (1 > $fabricIndex = $payment->getIndex()) {
                return new JsonResponse(['error' => 0]);
            }

            $fabric = $user->getFabric($fabricIndex);

            return [
                'fabrics' => [
                    ['id' => $fabric->getIndex(), 'count' => $fabric->getCount()],
                ]
            ];
        }

        if (in_array($payment->getType(), ['sponsor_big', 'sponsor_medium', 'sponsor_small'])) {
            return [
                'sponsor' => ['type' => $payment->getType(), 'index' => $payment->getIndex()]
            ];
        }

        if (null === $product = $user->getProduct($payment->getType())) {
            return ['error' => 0];
        }

        return [
            'products' => [['id' => $product->getId(), 'count' => $product->getCount()]]
        ];
    }

    private function order(Request $request)
    {
        $arr = explode('|', $request->post('item'));
        $name = $arr[0];
        $count = isset($arr[1]) ? $arr[1] : 0;

        if (substr($name, 0, 5) == 'offer') {
            $name = 'money';
            $count = $request->post('item_price') * 3;
        }

        $userId = $request->post('receiver_id');
        $orderId = $request->post('order_id');
        $status = $request->post('status');

        if ($status == 'chargeable') {
            $user = $this->container->getUserRepository()->findVk($userId);
            $this->container->getUserToken()->setUser($user);

            return new JsonResponse([
                'response' => $this->container->getShop()->handleVk($name, $count, $orderId, $user)
            ]);
        }


        return new JsonResponse([
            'order_id' => $orderId
        ]);
    }
}