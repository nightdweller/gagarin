<?php

namespace Api\Controller;
use App\Entity\User;
use App\Pagination\Pagination;
use Components\DB\QueryBuilder;
use Components\JsonResponse;
use Components\Request;
use Components\Response;

class RatingController extends BaseController
{
    public function placeAction(Request $request)
    {
        $users = $this->container->getRatingRepository()->getNear($money = $this->container->getUser()->getMoney());
        var_dump($money);
        var_dump($users);
        die;
//        return new Response($place);
    }

    public function allAction(Request $request)
    {
        $qb = $this
            ->container
            ->getUserRepository()
            ->createQueryBuilder()
        ;

        return $this->getRating($qb, $request->post('page'), $this->getType($request->post('type')));
    }

    public function friendsAction(Request $request)
    {
        $vkIds = $request->post('vk_ids');
        $qb = $this
            ->container
            ->getUserRepository()
            ->createQueryBuilder()
            ->andWhere('vk_id IN (:ids)')
            ->setParameter('ids', $vkIds)
        ;

        return $this->getRating($qb, $request->post('page'), $this->getType($request->post('type')));
    }

    public function cityAction(Request $request)
    {
        $qb = $this
            ->container
            ->getUserRepository()
            ->createQueryBuilder()
            ->andWhere('city = :city')
            ->setParameter('city', $this->container->getUser()->getCity())
        ;

        return $this->getRating($qb, $request->post('page'), $this->getType($request->post('type')));
    }

    private function getRating(QueryBuilder $qb, $page = 1, $type = 'sum')
    {
        $qb->addOrderBy($type, 'DESC');
        $pagination = new Pagination($qb, $page, 5);

        return new JsonResponse(array_map(function (User $user) use ($type) {
            return $this->getUserData($user, $type);
        }, $pagination->getItems()));
    }

    private function getUserData(User $user, $type = 'sum')
    {
        switch ($type) {
            case 'sum':
                return [
                    (int)$user->getVkId(),
                    (int)$user->getSum()
                ];
            case 'clicks':
                return [
                    (int)$user->getVkId(),
                    (int)$user->getClicks()
                ];
            case 'inc_value':
                return [
                    (int)$user->getVkId(),
                    $user->getIncValue()
                ];
        }
    }

    private function getType($type)
    {
        switch ($type) {
            case 'clicks': return 'clicks';
            case 'inc_value': return 'inc_value';
        }

        return 'sum';
    }
}