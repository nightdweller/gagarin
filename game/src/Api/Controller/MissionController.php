<?php

namespace Api\Controller;

use App\Entity\Mission;
use App\Mission\MissionRegistry;
use Components\Request;
use Components\Response;

class MissionController extends BaseController
{
    public function completeAction(Request $request)
    {
        $user = $this->container->getUser();
        $ids = explode(',', $request->request('ids'));
        $missions = $this
            ->container
            ->getMissionRepository()
            ->createQueryBuilder()
            ->andWhere('id in (:ids)')
            ->setParameter('ids', implode(',', $ids))
            ->getQuery()
            ->getResult()
        ;

        $map = [];
        /** @var Mission $mission */
        foreach ($missions as $mission) {
            $map[$mission->getId()] = $mission;
        }

//        foreach ($missions as $mission) {
//            $user->setBalance($user->getBalance() + $mission->getMoney());
//        }

        $api = $this->container->getVk();

        foreach ($ids as $id) {
            if (!$user->getMissions()->has($id)) {
                $user->getMissions()->add($id);
                $mission = $map[$id];
                $api->post('secure.addAppEvent', [
                    'activity_id' => $mission->getNumber(),
                    'user_id' => $user->getVkId()
                ]);
            }
        }

        $this->container->getORM()->flush($user);

        return new Response(1);
    }
}