<?php

namespace Api\Controller;

use App\Entity\Repository\GiftRepository;
use App\Entity\User;
use App\Product\ProductRegistry;
use Components\JsonResponse;
use Components\Request;
use Components\Response;

class GiftController extends BaseController
{
    public function sendAction(Request $request)
    {
        $vkId = $request->request('vkId');
        $type = $request->request('type');
        $index = $request->request('index');
        $user = $this->container->getUser();
        $this->container->getORM()->initInstance($user);

        if ($user->getGiftsCount() > 5) {
            return new Response(-1);
        }

        if (null === $target = $this->container->getUserRepository()->findVk($vkId)) {
            $target = new User();
            $target->setVkId($vkId);
            $this->container->getUserRepository()->insert($target);
            $this->container->getGiftRepository()->init($target->getId());
        }

        $result = $this->container->getGiftRepository()->push($target->getId(), $type, $index, $user);
        $this->handle($target, $type, $index);
        $user->setGiftsCount($user->getGiftsCount() + 1);

        $this->container->getORM()->flush($user);

        return new Response(5 - $user->getGiftsCount());
    }

    public function getAction(Request $request)
    {
        $result = $this->container->getGiftRepository()->findGifts($this->container->getUser()->getId());

        return new JsonResponse($result);
    }

    private function handle(User $target, $type, $index)
    {
        if ($type == GiftRepository::TYPE_PRODUCT) {
            if (null == $product = $target->getProduct($index)) {
                $product = ProductRegistry::get($index);
                $target->addProduct($product);
            }

            $product->incCount();
        }

        $this->container->getORM()->flush($target);
    }
}
