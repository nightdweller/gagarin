<?php
    return [
        '/' => ['App\\Controller\\MainController', 'index'],
        '/target' => ['App\\Controller\\MainController', 'target'],

        '/api/user.money' => ['Api\\Controller\\UserController', 'money'],
        '/api/fabric.buy' => ['Api\\Controller\\FabricController', 'buy'],
        '/api/rating.place' => ['Api\\Controller\\RatingController', 'place'],

        '/api/user.get_products' => ['Api\\Controller\\UserController', 'getProducts'],
        '/api/user.get' => ['Api\\Controller\\UserController', 'get'],
        '/api/user.set' => ['Api\\Controller\\UserController', 'set'],
        '/api/user.level' => ['Api\\Controller\\UserController', 'level'],

        '/api/shop.vk.buy' => ['Api\\Controller\\ShopController', 'buyVk'],
        '/api/shop.buy' => ['Api\\Controller\\ShopController', 'buy'],
        '/api/product.release' => ['Api\\Controller\\ProductController', 'release'],

        '/api/shop.update' => ['Api\\Controller\\ShopController', 'update'],

        '/api/mission.complete' => ['Api\\Controller\\MissionController', 'complete'],

        '/api/user.bonus' => ['Api\\Controller\\UserController', 'bonus'],

        '/api/gift.send' => ['Api\\Controller\\GiftController', 'send'],
        '/api/gift.get' => ['Api\\Controller\\GiftController', 'get'],

        '/api/rating.all' => ['Api\\Controller\\RatingController', 'all'],
        '/api/rating.friends' => ['Api\\Controller\\RatingController', 'friends'],
        '/api/rating.city' => ['Api\\Controller\\RatingController', 'city'],

        '/js/fabric_registry.js' => ['App\\Controller\\JsController', 'fabricRegistry'],
        '/js/level_registry.js' => ['App\\Controller\\JsController', 'levelRegistry'],
        '/js/product_registry.js' => ['App\\Controller\\JsController', 'productRegistry'],
        '/js/mission_registry.js' => ['App\\Controller\\JsController', 'missionRegistry'],

        '/mobile/init' => ['App\\Controller\\MobileController', 'init'],
        '/mobile/install' => ['App\\Controller\\MobileController', 'install'],
    ];
?>