<?php
return [
    'request' => ['class' => 'Components\\Request'],
    'router' => [
        'class' => 'Components\\Router',
        'arguments' => ['@container']
    ],
    'db' => [
        'class' => 'Components\\DB\\DB',
        'arguments' => [
            'p:mariadb75927-env-0035871.jelastic.regruhosting.ru',
            'root',
            'XY7ozqc2Ps',
            'gagarin',
        ]
    ],
    'orm' => [
        'class' => 'Components\\DB\\ObjectManager',
        'arguments' => ['@container']
    ],
    'php_template' => [
        'class' => 'Components\\PhpTemplate\\Template',
        'arguments' => ['@container']
    ],
    'user_token' => ['class' => 'Components\\UserToken']
];