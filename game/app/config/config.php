<?php

return [
    'rating_repository' => [
        'class' => 'App\\Entity\\Repository\\RatingRepository',
        'arguments' => ['@container']
    ],
    'shop' => [
        'class' => 'App\\Shop\\Shop',
        'arguments' => ['@container']
    ],
    'gift_repository' => [
        'class' => 'App\\Entity\\Repository\\GiftRepository',
        'arguments' => ['@container']
    ],
    'statistic_repository' => [
        'class' => 'App\\Entity\\Repository\\StatisticRepository',
        'arguments' => ['@container']
    ],
    'vk_api_client' => [
        'class' => 'Vk\\HttpClient\\HttpClient',
        'arguments' => [[
            'access_token' => '91ca786491ca7864915d8018cb91866a85991ca91ca7864c42d8ba68e311ce3ad2f17e6',
            'client_secret' => 'I7qpSn6oGESUBPXbUdmU',
//            'client_id' => '4985569'
        ]]
    ],
    'vk_api' => [
        'class' => 'Vk\Vk',
        'arguments' => ['@vk_api_client']
    ],
    'vk_api_simple_client' => [
        'class' => 'Vk\\HttpClient\\HttpClient',
        'arguments' => [[]]
    ],
    'vk_api_simple' => [
        'class' => 'Vk\Vk',
        'arguments' => ['@vk_api_simple_client']
    ]
];
