<?php

return [
    'App\\Entity\\User' => [
        'repository' => 'App\\Entity\\Repository\\UserRepository',
        'table' => 'users',
        'mapping' => [
            'id' => 'id',
            'vkId' => 'vk_id',
            'lastUpdate' => 'last_update',
            'lastGift' => 'last_gift',
            'money' => 'money',
            'incValue' => 'inc_value',
            'fabrics' => 'fabrics',
            'products' => 'products',
            'missions' => 'missions',
            'giftsCount' => 'gifts_count',
            'sum' => 'sum',
            'city' => 'city',
            'balance' => 'balance',
            'level' => 'level',
            'info' => 'info',
            'clicks' => 'clicks',
            'energy' => 'energy',
            'lastEnergyUpdate' => 'last_energy_update',
            'lastLevelUpdate' => 'last_level_update',
            'lastNotification' => 'last_notification'
        ],
        'transformers' => [
            'fabrics' => 'App\\Transformer\\UserTransformer',
            'products' => 'App\\Transformer\\UserTransformer',
            'missions' => 'App\\Transformer\\UserTransformer'
        ]
    ],
    'App\\Entity\\Payment' => [
        'repository' => 'App\\Entity\\Repository\\PaymentRepository',
        'table' => 'payments',
        'mapping' => [
            'id' => 'id',
            'orderId' => 'order_id',
            'userId' => 'user_id',
            'type' => 'type',
            'index' => 'item_index'
        ],
        'transformers' => []
    ],
    'App\\Entity\\Mission' => [
        'repository' => 'App\\Entity\\Repository\\MissionRepository',
        'table' => 'missions',
        'mapping' => [
            'id' => 'id',
            'name' => 'name',
            'description' => 'description',
            'image' => 'image',
            'type' => 'type',
            'message' => 'message',
            'extra' => 'extra',
            'money' => 'money',
            'number' => 'number'
        ],
        'transformers' => []
    ],
    'App\\Entity\\Upgrade' => [
        'repository' => 'App\\Entity\\Repository\\UpgradeRepository',
        'table' => 'upgrades',
        'mapping' => [
            'id' => 'id',
            'name' => 'name',
            'description' => 'description',
            'message' => 'message',
            'fabric' => 'fabric',
            'code' => 'code'
        ]
    ],
    'App\\Entity\\Sponsor' => [
        'repository' => 'App\\Entity\\Repository\\SponsorRepository',
        'table' => 'sponsors',
        'mapping' => [
            'id' => 'id',
            'number' => 'number',
            'vkId' => 'vk_id',
            'type' => 'type',
            'votes' => 'votes'
        ],
        'transformers' => []
    ],
    'App\\Entity\\Level' => [
        'repository' => 'App\\Entity\\Repository\\LevelRepository',
        'table' => 'levels',
        'mapping' => [
            'id' => 'id',
            'name' => 'name',
            'number' => 'number',
            'criteria' => 'criteria'
        ],
        'transformers' => [
            'criteria' => 'App\\Transformer\\LevelTransformer'
        ]
    ]
];
