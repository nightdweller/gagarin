<?php
return [
    '/' => ['Admin\\Controller\\DefaultController', 'index'],
    '/test' => ['Admin\\Controller\\DefaultController', 'test'],

    '/payments' => ['Admin\\Controller\\PaymentController', 'list'],
    '/payments/edit' => ['Admin\\Controller\\PaymentController', 'edit'],

    '/missions' => ['Admin\\Controller\\MissionController', 'list'],
    '/missions/edit' => ['Admin\\Controller\\MissionController', 'edit'],
    '/missions/new' => ['Admin\\Controller\\MissionController', 'new'],

    '/upgrades' => ['Admin\\Controller\\UpgradeController', 'list'],
    '/upgrades/edit' => ['Admin\\Controller\\UpgradeController', 'edit'],
    '/upgrades/new' => ['Admin\\Controller\\UpgradeController', 'new'],

    '/levels' => ['Admin\\Controller\\LevelController', 'list'],
    '/levels/edit' => ['Admin\\Controller\\LevelController', 'edit'],
    '/levels/new' => ['Admin\\Controller\\LevelController', 'new'],

    '/users' => ['Admin\\Controller\\UserController', 'list'],
    '/users/edit' => ['Admin\\Controller\\UserController', 'edit'],
    '/users/new' => ['Admin\\Controller\\UserController', 'new'],

    '/mobile/date' => ['Admin\\Controller\\StatisticController', 'date'],

    '/retarget' => ['Admin\\Controller\\RetargetController', 'list'],
    '/retarget/all' => ['Admin\\Controller\\RetargetController', 'all'],
];
