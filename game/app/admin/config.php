<?php

return array_merge(require(WEB_DIR . '/../../app/config/config.php'), [
    'criteria_type' => [
        'class' => 'Admin\\Form\\Type\\CriteriaType',
        'arguments' => ['@container']
    ]
]);
