<?php

const WEB_DIR = __DIR__;
$loader = require_once __DIR__ . '/../vendor/autoload.php';

use Components\Kernel\Kernel;
use Components\Console\Console;

$kernel = new Kernel('App\\Container\\Container');
$input = new \Components\Console\ArgvInput();

$console = new Console($kernel->getContainer());
$console->initCommands();
$console->run($input);